package com.uthkrushta.basis.bean;

public class NameValueBean {
	
	private String strBeanFieldName;
	private String strBeanFieldValue;
	private boolean blnKey;
	
	
	public boolean isBlnKey() {
		return blnKey;
	}

	public void setBlnKey(boolean blnKey) {
		this.blnKey = blnKey;
	}

	public String getStrBeanFieldName() {
		return strBeanFieldName;
	}

	public void setStrBeanFieldName(String strBeanFieldName) {
		this.strBeanFieldName = strBeanFieldName;
	}

	public String getStrBeanFieldValue() {
		return strBeanFieldValue;
	}

	public void setStrBeanFieldValue(String strBeanFieldValue) {
		this.strBeanFieldValue = strBeanFieldValue;
	}

	public NameValueBean(String strBeanFieldName, String strBeanFieldValue,
			boolean blnKey) {
		super();
		this.strBeanFieldName = strBeanFieldName;
		this.strBeanFieldValue = strBeanFieldValue;
		this.blnKey = blnKey;
	}
	
	
	
	
}
