package com.uthkrushta.basis.bean;

public class PaginationBean {
	private int intStartingCount;
	private int intNumberOfRecords;

	public int getIntStartingCount() {
		return intStartingCount;
	}

	public void setIntStartingCount(int intStartingCount) {
		this.intStartingCount = intStartingCount;
	}

	public int getIntNumberOfRecords() {
		return intNumberOfRecords;
	}

	public void setIntNumberOfRecords(int intNumberOfRecords) {
		this.intNumberOfRecords = intNumberOfRecords;
	}

}
