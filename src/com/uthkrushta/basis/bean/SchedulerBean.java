package com.uthkrushta.basis.bean;

public class SchedulerBean {
	
	long lngID;
	long lngInstanceID;
	long lngLoopCounter;
	int intStatus;
	String strFilePath;
	
	public SchedulerBean() {
		super();
		intStatus = 1;
		lngLoopCounter = 0;
		strFilePath = "";
	}
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public long getLngInstanceID() {
		return lngInstanceID;
	}
	public void setLngInstanceID(long lngInstanceID) {
		this.lngInstanceID = lngInstanceID;
	}
	public long getLngLoopCounter() {
		return lngLoopCounter;
	}
	public void setLngLoopCounter(long lngLoopCounter) {
		this.lngLoopCounter = lngLoopCounter;
	}
	public int getIntStatus() {
		return intStatus;
	}
	public void setIntStatus(int intStatus) {
		this.intStatus = intStatus;
	}
	public String getStrFilePath() {
		return strFilePath;
	}
	public void setStrFilePath(String strFilePath) {
		this.strFilePath = strFilePath;
	}
}
