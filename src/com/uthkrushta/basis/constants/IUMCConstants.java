package com.uthkrushta.basis.constants;

public interface IUMCConstants {
	
	int BIOMETRIC_STATUS_ACTIVE = 1;
	int BIOMETRIC_STATUS_INACTIVE = 2;
	int BIOMETRIC_URL_LNGID = 1;
	
	
	String BN_APPOINTMENT_HEADER_TRANSACTION = "AppointmentHeaderTransactionBean";
	String ST_APMT_BOOKING_DATE= "{[APMT_BOOK_DATE]}";
	String ST_APMT_CREATE_DATE= "{[APMT_CREATE_DATE]}";
	String ST_APMT_MEMBER_NAME= "{[MEMBER_NAME]}";
	String ST_APMT_MEMBER_PHONE_NUMBER= "{[MEMBER_PHONE_NUMBER]}";
	String ST_APMT_MEMBER_EMAIL_ID= "{[MEMBER_EMAIL_ID]}";
	String ST_APMT_SERVICE_NAME= "{[SERVICE_NAME]}";
	String ST_APMT_TIME_SLOT= "{[APMT_TIME_SLOT]}";
	String ST_APMT_TIME_FROM= "{[APMT_TIME_FROM]}";
	String ST_APMT_TIME_TO= "{[APMT_TIME_TO]}";
	String ST_APMT_PACKAGE_NAME= "{[PACKAGE_NAME]}";
	String ST_APMT_STAFF_NAME= "{[STAFF_NAME]}";
	String ST_APMT_STATUS = "{[APPOINTMENT_STATUS]}";
	
	
	long MSG_TYPE_APMT_FOR_STAFF = 19;
	long MSG_TYPE_APMT_FOR_MEMBER = 20;
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//october 7th
	long STATUS_COMPLETED = 5; 
	String BN_ASSETS_GROUP_MASTER_BEAN = "AssetsGroupMasterBean";
	String ACTION_DETAILS = "ACTION_DETAILS";
	String BN_ASSETS_TRACE_GYM_BEAN = "AssetsTraceGymBean";
	String BN_ASSETS_GYM_BEAN_VIEW = "AssetsGymBeanView"; 
	String BN_SERVICE_MASTER_BEAN = "ServiceMasterBean";
	String BN_ASSETS_GYM_BEAN = "AssetsGymBean";
	String BN_SERVICE_TYPE_CODE_BEAN = "ServiceTypeCodeBean";
	String BN_PURCHASE_TYPE_CODE_BEAN = "PurchaseTypeCodeBean";
	String EXPIRING_MEMBERS ="EXPIRING_MEMBERS";
	long lngCurrent = 1;
	String BN_SUP_MM_NOTIFICATION_MASTER_BEAN = "NotificationMasterBean";
	String BN_STAFF_ROLE_MASTER_BEAN_VIEW = "StaffRoleMasterBeanView";
	//old
	String ACTION_COMPANY = "ACTION_COMPANY";
	String BATCH_LIST = "BATCH_LIST";
	String STAFF_BATCH_DETAILS = "STAFF_BATCH_DETAILS";
	String STAFF_BATCH_CONT = "STAFF_BATCH_CONT";
	String BN_PLAN_BATCH_NAME_MASTER_BEAN_VIEW = "PlanBatchNameMasterBeanView";
	String DETAILED_ATTENDANCE_REPORT = "DETAILED_ATTENDANCE_REPORT";
	String DIFF_DAYS = "DIFF_DAYS";
	String MEMBER_LIST ="MEMBER_LIST";
	String STAFF_LIST = "STAFF_LIST";
	String ATTANDACE_SUMMARY_REPORT = "ATTANDACE_SUMMARY_REPORT";
	long LOGGED_IS_STAFF = 0;
	long LOGGED_IS_MEMBER = 1;
	String ATTANDACE_REPORT= "ATTANDACE_REPORT";
	String BN_ATTENDANCE_GYM_BEAN= "AttendanceGymBean";
	long ATTENDANCE = 8;
	String MEMBER_TRANSFER_REPORT= "MEMBER_TRANSFER_REPORT";
	String BN_MEMBER_TRANSFER_GYM_BEAN_VIEW = "MemberTransferGymBeanView";
	String BN_MEMBER_PLAN_SUBS_FROM_TO="MemberPlanSubsFromToTransactionBean";
	long TRANSFER_REASON = 2;
	long DISCOUNT_REASON = 1;
	String BN_OCCUPATION_CODE_BEAN = "OccupationCodeBean";
    String BN_SUBS_NAME_PLAN_AMT_VIEW="SubsNamePlanBasedAmtCodeViewBean";
    
	long FREEZED = 1;
	long EXTENDED = 2;
	//20 aug 2015
	//by me
	String ACTION_GET_TO_COMP = "ACTION_GET_TO_COMP";
	String ACTION_PLAN = "ACTION_PLAN";
	String ACTION_PAUSE = "ACTION_PAUSE";
	String ACTION_SIGN = "SIGN";
	String BN_MEMBER_PAUSE_TRANSACTION_BEAN = "MemberPauseTransactionBean";
	String BN_PAUSE_TYPE = "ExtensionTypeCodeBean";
	long REFERRED_MEMBER = 2;
	long REFERRED_STAFF = 1;
	String ENQUIRY_REPORT = "ENQUIRY_REPORT";
	String MEMBER_REPORT = "MEMBER_REPORT";
	String PAGE_MODE_CREATE = "Create";
	String Asset_Report = "Asset_Report";
	
	//Newly Added
	String BN_TIME_HOUR="TimeHourCodeBean";
	String BN_TIME_MINUTE="TimeMinuteCodeBean";
	String BN_TIME_FORMAT="TimeFormatCodeBean";

	long NONE= -99;
	
	long USER_TYPE_CUSTOMER = 2;
	long USER_TYPE_EMPLOYEE = 1;
	
	long MATERIAL_TYPE_FINISHED = 3;
	long MATERIAL_TYPE_SEMIFINISHED = 2;
	long MATERIAL_TYPE_RAW_MAT = 1;		
	
	
	String ROLEID = "ROLEID";
	String USERID = "USERID";
	
	
	
	long ITEM_STATUS_NEW = 1;
	long ITEM_STATUS_INVENTORY_ALLOCATED = 2;
	long READY_FOR_CUTTING = 3;
	long READY_FOR_STITCHING = 4;
	long READY_FOR_DISPATCH = 5;
	long DELIVERED = 6;
	long CANCELED = 7;
	long REJECTED = 8;
	
	long ACCOUNT_BOOK_TYPE_INCOME = 1;
	long ACCOUNT_BOOK_TYPE_EXPENSE = 2;
	
	int IS_VARBAL_CLICKED =1;	
	int YES = 1;
	int NO = 2;
	int STATUS_NEW = 1;
	int STATUS_ACTIVE = 2;
	String ADD_CLICK = "ADD_CLICK";
	String SAVE_CLICK = "SAVE_CLICK" ;
	String CANCEL_CLICK = "CANCEL_CLICK";
	String ACTION_UPDATE_INVANTORY = "UPDATE_INVANTORY";
	String ACTION_CANCEL_INVOICE    = "CANCEL_INVOICE";
	long TR_QUOTATION = 1;
	long TR_PO = 2;
	int STATUS_REJECTED=4;

	String ACTION_FOR_CUTTING = "FOR_CUTTING";
	String ACTION_SUBMIT_BY_SUPERVISOR = "ACTION_SUBMIT_BY_SUPERVISOR";
	
	long WI_ALLOCATED = 2;
	
	long ROLE_TYPE_ADMIN = 1;	
	long ROLE_TYPE_CUSTOMER = 3;
	long ROLE_TYPE_USER = 2 ;
	
	long COMPANY = 1;
	long BRANCH = 2;
	long SUPPLIER = 3;
	long SERVICE_PROVIDER = 4;
	long MANUFACTURER = 5;
	
	int COMPANY_TYPE_SUPPLIER = 2;
	int COMPANY_TYPE_SELF = 1;
	
	String BN_ENQUIRY_TRACE_TRANSACTION_BEAN = "EnquiryTraceTransaction";
	String BN_ENQUIRY_HEADER_TRANSACTION_BEAN = "EnquiryHeaderTransaction";
	String BN_ENQUIRY_HEADER_TRANSACTION_BEAN_VIEW = "EnquiryHeaderTransactionView";
	String BN_VOUCHER_HEADER = "VoucherHeaderBean";
	String BN_VOUCHER_ITEM = "VoucherItemBean";
	String BN_EXPENSE_BEAN = "ExpenseBean";
	String BN_WORK_ORDER_VIEW = "WorkOrderViewBean";
	String QUOTATION_REPORT_QUERY_STRING = "QUOTATION_REPORT_QUERY_STRING";
	String MATERIAL_REPORT_QUERY_STRING = "MATERIAL_REPORT_QUERY_STRING";
	String OUT_DC_CLICK="OUT_DC_CLICK";
	int STATUS_INACTIVE = 3;
	String LOAD_TERMS_COND="LOAD_TERMS_COND";
	String LOAD_PO="LOAD_PO";
	String DEFAULT_BUTTONS = "DEFAULT_BUTTONS";
	String LOAD_CUST_DC="LOAD_CUST_DC";
	int STATUS_DELETED = 4;
	int IS_VERBAL=1;
	String LOAD_CUST_INFO = "LOAD_CUST_INFO";
	String MATERIAL_REPORT = "MATERIAL_REPORT";
	String SUPPLIER_REPORT = "SUPPLIER_REPORT";
	
	long SEMI_FINISHED=2;
	long FINISHED=3;
	int OUTGOING_TOOL=1;
	int INCOMING_TOOL=2;
	
	
	long STAFF = 1;
	long CUSTOMER = 2;
	
	String BN_MATERIAL_OUTPUT = "MaterialOutputBean";
	String BN_CUSTOMER_MEASUREMENT = "CustomerMesurementBean";
	String BN_ITEM_STATUS = "ItemStatusBean";
	String BN_WORK_ORDER_ITEM = "WorkOrderItemBean";
	String BN_WORK_ORDER_HEADER = "WorkOrderHeaderBean";
	String BN_CLOTHINGS = "ClothingsBean";
	String LOAD_USERS = "LOAD_USERS";
	String ACTION_MATERIAL_CHANGE="Material change";
	String ACTION_MATERIAL_TYPE_CHANGE="MaterialType change";
	String ADD_BTN_VALUE = "ADD_BTN_VALUE";
	String SAVE_BTN_VALUE = "SAVE_BTN_VALUE" ;
	String CANCEL_BTN_VALUE = "CANCEL_BTN_VALUE";
	String PRINT_BTN_VALUE = "PRINT_BTN_VALUE";
	String PRINT_CLICK = "PRINT_CLICK";
    String LOAD_ITEMS_FROM_REF_DOC = "LOAD_ITEMS_FROM_REF_DOC";
    String EMPLOYEE_REPORT = "EMPLOYEE_REPORT";
    
    String BN_MATERIAL_VIEW_BEAN="MaterialViewBean";
	String SHOW_DEL = "SHOW_DEL";
	String SHOW_ADD = "SHOW_ADD";
	String SHOW_CANCEL = "SHOW_CANCEL";
	String SHOW_PRINT = "SHOW_PRINT";
	String WORK_ORDER_TRACKING_REPORT = "WORK_ORDER_TRACKING_REPORT";
	String CUSTOMER_REPORT = "CUSTOMER_REPORT";
	String CONTEXT_PATH = "CONTEXT_PATH";
	
	int TOOL_STATUS = 4;
	
	int NOT_AUTO_GENERATE = 0;
	int FINANCIAL_YEAR_NOT_RESET = 0;
	int FINANCIAL_YEAR_RESET = 1;
	int AUTO_GENERATE = 1;
	int UT_EMPLOYEE = 1;
	int UT_CUSTOMER = 2;
	int UT_SUPPLIER = 3;
	
	  public final int PO_STATUS_OPEN = 1;
      public final int PO_STATUS_INVOICED = 2;
      public final int PO_STATUS_CANCELLED = 3;
      public final int PO_STATUS_INVOICE_CANCELLED = 4;
      public final int PO_STATUS_PARTIALLY_INVOICED = 5;
      public final int PO_STATUS_COMPLETED = 6;
      public final int PO_STATUS_DC_ISSUED = 7;
      public final int PO_STATUS_DC_COMPLETED = 8;

      public final int DC_STATUS_OPEN = 1;
      public final int DC_STATUS_INVOICED = 2;
      public final int DC_STATUS_CANCELLED = 3;
      public final int DC_STATUS_INVOICE_CANCELLED = 4;
      public final int DC_STATUS_PARTIALLY_INVOICED = 5;
      public final int DC_STATUS_COMPLETED = 6;

      public final int INVOICE_STATUS_OPEN = 1;
      public final int INVOICE_STATUS_INVOICED = 2;
      public final int INVOICE_STATUS_CANCELLED = 3;
      public final int INVOICE_STATUS_PARTIALLY_INVOICED = 4;
      public final int INVOICE_STATUS_PARTIALLY_CLEARED = 5;
      public final int INVOICE_STATUS_COMPLETED = 6;
      
      public final int WI_STATUS_NEW = 1;
      public final int WI_STATUS_INVENTORY_ALLOCATED = 2;
      public final int WI_STATUS_READY_TO_CUT = 3;
      public final int WI_STATUS_READY_TO_STITCH = 4;
      public final int WI_STATUS_READY_TO_DISPATCH = 5;
      public final int WI_STATUS_DELIVERED = 6;
      
	
	String COMPANY_ID = "COMPANY_ID" ;
	String ACTION_INV_TYPE_CHANGE="INV_TYPE_CHANGE";
	String ACTION_CATEGORY_CHANGE="CATEGORY_CHANGE";
	
	String PAGE_MODE_PRINT="PRINT";
	String ENTITY_PLAN ="ENTITY_PLAN";
	String SHOW_SAVE = "SHOW_SAVE";
	String STOCK_REPORT = "STOCK_REPORT";
	long MATERIAL_USAGE_SALES= 1;
	long MATERIAL_USAGE_LABOUR= 2;
	
	int MATERIAL_SALES= 2;
	int MATERIAL_LABOUR= 1;
	
	
	int PROPERTY_STATUS_PENDING = 1; 
	int PROPERTY_STATUS_APPROVED = 2;
	int PROPERTY_STATUS_REJECT = 3;
	int PROPERTY_STATUS_EXPIRE = 4;
	int PAYMENT_METHOD_CARD_TYPE=3;
	
long INV_CUST=2;
long INV_SUP=1;
long INV_PROFORMA=3;
long INV_EXCISE=4;

long DC_CUST=1;
long DC_OUT_DC=2;
long DC_IN_DC=3;

long PO_SUP=1;
long PO_CUST=2;
	
	String BN_MEMBER_MASTER_BEAN_VIEW = "MasterMemberBeanView";
	String BN_ENQUIRY_STATUS_CODE_BEAN = "EnquiryStatusCodeBean";
	String BN_CONVERSION_CODE_BEAN = "ConversionChanceCodeBean";
	String BN_SOURCE_CODE_BEAN = "SourceCodeBean";
	String BN_PAYMENT_METHOD_BEAN="PaymentMethodBean";
	
	String BN_User_Master_Bean = "UserMasterBean";
	
	String BN_MATERIAL_USAGE = "materialUsageBean";
	String BN_STOCK_TYPE = "stockTypeBean";
	String BN_SECURITY_QUESTION = "SecurityQuestionsCodeBean";
	String BN_STATUS = "StatusBean";
	String selCustomer = "selCustomer";
	String BN_TEST_HEADER_BEAN="TestHeaderBean";
	String BN_TEST_ITEM_BEAN="TestItemBean";
	String BN_PRODUCTION_ENTRY_HEADER_BEAN = "ProductionEntryHeaderBean";
	String BN_PRODUCTION_ENTRY_ITEMS_BEAN = "ProductionEntryItemsBean";
	String BN_ANNEXURE_BEAN="AnnexureBean";
	String BN_TAX_BEAN="TaxBean";
	
	String BN_GOODS_RECIVABLE_NOTE_HEADER="GoodsRecivableNoteHeader";
	String BN_TEST_ITEM_CAT_BEAN="TestItemCatBean";
	String BN_VIEW_REGISTER_ITEMS="ViewToolsMovementRegisterBean";
	String BN_GOODS_RECIVABLE_NOTE_ITEM="GoodsRecivableNoteItem";
	String BN_SUPPLIER_ENQUIRY_HEADER="SupplierEnquiryHeaderBean";
	String BN_TOOLS_MOV_REG_HEADER="ToolsMovementRegistryHeaderBean";
    
	String BN_QUOTATION_VIEW_BEAN = "QuotationViewBean";
	String BN_SUPPLIER_ENQUIRY_ITEM="SupplierEnquiryItemBean";
	
	String BN_TRANS_TEST_HEADER="TransTestHeaderBean";
	String BN_TRANS_TEST_ITEMS="TransTestItemBean";
	String BN_WORK_ORDER_STATUS = "ItemStatusBean";
	
	String BN_COMPANY_MASTER = "CompanyProfileBean";
	String BN_COMPANY_VIEW = "companyViewBean";
	String INVOICE_REPORT_QUERY_STRING = "INVOICE_REPORT_QUERY_STRING";
	int PAYMENT_METHOD_CHEQUE =2;
	String PAGE_HEADER = "HEADER";
	String PAGE_DESC = "DESC";
	String STATUS_TEXT = "STATUS";
	String STATUS_STYLE = "STYLE";
	String LINK = "LINK";
	String LOGIN_OBJ = "LOGIN_OBJ";

	String CITYID = "CITYID";
	String USERNAME = "USERNAME";
	String ROLE_ID = "ROLE_ID";
	String USER_ID = "USER_ID";
	String COMP_ID = "COMP_ID";
	String USER_TYPE = "USER_TYPE";
	String ROLE_TYPE = "ROLE_TYPE";
	String DISPLAY_NAME = "DISPLAY_NAME";
	
	
	int FIRST_LOGIN = 1;
	int FILE_UPLOAD_SINGLE = 1;
	
	String GET_ACTIVE_ROWS = " intStatus = "+ STATUS_ACTIVE;
	
	String PAGE_MODE_EDIT = "Edit";
	String PAGE_MODE_NEW = "NEW";
	
	String LOOKUP_FILE_UPLOAD_URL = "../Lookup/lookupFileUploader.jsp";

	long PENDING_APPROVAL = 1;
	long RELEAVED = 3;
	long INHOUSE_MATERIAL = 1;
	long CUSTOMER_MATERIAL = 2;
	long APPLICATION_STATUS = 1;
	long MATERIAL_STATUS = 2;
	long SELF_COMPANY = 1;
	
	long CUSTOMER_COMPANY = 2;
	long SUPPLIER_COMPANY = 3;
	long SUBCONTRACTOR_COMPANY=4;
	long INCOMING_DC = 1;
	long OUTGOING_DC = 2;
	
	String ACTION_GET_MEM = "ACTION_GET_MEM";
	String ACTION_ACTIVATE = "ACTIVATE";
	String ACTION_SUBMIT = "SUBMIT";
	String ACTION_GET = "GET";
	String ACTION_ADD = "ADD";
	String ACTION_DEL = "DEL";
	String ACTION_MATCHING_MAIL = "MATCHING_EMAIL";
	String ACTION_NEW_PASSWORD = "NEW_PASSWORD";
	
	String ACTION_EDIT = "EDIT";
	String ACTION_DELETE_ITEM = "DEL_ITEM";
	String ACTION_GET_NEXT_NUM = "GETNEXTNUM";
	String ACTION_GET_MAT_USED = "GET_MAT_USED";
	
	String STR_BLANK = "";
    String ACTION_RESET= "RESET";
	String OK = "bg-green" ; //StatusOk";
	String WARNING = "bg-brick lt";      //"StatusWarning";
	String ERROR = "bg-red dker" ; //StatusError";
	String LOAD_IS_LOGIN_REQUIRED = "LOAD_IS_LOGIN_REQUIRED";
	String DATE_TIME_AM_PM_FORMAT = "dd-MM-yyyy hh:mm:ss a";
	String DATE_TIME_FORMAT = "dd-MM-yyyy hh:mm:ss";
	String DATE_FORMAT = "dd-MM-yyyy";
	String DB_DATE_FORMAT = "yyyy-MM-dd";
	String DB_DATE_TIME_FORMAT = "yyyy-MM-dd hh:mm";
	String DB_DATE_TIME_SEC_FORMAT = "yyyy-MM-dd HH:mm:ss";//(HH)to store the time in 24hr format
	String DATETIME_FORMAT = "YYYY-MM-DDTHH:MM:SSZ";//1985-04-12T23:20:50.52
	String FROM_LIST = "FROM_LIST";
	
	String NAME_SORT_ASC = "strName";
	
	
	String BN_MATERIAL_ITEM = "MaterialMasterItemBean";
	String BN_MATERIAL_HEADER = "MaterialMasterHeaderBean";
    String BN_CATEGORY_MASTER = "CategoryMasterBean";
	String BN_CARD = "CardTypeBean";
	String BN_INVOICE_ITEM = "InvoiceItemBean";
	String BN_INVOICE_HEADER = "InvoiceHeaderBean";
	String BN_PAYMENT_METHOD = "PaymentMethodBean";
	String BN_OPERATION_MASTER = "OperationMasterBean";
	String BN_TERMSCONDITION_MASTER = "TermsConditionsBean";
	String BN_PRODUCTION_ENTRY_ITEM = "ProductionEntryItemBean";
	String BN_PRODUCTION_ENTRY_HEADER = "ProductionEntryHeaderBean";
	String BN_PRODUCTION_PLAN_ITEM = "ProductioPlanItemsBean";
	String BN_PRODUCTION_PLAN_HEADER = "ProductionPlanHeaderBean";
	String BN_TRANSACTION_SETTING = "TransactionSettingBean";
	String BN_GLOBAL_SETTING = "GlobalSettingBean";
	String BN_STOCK_UPDATE_ON = "StockUpdateOn";
	String BN_FINANCIAL_YEAR = "FinancialYearBean";
	String BN_USER_MASTER_BEAN="UserMasterBean";
	String BN_SECURITY_QUESTION_BEAN = "SecurityQuestionBean";
	String BN_EMPLOYEE_TYPE_BEAN = "EmployeeTypeBean";
	String BN_NAVIGATION_BEAN ="NavigationBean";
	String BN_ROLE_BEAN = "RoleBean";
	String BN_ROLE_TYPE = "RoleTypeBean";
	String BN_MATERIAL_TYPE="MaterialTypeBean";
	String BN_TOOLS_BEAN = "ToolsMasterBean";
	String BN_UOM = "UnitOfMesurementBean ";
	String BN_TAX_MASTER = "TaxMasterBean";
	String BN_TAX_TYPE = "TaxTypeCodeBean";
	String BN_CITY_MASTER = "CityMasterBean";
	String BN_STATE_MASTER = "StateMasterBean";
	String BN_COUNTRY_MASTER = "CountryBean";
	String BN_LOGIN = "LoginBean";
	String BN_ROLE_MASTER_BEAN = "RoleMasterBean";
	String V_MENU="ApplicationMenuBean";
	String BN_MENU="MenuBean";
	String BN_USER_MASTER_VIEW = "UserViewBean";
	String BN_ACCOUNT_BOOK="AccountBookBean";
	String BN_COMPANY_OWNED_TYPE="CompanyOwnedTypeCodeBean";
	String BN_COMPANY_PLAN="CompanyPlanCodeBean";
	String BN_CURRENCY="CurrencyConfigurationBean";
	String BN_TIME_ZONE="TimeZoneConfigurationBean";
	String BN_LANGUAGE="LanguageConfigurationBean";
	String BN_MEMBER_PAYMENT_TRANSACTION="MemberPaymentTransactionBean";
	String BN_PAYMENT_MODE="PaymentModeCodeBean";
	String BN_PLAN_TYPE="PlanTypeCodeBean";
	String BN_SUBSCRIPTION_TYPE="SubscriptionTypeCodeBean";
	String BN_PLAN_BASED_AMOUNT="PlanBasedAmountCodeBean";
	String BN_RECEIPT_MEM_PLAN_SUBS="MemberPaymentReceiptListViewTransactionBean";
	
	String BN_GENDER_CODE_BEAN = "GenderCodeBean";
	String BN_BLOOD_GROUP_CODE_BEAN = "BloodGroupCodeBean";
	String BN_STATUS_CODE_BEAN = "StatusCodeBean";
	String BN_ICERelation_CODE_BEAN = "ICERelationBean";
	String BN_STAFF_MASTER_BEAN = "StaffMasterBean";
	String BN_STAFF_STATUS_CODE_BEAN  = "StatusCodeBean";
	String BN_MEMBERSHIP_STATUS_CODE_BEAN="MembershipStatusCodeBean";
	
	//By 007 for MM
	long MEMBER_ACTIVE = 1;
	long MEMBER_INACTIVE = 2;
	
	long STAFF_ACTIVE = 1;
	long STAFF_INACTIVE = 2;
	
	String BN_REFERRED_TYPE_CODE_BEAN  = "ReferredTypeCodeBean";
	long CD_STAFF_TRANSACTION_SETTING_NO= 1;
	long CD_MEMBER_TRANSACTION_SETTING_NO= 2;
	long CD_PLAN_TRANSACTION_SETTING_NO= 3;
	long CD_RECEIPT_TRANSACTION_SETTING_NO= 4;
	long CD_ASSETS_TRANSACTION_SETTING_NO= 5;
	long CD_SUPPLIER_TRANSACTION_SETTING_NO = 6;
	
	
	
	String BN_COMPANY_MASTER_BEAN = "CompanyMasterBean";
	String BN_MEMBER_MASTER_BEAN="MemberMasterBean";
	String BN_TAX_MASTER_BEAN = "TaxMasterBean";
	String BN_NAME_PREFIX = "NamePrefixCodeBean";
	String BN_CONF_BEAN = "ConfigurableBean";
	String BN_LAYOUT_TYPE_BEAN = "LayoutTypesBean";
	String BN_OM_BEAN = "OperatingManualsBean";
	String BN_BANK_BEAN = "BankBean";
	String BN_CONTACT_BEAN = "ContactDetailsBean";
	String BN_INHOUSE_REFERENCE_BEAN = "ReferenceInhouseMaterialsBean";
	String BN_ACC_BEAN = "AccessoriesBean";
	String BN_DEPT_BEAN = "DepartmentBean";
	String BN_FQ_BEAN = "FrequencyBean";
	String BN_CLS_BEAN = "ClassificationBean";
	String BN_STATUS_BEAN = "StatusBean";
	String BN_ACCOUNT_TYPE_BEAN = "AccountTypeBean";
	String BN_MATERIAL_MASTER_BEAN = "MaterialMasterViewBean";
	String BN_LAYOUT_BEAN = "LayoutBean";
	String BN_DC_HEADER_BEAN = "DeliveryChallanHeaderBean";
	String BN_DC_ITEMS_BEAN = "DeliveryChallanItemsBean";
	String BN_CITY_BEAN = "CityMasterBean";
	String BN_STATE_BEAN = "StateMasterBean";
	String BN_AREA_BEAN = "AreaMasterBean";
	String BN_MAKE_BEAN = "MakesBean";
	String BN_EMP_STATUS = "EmployeeStatusBean";
	String BN_TOOL_MASTER_BEAN = "ToolsMasterBean";
	String BN_BOM_HEADER_BEAN = "BillOfMaterialHeaderBean";
	String BN_BOM_ITEMS_BEAN = "BillOfMaterialItemsBean";
	String BN_PLAN_ENTRY_HEADER_BEAN = "PlanningEntryHeaderBean";
	String BN_PLAN_ENTRY_ITEMS_BEAN = "PlanningEntryItemsBean";
	
	String BN_PURCHASE_ORDER_HEADER_BEAN = "PurchaseOrderHeaderBean";
	String BN_PURCHASE_ORDER_ITEMS_BEAN = "PurchaseOrderItemsBean";
	String BN_QUOTATION_HEADER_BEAN = "QuotationHeaderBean";
	String BN_QUOTATION_ITEMS_BEAN = "QuotationItemsBean";
	String BN_TOOLS_HEADER_BEAN = "ViewToolsStatusBean";
	String BN_TOOLS_ITEMS_BEAN = "ToolsMovementRegistryItemsBean";
	String BN_FINANCIAL_YEAR_BEAN="FinancialYearBean";
	String BN_FINANCIAL_YEAR_MASTER_BEAN="FinancialYearMasterBean";
	String BN_APPLICATION_MENU_BEAN="ApplicationMenuBean";
	String BN_APPLICATION_MASTER_MENU_BEAN = "ApplicationMasterBean";
	String BN_ACTIVITY_ROLE_BINDING_BEAN = "ActivityRoleBindingFrameWorkBean";
	String BN_CUSTOMER_TYPE_BEAN = "CustomerTypeBean";
	String BN_PLAN_HEADER_MATERIAL_VIEW_BEAN="ProductionPlanHeaderMaterialViewBean";
	String BN_BATCH_MASTER="BatchMasterBean";
	String BN_DISCOUNT_REASON="ReasonForDiscountMasterBean";
	
	int OBJ_MASTER = 2;
	int OBJ_CODE = 8;
	int OBJ_MYWORK = 3;
	int CODE_BEAN = 1;
	int OBJ_COFIG = 4;
	int OBJ_FRAMEWORK =5;
	int OBJ_TRANSACTION = 6;
	int OBJ_PRODUCTION = 7;
	
	int PAGINATION_SIZE = 15;
	int COMMIT_SIZE = 15;
	
	int USER_LOGIN = 1;
	int  CLIENT_LOGIN = 2;
	String BN_PAYMENT_TERMS_BEAN = "TermsConditionsBean";
  
	
	
	
	int QUOTATION_NEW = 1;
	int QUOTATION_PARTIALLY_USED = 2;
	int QUOTATION_COMPLETED = 3;	
	int QUOTATION_CANCELLED = 4;
	
	
	
	int ENQUIRY_NEW = 1;
	int ENQUIRY_PARTIALLY_USED = 2;
	int ENQUIRY_COMPLETED = 3;	
	int ENQUIRY_CANCELLED = 4;
	
	int  PO_NEW = 1;
	int PO_PARTIALLY_USED = 2;
	int PO_COMPLETED = 3;
	int PO_CANCELLED = 4;
	

	long INVOICE_NEW = 1;
	long PARTIALLY_CLEARED_INVOICE = 2;
	long COMPLETED_INVOICE = 3;
	long CANCELLED_INVOICE = 4;

    int DC_NEW =1;
    int DC_PARTIALLY_USED = 2;
    int DC_COMPLETED =3;
    int DC_CANCELLED =4;
    
    
//Newly Added on 09/08
    
    String BN_LEDGER_BEAN="LedgersConfigurationBean";
    String BN_TAG_BEAN="TagsConfigurationBean";
    
	
    //Newly Added on 10/08
    
    int OPEN = 1;
    int CLOSED = 2;
    int INCOMING = 1;
    int OUTGOING = 2;
    
     
    String FIANCIAL_YEAR = "FIANCIAL_YEAR";
    String ACTION_APPROVE = "APPROVE";
	String ACTION_UPDATE = "UPDATE";
	String CBItemReportList = "CBITEMREPORTLIST";
	String FROM_DATE = "FROM_DATE";
	String TO_DATE = "TO_DATE";
	String LEDGER = "LEDGER";
	String EMPLOYEE = "EMPLOYEE";
	String TAGS = "TAGS";
	String IS_TALLY_BILL_ENTERED = "IS_TALLY_BILL_ENTERED";
	String IS_CASH_BOOK_APPROVED = "IS_CASH_BOOK_APPROVED";
	String CATEGORY = "CATEGORY";
	String PAY_METHOD = "PAY_METHOD";
	String INCOMING_AMOUNT = "INCOMING_AMOUNT";
	String OUTGOING_AMOUNT = "OUTGOING_AMOUNT";
	String BALANCE_AMOUNT = "BALANCE_AMOUNT";
	String ItemList = "ItemList";
	String INVOICE_TRACKING_REPORT = "INVOICE_TRACKING_REPORT";
    
    String BN_CB_STATUS_BEAN = "CashBookStatusCodeBean";
    String BN_CBIT_VIEW = "CashBookItemViewBean";
    String BN_CBHT = "CashBookHeaderTransactionBean";
    String BN_CBIT = "CashBookItemsTransactionBean";
    String BN_CASH_LEDGER_VIEW_BEAN = "CashLedgerViewTransactionBean";
    String BN_EXPENSE_CATEGORY_BEAN = "ExpenseCategoryBean";
    String BN_INCOME_SOURCE_BEAN = "IncomeSourceConfigurableBean";
    String BN_LEDGER_VIEW_BEAN = "LedgerViewConfigurationBean";
    String BN_FUND_ALLOCATION_BEAN = "FundAllocationTransactionBean";
    String BN_FUND_ALLOCATION_VIEW_BEAN = "FundAllocationViewTransactionBean";
    String RECEIPT_TRACKING_REPORT = "RECEIPT_TRACKING_REPORT";
    
  //Added Newly on 3/9/2015
    String BN_PLAN_SUBS_MEM_NAME="PlanSubsPaidMemNameTransactionViewBean";
    String PAYMENT_TRACKING_REPORT = "PAYMENT_TRACKING_REPORT";
    
    String BN_PAYMENT_TYPE = "PaymentTypeCodeBean";
    
  //Added Newly on 15/9/2015
    String BN_MONTH="MonthCodeBean";
	
  //Added Newly on 23/9/2015
    String BN_ZONAL_BEAN="ZonalBean";
    
   //Added Newly on 07/10/2015
    String BN_SUP_MM_PLAN_TYPE="SupPlanTypeMasterBean";
    String BN_SUP_MM_PLAN_BASED_AMT="SupPlanSubsAmtConfigurationBean";
    
    //Added Newly on 07/10/2015
    String BN_NOTIFICATION_MSG="MessageNotificationBean";
    
  //Added Newly on 17/10/2015
    String BN_LETTER_HEADER = "LetterHeadBean";
    int OBJ_UTILIES = 9;
    
    //Added on 3Nov2015
    String BN_REPORT_REPOSITORY="ReportRepositoryCodeBean";
    String BN_PRODUCT_GYM_BEAN="ProductGymBean";
    String BN_PRODUCT_GROUP="ProductGroupMasterBean";
    String BN_PRODUCT_CATEGORY="ProductCategoryCodeBean";
    String BN_PRODUCT_GYM_BEAN_VIEW="ProductGrpCatSuplrViewBean";
    String BN_BILL_TO="BillToCodeBean";
    String BN_OUTSIDERS="OutsidersProductBean";
    
    //Added on 9Nov 2015
    String ACTION_SEND_MAIL="SEND_MAIL";
    String BN_MAIL_TEMPLATE = "mailTemplateBean";
    String BN_INVOICE_VIEW_BEAN = "InvoiceViewBean";
    String ST_COMP_NAME = "{[COMPANY_NAME]}";
    String ST_COMP_DETAILS = "{[COMPANY_DETAILS]}";
    String ST_COMP_PIN = "{[COMPANY_PINNO]}";
    String ST_CUST_NAME = "{[CUST_NAME]}";
    String ST_CUST_ADDRESS = "{[CUST_ADDRESS]}";
    String ST_CUST_CONTACT_NO = "{[PHONE_NO]}";
    String BN_ADVANCE_RECIPT = "AdvanceRecipt";
    int AD_USED = 18;
    long CUST_INVOICE = 7;
    
    //Added on 23Nov2015
    int OBJ_PRODUCT_VIEW = 9;
    
    //Added on 25NOV
    
   
    long MEMBER = 2;
    long OUTSIDER = 3;
    
    String BN_SALES_REPO_VIEW="SalesReportProductViewBean";
    long CD_PRODUCT_INVOICE_TRANSACTION_SETTING_NO = 7;
    
    //Added on DEC3
    String ST_INV_TEM_START = "{[ITEM_ROW_START]}";
    String ST_INV_TEM_END = "{[ITEM_ROW_END]}";
    String ST_SL_NO = "{[SL.NO.]}";
	String ST_RATE = "{[RATE]}";
	String ST_MAT_NAME = "{[MAT_NAME]}";
	String ST_QUANTITY = "{[QUANTITY]}";
	 String ST_ITEM_AMOUNT = "{[AMOUNT]}";
	 String ST_INVOICE_NO = "{[INVOICE_NO]}";
	 String ST_SUB_TOTAL = "{[TOTAL_AMOUNT]}";
	 String ST_TAX_AMT = "{[TAX_AMOUNT]}";
	 String ST_GRAND_TOTAL = "{[GRAND_TOTAL]}";
	 String ST_DATE = "{[DATE]}";
	 String ST_AMT_IN_WORDS = "{[AMOUNT_IN_WORDS]}";
	 
	 //Added on DEC18
	 String DUE_AMOUNT = "bg-yellow" ;
	 String ABOUT_TO_EXPIRE = "bg-orange" ;
	 
	 //Added on DEC21
	 
	 String BN_OUTPUT_MANAGER="OutputManagerBean";
	 int EXP_FORMAT_PDF=5;
	 int EXP_FORMAT_DOC=1;
	 int EXP_FORMAT_EXCEL=2;
	 
	 //Added on DEC22
	  String ST_RECEIPT_NO = "{[RECEIPT_NO]}";
	  String ST_RECEIPT_DATE = "{[RECEIPT_DATE]}";
	  String ST_RECEIPT_AMT = "{[RECEIPT_AMT]}";
	  String ST_COMP_ADDRESS= "{[COMP_ADDRESS]}";
	  String ST_COMP_EMAIL = "{[COMP_EMAIL]}";
	 
	  //Added on DEC29
	  String BN_MEMBER_PROGRESS_TRACKING_BEAN="MemberProgressTrackingBean";
	  
	  //Added on DEC30
	 String BN_MEMBER_PROGRESS_VIEW_BEAN= "MemberProgressTrackingViewBean";
	 
	 //Added on DEC31
	 String BN_SMS_TYPE_UTILITY_BEAN="SMSTypeUtilityBean";
	 String BN_SMS_TEMPLATE_BEAN="SmsTemplateBean";
	 
	 //Added on 1JAN
	 String LOAD_SMSTEMPLATE="LOAD_SMSTEMPLATE";
	 String LOAD_SMSTEMPLATE_TXT="LOAD_SMSTEMPLATE_TXT";
	 String ST_SEND_TO="{[SEND_TO]}";
	 String ST_SMS_TEXT="{[SMS_TEXT]}";
	 
	 //Added on 5JAN
	 String ACTION_SEND_SMS="ACTION_SEND_SMS";
	 
	 //Added on 6JAN
	 int BIRTHDAY=1;
	 int ANIVERSARY=2;
	 int PAYMENTDUE=3;
	 int RENEWAL=4;
	 int PROMOTIONAL=5;
	 int FESTIVAL=6;
	 int PAYMENT=7;
	 int INVOICE=8;
	 int ABSENT=9;
	 int PAYMENT_SUMMARY=10;
	 int DAILY_SUMMARY=13;
	 //Added newly
	 int TODAY_EXPIRY=15;
	 int PLAN_EXPIRED=16;
	 int MEMBERSHIP_DUE=17;//Schedular membership
	 int SERVICE_DUE=18;//service membership
	 
	 String ST_NAME="{[NAME]}";
	 String BN_SMS_TEMP_NOTIFICATION="SMSTemplateNotificationBean";
	 //Added on 11JAN
	 String BN_SMS_TYPE="SmsTypeCodeBean";
	 String LOAD_SMS_TYPE="LOAD_SMS_TYPE";
	 int TRANSACTIONAL=1;
	 int PROMOTION=2;
	 
	 //Added on JAN18
	 int PH_NO_FROM_EXCEL=1;
	 int ENQ_MEM=3;
	 int ALL_MEMBER=2;
	 int INACTIVE_MEMBER=4;
	 int ALL_STAFF=5;
	 int STAFF_MEMBER=6;
	 
	 //Added on JAN22
	 String BN_MAIL_VARIABLE_CODE_BEAN="MailVariableCodeBean";
	 String BN_SMS_VARIABLE_CODE_BEAN="SMSVariableCodeBean";
	 
	 //Added on JAN23
	 String BN_SEND_TO_CODE_BEAN="SendToCodeBean";
	 int MEM_STATUS_ACTIVE=1;
	 int MEM_STATUS_INACTIVE=2;
	 String BN_MAIL_TEMP_NOTIFICATION="MailTemplateNotificationBean";
	 int MAIL_PAYMENT_RECEIPT=7;
	 int MAIL_INVOICE_RECEIPT=8;
	 int SEND=1;
	 
	 //Added on JAN30
	 int PRODUCTINVOICECANACELLED=4;
	 String BN_DASHBOARD_CONFIG="DashBoardConfigurationBean";
	 String BN_DASHBORD_CODE="DashBoardCodeBean";
	 
	 //Added on Feb10
	 String BN_ACTIVITY_TRACK_BEAN="ActivityTrackBean";
	 String BN_ENTITY_VIEW="EntityPlanMasterView";
	 String BN_ACTIVITY_TRACK_VIEW="ActivityTrackViewBean";
	 
	 //Added on 22FEB16
	 String ACTION_SHOW_BATCH="Show Batch";
	 
	 //Added on 9March16
	 String ST_CUST_PH_NO="{[CUST_PH_NO]}";
	 String ST_CUST_MOB_NO="{[CUST_MOB_NO]}";
	 String ST_CUST_EMAIL="{[CUST_EMAIL]}";
	 String ST_CENTER_NAME="{[CENTER_NAME]}";
	 String ST_CENTER_NO="{[CENTER_NO]}";
	 String ST_CENTER_ADDRESS="{[CENTER_ADDRESS]}";
	 String ST_CENTER_ADMIN="{[CENTER_ADMIN]}";
	 String ST_CENTER_EMAIL="{[CENTER_EMAIL]}";
	 String ST_DOB="{[DOB]}";
	 String ST_DOM="{[DOM]}";
	 String ST_DOC_NO="{[DOC_NO]}";
	 String ST_DOC_DATE="{[DOC_DATE]}";
	 String ST_DOC_AMT="{[DOC_AMT]}";
	 String ST_PAYMENT_SUMMARY="{[PAYMENT_SUMMARY]}";
	 String ST_PAYMENT_COLLECTED="{[PAYMENT_COLLECTED]}";
	 String ST_PAYMENT_DUE="{[PAYMENT_DUE]}";
     
	 String ST_NEWLY_JOINED="{[NEWLY_JOINED]}";
	 String ST_ENQUIRY="{[ENQUIRY]}";
	 String ST_MEMBER_ATTENDANCE="{[MEMER_ATTENDANCE]}";
	 String ST_STAFF_ATTENDANCE="{[STAFF_ATTENDANCE]}";
	 String ST_RENEWED="{[RENEWED]}";
	 
	 
	String FROM_PAGE="FROM_PAGE";
	String PREV_PAGE="PREV_PAGE";
	String PREVBACK_PAGE="PREVBACK_PAGE";
	int INTENQTOMEM=1;
	String ACTION_DEACTIVATE = "DEACTIVATE";
	int INTENQRPTTOENQMEM=2;
	int INTSTATUSNEW=1;
   
	String BN_BACKUP_CONF_BEAN = "BackupConfBean";
	String BACK_UP_DB = "BACK_UP_DB";
	int INTPAYMENT=3;
	//int INTMEMTOBACKENQ=4;
	int INTMEMREPOTOMEM=4;
	int INTMEMTOPAYMENT=5;
	int INTBALTORENEW=6;
	
	String ACTION_ENQFOLLOWUP = "ENQFOLLOWUP";
	
	//Fields from IUMC
	String BN_MM_ABOUT_UTHKRUSHTA = "AboutUthkrushtaMaster";
	String ACTION_GETALL = "GETALL";
	String BN_MM_STAFF_MASTER= "StaffMasterBean";
	
	//New Fields
	int INT_LOGGED_STAFF=0;
	int INT_LOGGED_MEMBER=1;
	int INT_CURRENTLY_LOGGED_IN=1;
	
	//New Fiels 21-04-2016
	int INT_ADMIN=2;
	
	//Jun3 Added due to reward points
	String BN_REWARD_POINTS_CFG_BEAN="RewardPointsConfigurationBean";
	
	 //Jun3
    int RT_INTREFERAL=1;
    int RT_INTWEEKLYATTENDANCE=2;
    int RT_INTBIRTHDAYANIVERSARY=3;
    int RT_INTSALES=4;
    
    //Jun4
    String BN_TRIGGERSUPCONFIG="TriggerSuperConfigurationBean";
    int TG_INT_WEEK=1;
    int TG_INT_MONTH=2;
    
    
  //4July16 Logg Reason
    long LNG_REFERAL=1;
    long LNG_BIRTHDAY=2;
    long LNG_ANIVERSARY=3;
    long LNG_ATTENDANCE=4;
    long LNG_SALES=5;
    long LNG_MEM_BAL_PAY=6;
    long LNG_MEM_RENEW_PAY=7;
    long LNG_MEM_RENEW_EXT=8;
    long LNG_SALE_REDEEM=9;
    long LNG_TRAINER_SERVICE_REDEM=10;		  
    
    //Add or redeem points
    long LNG_ADD_POINTS=1;
    long LNG_REDEEM_POINTS=2;
    long LNG_REVERT_BACK_POINTS=3;
    
    int INT_FEEDBACK_PENDING=12;
    int INT_FEEDBACK_TYPE_MEMBEER=3;
    
    //Payment Type;
    long LNG_MEM_NEW_PAYMENT=1;
    long LNG_MEM_BAL_PAYMENT=2;
    long LNG_MEM_RENEW_PAYMENT=3;
    
    String CLI_TASK_TYPE_NOTIFICATION = "notification";
    String CLI_TASK_TYPE_SCHEDULER = "scheduler";
    
    String BN_SERVICE_FORM_VIEW="ServiceFormTransactionViewBean";
    String BN_SERVICE_FORM="ServiceFormTransactionBean";
}


