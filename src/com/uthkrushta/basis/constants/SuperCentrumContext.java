package com.uthkrushta.basis.constants;

public class SuperCentrumContext {

	private static ThreadLocal<String> dbUserName = new ThreadLocal<>();
	
	private static ThreadLocal<String> dbPassword = new ThreadLocal<>();
	


	public static String getDbUserName() {
		return dbUserName.get();
	}

	public static void setDbUserName(String username) {
		dbUserName.set(username);
	}

	public static String getDbPassword() {
		return dbPassword.get();
	}

	public static void setDbPassword(String password) {
		dbPassword.set(password);
	}
	
	
}
