package com.uthkrushta.utils;

import java.util.Date;
import java.util.List;

import com.uthkrushta.basis.constants.IUMCConstants;
import com.uthkrushta.basis.constants.SuperCentrumContext;
import com.uthkrushta.mm.supreme.control.EntityPlanMasterView;
import com.uthkrushta.mm.utility.controller.NotificationController;
import com.uthkrushta.mm.utility.controller.SchedularController;

public class CronEntryPoint {

	public static void main(String[] args) {
		
		
		String username = "";
		String password = "";
		String taskType = "";
		
		try{
			username = args[0];
			password = args[1];
			taskType = args[2];
		} catch (Exception e){
			System.out.println("java -jar <xxx.jar> <username> <password> <tasktype>");
			return;
		}
		if(username.isEmpty() || password.isEmpty() || taskType.isEmpty() ){
			System.out.println("java -jar <xxx.jar> <username> <password> <tasktype>");
			return;
		}
		
		SuperCentrumContext.setDbUserName(username);
		
		SuperCentrumContext.setDbPassword(password);
		
		System.out.println("DEBUG Starting of CronEntryPoint at: "+new Date());
		
		EntityPlanMasterView objEPMV = new EntityPlanMasterView();
		
		// Get List of Centers from super centrum DB
		List lstCenters = DBUtilStatic.getActiveCenters(IUMCConstants.BN_ENTITY_VIEW);
		
		if(null == lstCenters){
			
			System.out.println("DEBUG No centers set up. exiting at: "+new Date());
			return;
		}
		
		if(taskType.equals(IUMCConstants.CLI_TASK_TYPE_NOTIFICATION))
		// Loop on ma_entity and connect to all active gyms having sms notification set to their plan
		 for(int i=0;i<lstCenters.size();i++){
			 objEPMV = (EntityPlanMasterView) lstCenters.get(i);
			 
			 
			 // Delegate to Notification Controller 
			 if(null == objEPMV) continue;
			 
			 System.out.println("DEBUG processing NotificationController for: "+objEPMV.getStrDbName()+" at: "+new Date());
			 
			 NotificationController.delegateCall(objEPMV);
			 

		 }
		else if(taskType.equals(IUMCConstants.CLI_TASK_TYPE_SCHEDULER))
		{
			// Loop on ma_entity and connect to all active gyms having sms notification set to their plan
		 for(int i=0;i<lstCenters.size();i++){
			 objEPMV = (EntityPlanMasterView) lstCenters.get(i);
			 
			 
			 // Delegate to Notification Controller 
			 if(null == objEPMV) continue;
	
			 
			 System.out.println("DEBUG processing SchedularController for: "+objEPMV.getStrDbName() +" at: "+new Date());
			 
			 SchedularController.delegateCall(objEPMV);
		 }
		}

		 System.out.println("DEBUG Ending of CronEntryPoint at: "+new Date());
	}

}
