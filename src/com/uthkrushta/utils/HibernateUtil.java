package com.uthkrushta.utils;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.uthkrushta.basis.constants.ISessionAttributes;
import com.uthkrushta.basis.constants.SuperCentrumContext;
import com.uthkrushta.mm.supreme.control.EntityPlanMasterView;

@SuppressWarnings("deprecation")
public class HibernateUtil 
{
	public static SessionFactory sessionFactory;
	public static  HashMap<String, SessionFactory> mapCenterSessionFactory = new  HashMap<String, SessionFactory>();
	public static  SessionFactory sessionFactorySupreme;
	/*static {
		try
		{
			Configuration conf = new Configuration();	
			
			sessionFactory  = conf.configure().buildSessionFactory();
			
		}
		catch(Throwable ex)
		{
			System.err.println("Cant Load the session Factory" + ex.toString());
			throw new ExceptionInInitializerError(ex);
		}
	}*/

	
	public SessionFactory getSessionFactory(HttpServletRequest request)
	{
		String strCenterCode = "";
		if(null != request.getSession().getAttribute(ISessionAttributes.CENTER_CODE)){
			strCenterCode =  (String)request.getSession().getAttribute(ISessionAttributes.CENTER_CODE);
		}else{
			strCenterCode = "";
		}
		sessionFactory = mapCenterSessionFactory.get(strCenterCode);
		return sessionFactory;
	}
	
	// To get session factory initialization without request object
	public SessionFactory getSessionFactory(String strCenterCode)
	{
		sessionFactory = mapCenterSessionFactory.get(strCenterCode);
		return sessionFactory;
	}
	
	public static SessionFactory getSessionFactorySupreme()
	{
		try
		{
			/*if(null != sessionFactorySupreme)
				sessionFactorySupreme.close();*/
			
			Configuration conf = new Configuration();
			conf.addResource("hibernate.cfg.xml");
			conf.addResource("mm.hbm.supreme.control.xml");
	        System.setProperty("hibernate.connection.url","jdbc:mysql://localhost:3306/super_centrum");
	        System.setProperty("hibernate.connection.driver_class","com.mysql.jdbc.Driver");
	        //System.setProperty("hibernate.connection.username","vsan_1979");//vsan_1979
	        //System.setProperty("hibernate.connection.password","Kushi@UthTech2013$*#");//Kushi@2015$
	        
	        System.setProperty("hibernate.connection.username",SuperCentrumContext.getDbUserName());//vsan_1979
	        System.setProperty("hibernate.connection.password",SuperCentrumContext.getDbPassword());//Kushi@2015$

	        if(null == SuperCentrumContext.getDbUserName() || null == SuperCentrumContext.getDbPassword() || SuperCentrumContext.getDbUserName().trim().length() == 0 || SuperCentrumContext.getDbPassword().trim().length() == 0) {
	        	System.setProperty("hibernate.connection.username","vsan_1979");
	        	System.setProperty("hibernate.connection.password","Kushi@2019$#@");
	        	
	        	
	        }       
	        
	        
	        conf.setProperties(System.getProperties());
	        conf.configure();
	        sessionFactorySupreme = conf.buildSessionFactory();
			return sessionFactorySupreme;
		}
		catch(Throwable ex)
		{
			System.err.println("Cant Load the session Factory" + ex.toString());
			throw new ExceptionInInitializerError(ex);
		}
	}
	
	//@SuppressWarnings("resource")
	public  SessionFactory getSessionFactoryForCenter(HttpServletRequest request,EntityPlanMasterView obj)
	{
		/*Writer writer = null;*/
		try
		{
			/*if(null != sessionFactory)
				sessionFactory.close();*/
			Configuration conf = new Configuration();
			String strCenterCode = "";
			
			if(null != request.getSession().getAttribute(ISessionAttributes.CENTER_CODE)){
				strCenterCode =  (String)request.getSession().getAttribute(ISessionAttributes.CENTER_CODE);
			}else if(null != obj && null != obj.getStrGymCode()){
				strCenterCode = obj.getStrGymCode().trim();
			}
			
			//strCenterCode = obj.getStrGymCode();
			conf.addResource("hibernate.cfg.center.xml");
			
			conf.addResource("mm.hbm.master.xml");
			conf.addResource("mm.hbm.framework.xml");
			conf.addResource("mm.hbm.code.xml");
			conf.addResource("mm.hbm.Configuration.xml");
			conf.addResource("mm.hbm.transactions.xml");
			conf.addResource("mm.hbm.gym.xml");
			conf.addResource("mm.hbm.utility.xml");
			/*conf.addResource("mm.hbm.product.xml");
			conf.addResource("mm.hbm.assets.xml"); Not Required */
	        System.setProperty("hibernate.connection.url",obj.getStrUrl()+obj.getStrDbName());
	        System.setProperty("hibernate.connection.driver_class",obj.getStrDriverClass());
	        System.setProperty("hibernate.connection.username",obj.getStrUserName());
	        System.setProperty("hibernate.connection.password",obj.getStrPassword());
	        /*writer.append("</hibernate-configuration>");*/
	        
	        
	        conf.setProperties(System.getProperties());;
	        conf.configure();
	       // sessionFactory = conf.buildSessionFactory();
	        if(null != mapCenterSessionFactory){
	        	// check if session already exists
	        	sessionFactory = mapCenterSessionFactory.get(strCenterCode);
	        	
	        }
	        if(null == sessionFactory){
        		sessionFactory = conf.buildSessionFactory();
        		mapCenterSessionFactory.put(strCenterCode, sessionFactory);
        	}
	        //request.getSession().setAttribute("sfm", sessionFactory);
			return sessionFactory;
		}
		catch(Throwable ex)
		{
			System.err.println("Cant Load the session Factory" + ex.toString());
			throw new ExceptionInInitializerError(ex);
		}
	}
	
	
	
	
	

	
	//Added to query for centre without request object
	public static SessionFactory getSessionFactoryForCenter(EntityPlanMasterView obj)
	{
		/*Writer writer = null;*/
		try
		{
			/*if(null != sessionFactory)
				sessionFactory.close();*/
			Configuration conf = new Configuration();
			String strCenterCode = "";
			
			 if(null != obj && null != obj.getStrGymCode()){
				strCenterCode = obj.getStrGymCode().trim();
			}
			
			//strCenterCode = obj.getStrGymCode();
			conf.addResource("hibernate.cfg.xml");
			
			conf.addResource("mm.hbm.master.xml");
			conf.addResource("mm.hbm.framework.xml");
			conf.addResource("mm.hbm.code.xml");
			conf.addResource("mm.hbm.Configuration.xml");
			conf.addResource("mm.hbm.transactions.xml");
			conf.addResource("mm.hbm.gym.xml");
			conf.addResource("mm.hbm.utility.xml");
			/*conf.addResource("mm.hbm.product.xml");
			conf.addResource("mm.hbm.assets.xml");Not required*/
	        System.setProperty("hibernate.connection.url",obj.getStrUrl()+obj.getStrDbName());
	        System.setProperty("hibernate.connection.driver_class",obj.getStrDriverClass());
	        System.setProperty("hibernate.connection.username",obj.getStrUserName());
	        System.setProperty("hibernate.connection.password",obj.getStrPassword());
	        /*writer.append("</hibernate-configuration>");*/
	        
	        
	        conf.setProperties(System.getProperties());;
	        conf.configure();
	       // sessionFactory = conf.buildSessionFactory();
	        if(null != mapCenterSessionFactory){
	        	// check if session already exists
	        	sessionFactory = mapCenterSessionFactory.get(strCenterCode);
	        	
	        }
	        if(null == sessionFactory){
        		sessionFactory = conf.buildSessionFactory();
        		mapCenterSessionFactory.put(strCenterCode, sessionFactory);
        	}
			return sessionFactory;
		}
		catch(Throwable ex)
		{
			System.err.println("Cant Load the session Factory" + ex.toString());
			throw new ExceptionInInitializerError(ex);
		}
	}
	
}
