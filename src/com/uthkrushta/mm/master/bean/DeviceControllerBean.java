package com.uthkrushta.mm.master.bean;

import java.util.ArrayList;
import java.util.List;

public class DeviceControllerBean {

	public  DeviceControllerBean() {
		lstBiometricIDs = new ArrayList<BiometricUserBean>();
	}
	
	private int intActionID;
	private String strCenterCode;
	private long lngCompanyID;
	private List<BiometricUserBean> lstBiometricIDs;
	
	
	
	
	public int getIntActionID() {
		return intActionID;
	}
	public void setIntActionID(int intActionID) {
		this.intActionID = intActionID;
	}
	public String getStrCenterCode() {
		return strCenterCode;
	}
	public void setStrCenterCode(String strCenterCode) {
		this.strCenterCode = strCenterCode;
	}
	public long getLngCompanyID() {
		return lngCompanyID;
	}
	public void setLngCompanyID(long lngCompanyID) {
		this.lngCompanyID = lngCompanyID;
	}
	public List<BiometricUserBean> getLstBiometricIDs() {
		return lstBiometricIDs;
	}
	public void setLstBiometricIDs(List<BiometricUserBean> lstBiometricIDs) {
		this.lstBiometricIDs = lstBiometricIDs;
	}
	@Override
	public String toString() {
		return "DeviceControllerBean [intActionID = "+intActionID+", strCenterCode=" + strCenterCode + ", lngCompanyID=" + lngCompanyID
				+ ", lstBiometricIDs=" + lstBiometricIDs + "]";
	}
	
	
	
	
}
