package com.uthkrushta.mm.utility.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;

import org.codehaus.jackson.map.ObjectMapper;

import com.ibm.icu.text.DateFormat;
import com.ibm.icu.text.SimpleDateFormat;
import com.ibm.icu.util.Calendar;
import com.uthkrushta.basis.constants.ILabelConstants;
import com.uthkrushta.basis.constants.IUMCConstants;
import com.uthkrushta.mm.configuration.bean.TimeZoneConfigurationBean;
import com.uthkrushta.mm.code.bean.AppointmentStatusCodeBean;
import com.uthkrushta.mm.code.bean.MailVariableCodeBean;
import com.uthkrushta.mm.code.bean.SMSVariableCodeBean;
import com.uthkrushta.mm.configuration.bean.GlobalSettingBean;
import com.uthkrushta.mm.configuration.bean.RewardPointsConfigurationBean;
import com.uthkrushta.mm.frameWork.bean.DateRangeBean;
import com.uthkrushta.mm.gym.bean.AttendanceGymBean;
import com.uthkrushta.mm.master.bean.BiometricUserBean;
import com.uthkrushta.mm.master.bean.CompanyMasterBean;
import com.uthkrushta.mm.master.bean.DeviceControllerBean;
import com.uthkrushta.mm.master.bean.FinancialYearMasterBean;
import com.uthkrushta.mm.master.bean.MasterMemberBeanView;
import com.uthkrushta.mm.master.bean.MemberMasterBean;
import com.uthkrushta.mm.master.bean.StaffMasterBean;
import com.uthkrushta.mm.master.bean.StaffRoleMasterBeanView;
import com.uthkrushta.mm.supreme.control.EntityPlanMasterView;
import com.uthkrushta.mm.supreme.control.TriggerSuperConfigurationBean;
import com.uthkrushta.mm.transaction.bean.AppointmentHeaderTransactionBean;
import com.uthkrushta.mm.transaction.bean.LoggTransactionBean;
import com.uthkrushta.mm.transaction.bean.MemberPauseTransactionBean;
import com.uthkrushta.mm.transaction.bean.MemberPaymentTransactionBean;
import com.uthkrushta.mm.transaction.bean.PlanSubsPaidMemNameTransactionViewBean;
import com.uthkrushta.mm.transaction.bean.ServiceFormTransactionViewBean;
import com.uthkrushta.mm.utility.bean.SMSTemplateNotificationBean;
import com.uthkrushta.utils.DBUtil;
import com.uthkrushta.utils.DBUtilStatic;
import com.uthkrushta.utils.SendMailToCenter;
import com.uthkrushta.utils.UMBException;
import com.uthkrushta.utils.WebUtil;
import org.codehaus.jackson.map.ObjectMapper;

public class NotificationController extends HttpServlet {
	
	public static String strTimeGMT="";
	ObjectMapper mapper;
	
	
	
	public static boolean delegateCall(EntityPlanMasterView objEPMV){
		boolean blnExecuted = false;
		
		strTimeGMT=getTimeZone(objEPMV);
		Date dtCurrent = WebUtil.getCurrentDate(strTimeGMT);
		SimpleDateFormat sdfTime =new SimpleDateFormat("HH:mm:ss");
		String strCurrentTime=sdfTime.format(dtCurrent);
		String strGivenStartTime="21:00:00";
		String strGivenEndTime="23:59:59";
		Date dtCurrentTime=null;
		Date dtGivenStartTime=null;
		Date dtGivenEndTime=null;
		try {
			 dtCurrentTime=sdfTime.parse(strCurrentTime);
			 dtGivenStartTime=sdfTime.parse(strGivenStartTime);
			 dtGivenEndTime=sdfTime.parse(strGivenEndTime);
			 if(dtCurrentTime.compareTo(dtGivenStartTime) >= 0 && dtCurrentTime.compareTo(dtGivenEndTime) <= 0)
			 {
				// If the center has SMS pack
				 if(objEPMV.getIntIsTransactionalSMS() == 1){
					//Working blnExecuted = sendSMSToAdmin(objEPMV);
				//	 blnExecuted =sendSMSToAdminDailySummary(objEPMV);
				 }
			 }
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		/*Previosly commented*/			
		//blnExecuted=sendPlanExpiryRemainderToMembers(objEPMV);
		
		/*blnExecuted=sendWishesToMembers( objEPMV);
		blnExecuted=sendPlanExpiryRemaindersToMembersBfrGivenExpDays(objEPMV);*/
		
		/*blnExecuted=sendPaymentDueMembershipRemainderToMembers(objEPMV);
		blnExecuted=sendPaymentDueServiceRemainderToMembers(objEPMV);
		blnExecuted=sendAlertToAbsentMembers(objEPMV);
		blnExecuted=sendAppointmentReminderSmsToMember(objEPMV);
		blnExecuted=sendAppointmentReminderSmsToStaff(objEPMV);*/
		
		blnExecuted=sendPlanExpiryRemaindersToMembersOnDay(objEPMV);
		blnExecuted=sendPlanExpiryRemaindersToMembersBfrOneDay(objEPMV);
		blnExecuted=sendPlanExpiryRemaindersToMembersAfterDay(objEPMV);
		
		if(null!=objEPMV && objEPMV.getIntIsMail()==1 && isEmailEnabledForCenter(objEPMV))
		{
			sendPendingFeedbackMemberMail(objEPMV);
		}
		return blnExecuted;
	}

	
	

	public static boolean sendWishesToMembers(EntityPlanMasterView objEPMV){
		List lstBirthDayMemberList = null;
		List lstAnniversaryMemberList = null;
		List lstPlanExpirySevenBfrMemberList=null;
		
		 Calendar c = Calendar.getInstance();
		 c.add(Calendar.DATE, 7);  // number of days to add
		 String strNextsevenDate= WebUtil.formatDate(c.getTime(), IUMCConstants.DB_DATE_FORMAT);
		
		String strTableName = IUMCConstants.BN_MEMBER_MASTER_BEAN;
		String strSelectList = "";
		String  strBDWhereClause =IUMCConstants.GET_ACTIVE_ROWS+" and DAY(dtDOB)=DAY('"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_FORMAT)+"') and MONTH(dtDOB)=MONTH('"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_FORMAT)+"') and intSMSNotification = 1"; //IUBESPOKEConstants.GET_ACTIVE_ROWS;
		String  strADWhereClause = IUMCConstants.GET_ACTIVE_ROWS+" and DAY(dtDOM)=DAY('"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_FORMAT)+"') and MONTH(dtDOM)=MONTH('"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_FORMAT)+"') and intSMSNotification = 1";//IUBESPOKEConstants.GET_ACTIVE_ROWS;
		String  strMemExpWhereClause=IUMCConstants.GET_ACTIVE_ROWS+" and dtToDate = '"+strNextsevenDate+"'  and intSMSNotification = 1";
		String strSortClause = "strName, strLastName";
		boolean blnExecuted = false;
		
       
		
		MemberMasterBean objMemberMasterBean = new MemberMasterBean();
		
		// Get SMS configuration
		 String strWhereSMS=IUMCConstants.GET_ACTIVE_ROWS+" and intSMSType = "+IUMCConstants.TRANSACTIONAL;
		 SMSVariableCodeBean objSMSVar=(SMSVariableCodeBean) DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_SMS_VARIABLE_CODE_BEAN, strSelectList, strWhereSMS, "");
		 if(null==objSMSVar) return false;
		
		 // Get Members who has b'day and anniversary and sms notification set to true
		lstBirthDayMemberList = DBUtilStatic.getRecordsFromCenter(objEPMV, strTableName, strSelectList, strBDWhereClause, strSortClause);
		lstAnniversaryMemberList = DBUtilStatic.getRecordsFromCenter(objEPMV, strTableName, strSelectList, strADWhereClause, strSortClause);
		
		MemberMasterBean objMMB=new MemberMasterBean();
		// Loop on the list and send SMS wishes
		//Bday Loop
		  if(null!=lstBirthDayMemberList && lstBirthDayMemberList.size()>0)  
		  {
		// for each member record, get appropriate sms template for bday
		   for(int i=0;i<lstBirthDayMemberList.size();i++)
		   {
			   objMMB=new MemberMasterBean();
			   objMMB=(MemberMasterBean)lstBirthDayMemberList.get(i);
			   if(null!=objMMB)
			   {
				   sendMessage(objMMB,objSMSVar,objEPMV,IUMCConstants.BIRTHDAY);
			   }
		   }
		// send sms
				
		  }
		// End of Bday Loop
		
		//Ann loop
		
		  if(null!=lstAnniversaryMemberList && lstAnniversaryMemberList.size()>0)  
		  {
		// for each member record, get appropriate sms template for Aday
			   for(int i=0;i<lstAnniversaryMemberList.size();i++)
			   {
				   objMMB=new MemberMasterBean();
				   objMMB=(MemberMasterBean)lstAnniversaryMemberList.get(i);
				   if(null!=objMMB)
				   {
					   sendMessage(objMMB,objSMSVar,objEPMV,IUMCConstants.ANIVERSARY);
				   }
			   }
		// send sms
				
		  }
		
		
		//End of Ann loop
		  
			  
		return blnExecuted;
	}
	
	
	
	
	
	//main calling function
	private static boolean sendPlanExpiryRemaindersToMembersBfrGivenExpDays(EntityPlanMasterView objEPMV){
		boolean blnSendToPlanExpMem=false;
		 GlobalSettingBean objGSB=(GlobalSettingBean) DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_GLOBAL_SETTING, "", IUMCConstants.GET_ACTIVE_ROWS, "");
		 int intPlanExpiryBeforeDays=0;
		 if(null!=objGSB){
			 intPlanExpiryBeforeDays=objGSB.getIntPlanExpiryNotificationDays();
		 }
		 
		return sendPlanExpiryRemainderToMembers(objEPMV,intPlanExpiryBeforeDays,IUMCConstants.RENEWAL);
	}
	
	//main calling function
	private static boolean sendPlanExpiryRemaindersToMembersBfrOneDay(EntityPlanMasterView objEPMV){
		boolean blnSendToPlanExpMem=false;
		 int intPlanExpiryBeforeOneDay=1;
		return sendPlanExpiryRemainderToMembers(objEPMV,intPlanExpiryBeforeOneDay,IUMCConstants.RENEWAL);
	}
	
	//main calling function
		private static boolean sendPlanExpiryRemaindersToMembersOnDay(EntityPlanMasterView objEPMV){
			boolean blnSendToPlanExpMem=false;
			 int intPlanExpiryBeforeOnDay=0;
			return sendPlanExpiryRemainderToMembers(objEPMV,intPlanExpiryBeforeOnDay,IUMCConstants.TODAY_EXPIRY);
		}
		//main calling function
				private static boolean sendPlanExpiryRemaindersToMembersAfterDay(EntityPlanMasterView objEPMV){
					boolean blnSendToPlanExpMem=false;
					 int intPlanExpiryAfterDay=-1;
					return sendPlanExpiryRemainderToMembers(objEPMV,intPlanExpiryAfterDay,IUMCConstants.PLAN_EXPIRED);
				}	
	
	private static boolean sendPlanExpiryRemainderToMembers(EntityPlanMasterView objEPMV,int intDays, int intMSGType) {
		// TODO Auto-generated method stub
		
		List lstPlanExpirySevenBfrMemberList=null;
		boolean blnSendToPlanExpMem=false;
		 Calendar c = Calendar.getInstance();
		// GlobalSettingBean objGSB=(GlobalSettingBean) DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_GLOBAL_SETTING, "", IUMCConstants.GET_ACTIVE_ROWS, "");
		 /*int intPlanExpiryBeforeDays=0;
		 if(null!=objGSB){
			 intPlanExpiryBeforeDays=objGSB.getIntPlanExpiryNotificationDays();
		 }*/
		 c.add(Calendar.DATE, intDays);  // number of days to add
		 String strNextsevenDate= WebUtil.formatDate(c.getTime(), IUMCConstants.DB_DATE_FORMAT);
		 
		// String  strMemExpWhereClause=IUMCConstants.GET_ACTIVE_ROWS+" and dtToDate = '"+strNextsevenDate+"'  and intSMSNotification = 1 and intIsCurrent = 1";
		 // modified om 10-09-2019
		 String  strMemExpWhereClause=IUMCConstants.GET_ACTIVE_ROWS+" and dtPlanEndDate = '"+strNextsevenDate+"'  and intSMSNotification = 1";
		 
		 lstPlanExpirySevenBfrMemberList=DBUtilStatic.getRecordsFromCenter(objEPMV, IUMCConstants.BN_MEMBER_MASTER_BEAN_VIEW, "", strMemExpWhereClause, "");
		// Get SMS configuration
				 String strWhereSMS=IUMCConstants.GET_ACTIVE_ROWS+" and intSMSType = "+IUMCConstants.TRANSACTIONAL;
				 SMSVariableCodeBean objSMSVar=(SMSVariableCodeBean) DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_SMS_VARIABLE_CODE_BEAN, "", strWhereSMS, "");
				 if(null==objSMSVar) return false;
		 
				//Members with plan expiring in seven days
				 MasterMemberBeanView objMMBV= new MasterMemberBeanView();
				   if(null!=lstPlanExpirySevenBfrMemberList)
				   {
				     //for loop of the list of members
					 for(int i=0;i<lstPlanExpirySevenBfrMemberList.size();i++)
					 {
						 objMMBV=new MasterMemberBeanView(); 
						 objMMBV = (MasterMemberBeanView)lstPlanExpirySevenBfrMemberList.get(i);
						 
		                if(null!=objMMBV)
		                {
				           //for each member get the appropriate sms template
		                	String strWhereSMSTemplate=IUMCConstants.GET_ACTIVE_ROWS+" and lngMsgTypeID = "+intMSGType+" and intSend = 1 and lngCompanyID= "+objMMBV.getLngCompanyID();
		 				    SMSTemplateNotificationBean objSMSTNB=(SMSTemplateNotificationBean)DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_SMS_TEMP_NOTIFICATION, "", strWhereSMSTemplate, "");
		 				   
		 				   String strWhereCompany=IUMCConstants.GET_ACTIVE_ROWS+" and lngID = "+objMMBV.getLngCompanyID();
		 				   CompanyMasterBean objCMB=(CompanyMasterBean)DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_COMPANY_MASTER_BEAN, "", strWhereCompany, "");
		                   
		 				  //look
						   StringBuilder builder = new StringBuilder();
						   String strSMSAPI = "";
							 if(null!=objSMSVar && objSMSVar.getStrName().trim().length()>0)
							 {
							 strSMSAPI=objSMSVar.getStrName();
							 }
							 String strSMSAPIForCust = strSMSAPI ;
							 URL url = null; 
							 HttpURLConnection uc;
							 //look
		 				    
							 if(null!=objSMSTNB)
							 {
								//look
						         String strContent="";
						         String strSpecificContent = "";
							     strContent =objSMSTNB.getStrContent();
								 strContent = strContent.replaceAll(" ", "%20");
								 strSpecificContent=strContent;
								 
								 //To get the Admin Name of the center
								 String strWhereStaff= IUMCConstants.GET_ACTIVE_ROWS+" and lngID = "+objCMB.getLngAdminID();
								 StaffMasterBean objAdminStaff=(StaffMasterBean) DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_STAFF_MASTER_BEAN, "", strWhereStaff, "");
								 String strAdminName="";
								 if(null!=objAdminStaff && objAdminStaff.getStrName().trim().length()>0)
								 {
									 strAdminName=objAdminStaff.getStrName()+" "+objAdminStaff.getStrLastName();
								 }
							   //look
								 
								 
								 // replace the sms template with actual text : replacing tokens to actual values
								   
									
								 String strName=objMMBV.getStrMemberName()+" "+objMMBV.getStrLastName();
						 			if(null!=strName && strName.trim().length()>0)
						 			 {
						 			    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_NAME, strName);
						 			 }
						 			 else
						 			 {
						 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_NAME,"");
						 			 }
						 			    
						 			  strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_ADDRESS, "");
						 			  strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_PH_NO, "");
						 			   
						 			  if(null!=objMMBV.getStrMobileNumber() &&  objMMBV.getStrMobileNumber().trim().length()>0)
						 			  {
						 			    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_MOB_NO, objMMBV.getStrMobileNumber());
						 			  }
						 			  else
						 			  {
						 				 strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_MOB_NO,"");
						 			  }
						 			 if(null!=objMMBV.getStrEmailID() &&  objMMBV.getStrEmailID().trim().length()>0)
						 			  {
							 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_EMAIL, objMMBV.getStrEmailID());
						 			  }
						 			 else
						 			 {
						 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_EMAIL,"");
						 			 }
						 			 
						 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOB,"");
						 		    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOM,"");
						 			
						 		    if(null!=objMMBV.getDtPlanEndDate()){
						 		    	strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOC_DATE, ""+objMMBV.getDtPlanEndDate());
						 		    }else{
						 		    	strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOC_DATE, "");
						 		    }
						 		    
						 			 if(null!=objCMB.getStrName() &&  objCMB.getStrName().trim().length()>0)
						 			  {
							 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NAME, objCMB.getStrName());
						 			  }	
						 			 else
						 			 {
						 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NAME, "");
						 			 }
						 			if(null!=objCMB.getStrPrimaryMobileNumber() &&  objCMB.getStrPrimaryMobileNumber().trim().length()>0)
						 			  {
							 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NO, objCMB.getStrPrimaryMobileNumber());
						 			  }
						 			else
						 			{
						 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NO,"");
						 			}
						 			if(null!=objCMB.getStrAddress() &&  objCMB.getStrAddress().trim().length()>0)
						 			  {
							 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADDRESS, objCMB.getStrAddress());
						 			  }
						 			else
						 			{
						 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADDRESS,"");
						 			}
						 			if(null!=strAdminName &&  strAdminName.trim().length()>0)
						 			  {
							 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADMIN, strAdminName);
						 			  }
						 			else
						 			{
						 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADMIN, "");
						 			}
						 			if(null!=objCMB.getStrContactEmail() &&  objCMB.getStrContactEmail().trim().length()>0)
						 			  {
							 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_EMAIL, objCMB.getStrContactEmail());
						 			  }
						 			else
						 			{
						 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_EMAIL, "");
						 			}
						 			 
						 			 
						 			 
						 			 strSpecificContent = strSpecificContent.replaceAll(" ", "%20");
									 strSMSAPIForCust=strSMSAPIForCust.replace(IUMCConstants.ST_SEND_TO,objMMBV.getStrMobileNumber());
									 strSMSAPIForCust = strSMSAPIForCust.replace(IUMCConstants.ST_SMS_TEXT, builder.append(strSpecificContent).toString());
						         
								 System.out.print(strSMSAPIForCust);
							     try {
									url = new URL(strSMSAPIForCust);
									System.out.println(strSMSAPIForCust);
									uc = (HttpURLConnection)url.openConnection();
									uc.getResponseMessage();
								    uc.disconnect();
								    System.out.print(strSMSAPIForCust);
								    blnSendToPlanExpMem= true;
								} catch (MalformedURLException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
									blnSendToPlanExpMem= false;
								}
								catch (IOException e) {
							// TODO Auto-generated catch block
									e.printStackTrace();
									blnSendToPlanExpMem= false;
								}
							 }
						 	 
							 
		                }
					 }
				  //End of loop
				   }
				 //End of Members with plan expiring in seven days
		
		 
		 return blnSendToPlanExpMem;
	}
	

	private static boolean sendAlertToAbsentMembers(EntityPlanMasterView objEPMV) {
		// TODO Auto-generated method stub
		
		String  strSortClause=" strName,strLastName";
		AttendanceGymBean objAttBean = new AttendanceGymBean();
		List lstAbsentMembers=new ArrayList();
		boolean blnSendToAbsent=false;
		
		
      GlobalSettingBean objGSB = (GlobalSettingBean)DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_GLOBAL_SETTING, "", IUMCConstants.GET_ACTIVE_ROWS, "");
		
		//first get the list of members who are absent in a week
		List lstMember=DBUtilStatic.getRecordsFromCenter(objEPMV, IUMCConstants.BN_MEMBER_MASTER_BEAN, "", IUMCConstants.GET_ACTIVE_ROWS+" and lngMemberStatus="+IUMCConstants.MEM_STATUS_ACTIVE, strSortClause);
		if(null!=lstMember && lstMember.size()>0)
		{
			for(int i=0;i<lstMember.size();i++)
			{
				MemberMasterBean objMMBA=new MemberMasterBean();
				objMMBA=(MemberMasterBean)lstMember.get(i);
			     if(null!=objMMBA)
			     {
			    	 int intMemberAbsentCount=0;
						
			    	
			    	     
			    	 Date dtCurrentDate=WebUtil.getCurrentDate(strTimeGMT);
			    	 Date dtLastAttendedDate=objMMBA.getDtLastAttendedDate();
			    	 int intAbsentDays =0;
			    	 
			    	 //Checking whether the extension date is lesser than the current date and last attended date is also less than extension toDate
			    	 MemberPauseTransactionBean objMPTB =(MemberPauseTransactionBean)DBUtilStatic.getRecordFromCenter(objEPMV,IUMCConstants.BN_MEMBER_PAUSE_TRANSACTION_BEAN,"", " lngMemeberID = "+objMMBA.getLngID()+" and lngPauseTypeID="+IUMCConstants.FREEZED+" and intLatestRecord=1", ""); 
    				 if(null!=objMPTB && null!=objMPTB.getDtTo() && objMPTB.getDtTo().getTime() < dtCurrentDate.getTime() && null!=objMMBA.getDtLastAttendedDate() && objMMBA.getDtLastAttendedDate().getTime()< objMPTB.getDtTo().getTime()){
    					 intAbsentDays= (int)( (dtCurrentDate.getTime() - (objMPTB.getDtTo().getTime()+1* 24 * 60 * 60 * 1000))/ (1000 * 60 * 60 * 24) );
    				 }else
			    			 if(null!=objMMBA.getDtLastAttendedDate())
			    			 {
			    				 intAbsentDays= (int)( (dtCurrentDate.getTime() - dtLastAttendedDate.getTime())/ (1000 * 60 * 60 * 24) );
			    			 }
			    				else
			    				 if(null!=objMMBA.getDtDOJ()){//Checking if he is a new joinee and absent 
			    				 intAbsentDays= (int)( (dtCurrentDate.getTime() - objMMBA.getDtDOJ().getTime())/ (1000 * 60 * 60 * 24) );
			    				 }
			    			 
			    			 
			    	 int intIntervalSMSDays=0;
			    	        if(null!= objMMBA.getDtLastSentSMS())
			    	        {
			    	        	intIntervalSMSDays= (int)((dtCurrentDate.getTime() - objMMBA.getDtLastSentSMS() .getTime())/ (1000 * 60 * 60 * 24) );
			    	        }
			    	    if(intAbsentDays > objGSB.getIntContinuousAbsent() && objMMBA.getIntSMSSentCount() <= objGSB.getIntNoOfAlertSMS() && (intIntervalSMSDays >objGSB.getIntSMSIntervalDays()||intIntervalSMSDays==0) )   
			    	    {
			    	    	lstAbsentMembers.add(objMMBA);
			    	        int	intMemberSMSCount=objMMBA.getIntSMSSentCount()+1;
			    	    	String strMemberUpdateClause="intSMSSentCount = "+intMemberSMSCount+",dtLastSentSMS = '"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_FORMAT)+"'";
			    	    	String strWhereClause="lngID ="+objMMBA.getLngID();
			    	    	
			    	    	try {
								DBUtilStatic.updateRecordsOfCenter(objEPMV, IUMCConstants.BN_MEMBER_MASTER_BEAN, strMemberUpdateClause, strWhereClause);
							} catch (UMBException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
			    	    }
			    	 
						
			     }	
			}
		}
		//End of getting Absent members list
		
		
		String strTableName = IUMCConstants.BN_MEMBER_MASTER_BEAN;
		String strSelectList = "";
		
         MemberMasterBean objMemberMasterBean = new MemberMasterBean();
		
		// Get SMS configuration
		 String strWhereSMS=IUMCConstants.GET_ACTIVE_ROWS+" and intSMSType = "+IUMCConstants.TRANSACTIONAL;
		 SMSVariableCodeBean objSMSVar=(SMSVariableCodeBean) DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_SMS_VARIABLE_CODE_BEAN, strSelectList, strWhereSMS, "");
		 if(null==objSMSVar) return false;
		
		 
		
		MemberMasterBean objMMB=new MemberMasterBean();
		// Loop on the list and send SMS Alert
		//Absent Member Loop
		  if(null!=lstAbsentMembers && lstAbsentMembers.size()>0)  
		  {
		// for each member record, get appropriate sms template for Absent
		   for(int i=0;i<lstAbsentMembers.size();i++)
		   {
			   objMMB=new MemberMasterBean();
			   objMMB=(MemberMasterBean)lstAbsentMembers.get(i);
			   if(null!=objMMB)
			   {
				   sendMessage(objMMB, objSMSVar, objEPMV, IUMCConstants.ABSENT);
				   
			   }
		   }
		// send sms
				
		  }
		// End of Bday Loop
		
		
		return blnSendToAbsent;
	}
	
	
	
	
	
	
	public static List getMemberRecords(EntityPlanMasterView objEPMV){
		List lstMemberList = null;
		String strTableName = IUMCConstants.BN_MEMBER_MASTER_BEAN;
		String strSelectList = "";
		String  strWhereClause = IUMCConstants.GET_ACTIVE_ROWS;
		String strSortClause = "strName, strLastName";
		
		// Get Session for center and query member table
		return DBUtilStatic.getRecordsFromCenter(objEPMV, strTableName, strSelectList, strWhereClause, strSortClause);
		
		
		
		
	}
	
	
	
	public static String getTimeZone(EntityPlanMasterView objEPMV)
	{
	GlobalSettingBean objGSB = (GlobalSettingBean)	DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_GLOBAL_SETTING, "", IUMCConstants.GET_ACTIVE_ROWS, "");
			
			if( null!=objGSB )
			{
				//To get the selected time zone of different countries
				 List lstTimeZone=DBUtilStatic.getRecordsFromCenter(objEPMV, IUMCConstants.BN_TIME_ZONE, "", IUMCConstants.GET_ACTIVE_ROWS, "");
		         
		         
		         if(null!=lstTimeZone)
		         {
		        	 
		        	 for(int i=0;i<lstTimeZone.size();i++)
		        	 {
		        	    TimeZoneConfigurationBean objTZCB=(TimeZoneConfigurationBean)lstTimeZone.get(i);
		        	    if(null!=objTZCB && objTZCB.getLngID()==objGSB.getLngTimeZone())
		        	    {
		        	    	if(null!=objTZCB.getStrTimeZone())
		        	    	{
		        	    		strTimeGMT=objTZCB.getStrTimeZone();
		        	    	}
		        	    }
		        	 }
		         }
				//Getting  the time zone of different countries ends
			}
			
			return strTimeGMT;
	}
	
	
	//Replace the tokens and send message for bday and aniversary and absent members
	public static void sendMessage(MemberMasterBean objMMB, SMSVariableCodeBean objSMSVar,EntityPlanMasterView objEPMV,long lngMessageType){
		
		boolean blnExecuted = false;
		
		 String strWhereSMSTemplate=IUMCConstants.GET_ACTIVE_ROWS+" and lngMsgTypeID = "+lngMessageType+" and intSend = 1 and lngCompanyID ="+objMMB.getLngCompanyID();
		   SMSTemplateNotificationBean objSMSTNB=(SMSTemplateNotificationBean)DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_SMS_TEMP_NOTIFICATION, "", strWhereSMSTemplate, "");
		   
		   String strWhereCompany=IUMCConstants.GET_ACTIVE_ROWS+" and lngID = "+objMMB.getLngCompanyID();
		   CompanyMasterBean objCMB=(CompanyMasterBean)DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_COMPANY_MASTER_BEAN, "", strWhereCompany, "");
		  
		   
		   
		   
		   
		   //look
		   StringBuilder builder = new StringBuilder();
		   String strSMSAPI = "";
			 if(null!=objSMSVar && objSMSVar.getStrName().trim().length()>0)
			 {
			 strSMSAPI=objSMSVar.getStrName();
			 }
			 String strSMSAPIForCust = strSMSAPI ;
			 URL url = null; 
			 HttpURLConnection uc;
			 //look
		   if(null!=objSMSTNB)
		   {
			   //look
		         String strContent="";
		         String strSpecificContent = "";
			     strContent =objSMSTNB.getStrContent();
				 strContent = strContent.replaceAll(" ", "%20");
				 strSpecificContent=strContent;
				 
				 //To get the Admin Name of the center
				 String strWhereStaff= IUMCConstants.GET_ACTIVE_ROWS+" and lngID = "+objCMB.getLngAdminID();
				 StaffMasterBean objAdminStaff=(StaffMasterBean) DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_STAFF_MASTER_BEAN, "", strWhereStaff, "");
				 String strAdminName="";
				 if(null!=objAdminStaff && objAdminStaff.getStrName().trim().length()>0)
				 {
					 strAdminName=objAdminStaff.getStrName()+" "+objAdminStaff.getStrLastName();
				 }
			   //look
			   
       // replace the sms template with actual text : replacing tokens to actual values
			   
				
		 			 String strName=objMMB.getStrName()+" "+objMMB.getStrLastName();
		 			 
		 			 if(null!=strName && strName.trim().length()>0)
		 			 {
		 			    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_NAME, strName);
		 			 }
		 			 else
		 			 {
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_NAME,"");
		 			 }
		 			    if(null!=objMMB.getStrAddress() &&  objMMB.getStrAddress().trim().length()>0)
		 			    {
		 			    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_ADDRESS, objMMB.getStrAddress());
		 			    }
		 			    else
		 			    {
		 			    	strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_ADDRESS, "");
		 			    }
		 			    
		 			   if(null!=objMMB.getStrLandlineNumber() &&  objMMB.getStrLandlineNumber().trim().length()>0)
		 			    {
		 			    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_PH_NO, objMMB.getStrLandlineNumber());
		 			    }
		 			   else
		 			   {
		 				  strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_PH_NO, "");
		 			   }
		 			  if(null!=objMMB.getStrMobileNumber() &&  objMMB.getStrMobileNumber().trim().length()>0)
		 			  {
		 			    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_MOB_NO, objMMB.getStrMobileNumber());
		 			  }
		 			  else
		 			  {
		 				 strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_MOB_NO,"");
		 			  }
		 			 if(null!=objMMB.getStrEmailID() &&  objMMB.getStrEmailID().trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_EMAIL, objMMB.getStrEmailID());
		 			  }
		 			 else
		 			 {
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_EMAIL,"");
		 			 }
		 			 if(null!=objMMB.getDtDOB())
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOB, objMMB.getDtDOB()+"");
		 			  }	
		 			 else
		 			 {
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOB,"");
		 			 }
		 			if(null!=objMMB.getDtDOM() )
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOM, objMMB.getDtDOM()+"");
		 			  }
		 			else
		 			{
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOM,"");
		 			}
		 			 if(null!=objCMB.getStrName() &&  objCMB.getStrName().trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NAME, objCMB.getStrName());
		 			  }	
		 			 else
		 			 {
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NAME, "");
		 			 }
		 			if(null!=objCMB.getStrPrimaryMobileNumber() &&  objCMB.getStrPrimaryMobileNumber().trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NO, objCMB.getStrPrimaryMobileNumber());
		 			  }
		 			else
		 			{
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NO,"");
		 			}
		 			if(null!=objCMB.getStrAddress() &&  objCMB.getStrAddress().trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADDRESS, objCMB.getStrAddress());
		 			  }
		 			else
		 			{
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADDRESS,"");
		 			}
		 			if(null!=strAdminName &&  strAdminName.trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADMIN, strAdminName);
		 			  }
		 			else
		 			{
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADMIN, "");
		 			}
		 			if(null!=objCMB.getStrContactEmail() &&  objCMB.getStrContactEmail().trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_EMAIL, objCMB.getStrContactEmail());
		 			  }
		 			else
		 			{
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_EMAIL, "");
		 			}
		 			
		 			 
		 			 
					 strSpecificContent = strSpecificContent.replaceAll(" ", "%20");
					 strSMSAPIForCust=strSMSAPIForCust.replace(IUMCConstants.ST_SEND_TO,objMMB.getStrMobileNumber());
					 strSMSAPIForCust = strSMSAPIForCust.replace(IUMCConstants.ST_SMS_TEXT, builder.append(strSpecificContent).toString());
			           
					 System.out.print(strSMSAPIForCust);
						try {
							url = new URL(strSMSAPIForCust);
							System.out.println(strSMSAPIForCust);
							uc = (HttpURLConnection)url.openConnection();
							//uc.getResponseMessage();
							int responseCode = uc.getResponseCode();
							
							System.out.println("GET Response Code :: " + responseCode);
							
							if (responseCode == HttpURLConnection.HTTP_OK) { // success
								BufferedReader in = new BufferedReader(new InputStreamReader(
										uc.getInputStream()));
								String inputLine;
								StringBuffer response = new StringBuffer();

								while ((inputLine = in.readLine()) != null) {
									response.append(inputLine);
								}
								in.close();

								// print result
								System.out.println(response.toString());
							} else {
								System.out.println("GET request not worked");
							}
						    uc.disconnect();
						    System.out.print(strSMSAPIForCust);
						    blnExecuted= true;
						} catch (MalformedURLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							blnExecuted= false;
						}
						catch (IOException e) {
					// TODO Auto-generated catch block
							e.printStackTrace();
							blnExecuted= false;
						}
		   }
	}
	
	
	
	//Here we update the Member and log transaction bean
	
	private static boolean sendPaymentDueMembershipRemainderToMembers(EntityPlanMasterView objEPMV) {
		// TODO Auto-generated method stub
		
		List lstPlanExpirySevenBfrMemberList=null;
		boolean blnSendToMemberShipDueAmt=false;
		 Calendar c = Calendar.getInstance();
		 GlobalSettingBean objGSB=(GlobalSettingBean) DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_GLOBAL_SETTING, "", IUMCConstants.GET_ACTIVE_ROWS, "");
		 double dblMinBalanceAmt=0;
		 int intTotalSMS=0;
		 int intSMSInterval=0;
		 if(null!=objGSB){
			 dblMinBalanceAmt=objGSB.getDblMinBalanceRemainderAmt();
			 intTotalSMS=objGSB.getIntNoOfAlertSMS();
			 intSMSInterval=objGSB.getIntSMSIntervalDays();
		 }
		 
		 c.add(Calendar.DATE, intSMSInterval);  // number of days to add
		 String strIntervalDate= WebUtil.formatDate(c.getTime(), IUMCConstants.DB_DATE_FORMAT);
		 strTimeGMT =getTimeZone(objEPMV);
		 String  strMemExpWhereClause=IUMCConstants.GET_ACTIVE_ROWS+" and dblBalanceAmt > "+dblMinBalanceAmt+" and intSMSNotification = 1 and intIsCurrent = 1 and intSMSCount <="+intTotalSMS +" and (dtlastSMSDate like '"+intSMSInterval+"%' or dtlastSMSDate is null)";
		 lstPlanExpirySevenBfrMemberList=DBUtilStatic.getRecordsFromCenter(objEPMV, IUMCConstants.BN_PLAN_SUBS_MEM_NAME, "", strMemExpWhereClause, "");
		// Get SMS configuration
				 String strWhereSMS=IUMCConstants.GET_ACTIVE_ROWS+" and intSMSType = "+IUMCConstants.TRANSACTIONAL;
				 SMSVariableCodeBean objSMSVar=(SMSVariableCodeBean) DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_SMS_VARIABLE_CODE_BEAN, "", strWhereSMS, "");
				 if(null==objSMSVar) return false;
		 
				//Fetching the members whoms balance is more than the given configuration balance amount from membership
				 PlanSubsPaidMemNameTransactionViewBean objPSPMN= new PlanSubsPaidMemNameTransactionViewBean();
				   if(null!=lstPlanExpirySevenBfrMemberList)
				   {
				     //for loop of the list of members
					 for(int i=0;i<lstPlanExpirySevenBfrMemberList.size();i++)
					 {
						 objPSPMN=new PlanSubsPaidMemNameTransactionViewBean(); 
						 objPSPMN = (PlanSubsPaidMemNameTransactionViewBean)lstPlanExpirySevenBfrMemberList.get(i);
						 
		                if(null!=objPSPMN)
		                {
				           //for each member get the appropriate sms template
		                	String strWhereSMSTemplate=IUMCConstants.GET_ACTIVE_ROWS+" and lngMsgTypeID = "+IUMCConstants.MEMBERSHIP_DUE+" and intSend = 1 and lngCompanyID= "+objPSPMN.getLngCompanyID();
		 				    SMSTemplateNotificationBean objSMSTNB=(SMSTemplateNotificationBean)DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_SMS_TEMP_NOTIFICATION, "", strWhereSMSTemplate, "");
		 				   
		 				   String strWhereCompany=IUMCConstants.GET_ACTIVE_ROWS+" and lngID = "+objPSPMN.getLngCompanyID();
		 				   CompanyMasterBean objCMB=(CompanyMasterBean)DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_COMPANY_MASTER_BEAN, "", strWhereCompany, "");
		                   
		 				  //look
						   StringBuilder builder = new StringBuilder();
						   String strSMSAPI = "";
							 if(null!=objSMSVar && objSMSVar.getStrName().trim().length()>0)
							 {
							 strSMSAPI=objSMSVar.getStrName();
							 }
							 String strSMSAPIForCust = strSMSAPI ;
							 URL url = null; 
							 HttpURLConnection uc;
							 //look
		 				    
							 if(null!=objSMSTNB)
							 {
								//look
						         String strContent="";
						         String strSpecificContent = "";
							     strContent =objSMSTNB.getStrContent();
								 strContent = strContent.replaceAll(" ", "%20");
								 strSpecificContent=strContent;
								 
								 //To get the Admin Name of the center
								 String strWhereStaff= IUMCConstants.GET_ACTIVE_ROWS+" and lngID = "+objCMB.getLngAdminID();
								 StaffMasterBean objAdminStaff=(StaffMasterBean) DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_STAFF_MASTER_BEAN, "", strWhereStaff, "");
								 String strAdminName="";
								 if(null!=objAdminStaff && objAdminStaff.getStrName().trim().length()>0)
								 {
									 strAdminName=objAdminStaff.getStrName()+" "+objAdminStaff.getStrLastName();
								 }
							   //look
								 
								 
								 // replace the sms template with actual text : replacing tokens to actual values
								   
									
								 String strName=objPSPMN.getStrMemberName()+" "+objPSPMN.getStrMemberLastName();
						 			if(null!=strName && strName.trim().length()>0)
						 			 {
						 			    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_NAME, strName);
						 			 }
						 			 else
						 			 {
						 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_NAME,"");
						 			 }
						 			    
						 			  strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_ADDRESS, "");
						 			  strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_PH_NO, "");
						 			   
						 			  if(null!=objPSPMN.getStrContactNo() &&  objPSPMN.getStrContactNo().trim().length()>0)
						 			  {
						 			    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_MOB_NO, objPSPMN.getStrContactNo());
						 			  }
						 			  else
						 			  {
						 				 strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_MOB_NO,"");
						 			  }
						 			 if(null!=objPSPMN.getStrEmailID() &&  objPSPMN.getStrEmailID().trim().length()>0)
						 			  {
							 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_EMAIL, objPSPMN.getStrEmailID());
						 			  }
						 			 else
						 			 {
						 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_EMAIL,"");
						 			 }
						 			 
						 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOB,"");
						 		    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOM,"");
						 			
						 		    if(objPSPMN.getDblBalanceAmt()>0){
						 		    	strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOC_AMT, ""+objPSPMN.getDblBalanceAmt());
						 		    }else{
						 		    	strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOC_AMT, "");
						 		    }
						 		    
						 			 if(null!=objCMB.getStrName() &&  objCMB.getStrName().trim().length()>0)
						 			  {
							 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NAME, objCMB.getStrName());
						 			  }	
						 			 else
						 			 {
						 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NAME, "");
						 			 }
						 			if(null!=objCMB.getStrPrimaryMobileNumber() &&  objCMB.getStrPrimaryMobileNumber().trim().length()>0)
						 			  {
							 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NO, objCMB.getStrPrimaryMobileNumber());
						 			  }
						 			else
						 			{
						 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NO,"");
						 			}
						 			if(null!=objCMB.getStrAddress() &&  objCMB.getStrAddress().trim().length()>0)
						 			  {
							 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADDRESS, objCMB.getStrAddress());
						 			  }
						 			else
						 			{
						 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADDRESS,"");
						 			}
						 			if(null!=strAdminName &&  strAdminName.trim().length()>0)
						 			  {
							 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADMIN, strAdminName);
						 			  }
						 			else
						 			{
						 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADMIN, "");
						 			}
						 			if(null!=objCMB.getStrContactEmail() &&  objCMB.getStrContactEmail().trim().length()>0)
						 			  {
							 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_EMAIL, objCMB.getStrContactEmail());
						 			  }
						 			else
						 			{
						 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_EMAIL, "");
						 			}
						 			 
						 			 
						 			 
						 			 strSpecificContent = strSpecificContent.replaceAll(" ", "%20");
									 strSMSAPIForCust=strSMSAPIForCust.replace(IUMCConstants.ST_SEND_TO,objPSPMN.getStrContactNo());
									 strSMSAPIForCust = strSMSAPIForCust.replace(IUMCConstants.ST_SMS_TEXT, builder.append(strSpecificContent).toString());
						         
								 System.out.print(strSMSAPIForCust);
							     try {
									url = new URL(strSMSAPIForCust);
									System.out.println(strSMSAPIForCust);
									uc = (HttpURLConnection)url.openConnection();
									uc.getResponseMessage();
								    uc.disconnect();
								    System.out.print(strSMSAPIForCust);
								    blnSendToMemberShipDueAmt= true;
								    
								    if(blnSendToMemberShipDueAmt==true)
								    {
								    	try {
								    		DBUtilStatic.updateRecordsOfCenter(objEPMV, IUMCConstants.BN_MEMBER_PLAN_SUBS_FROM_TO, "intSMSCount=intSMSCount+"+1+",dtlastSMSDate = '"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_TIME_SEC_FORMAT)+"'", "lngID="+objPSPMN.getLngID());
											//new DBUtil(request).updateRecords(IUMCConstants.BN_MEMBER_PLAN_SUBS_FROM_TO, "intSMSCount=intSMSCount+"+1+",dtlastSMSDate = '"+WebUtil.formatDate(WebUtil.getCurrentDate(request), IUMCConstants.DB_DATE_TIME_SEC_FORMAT)+"'","lngID="+objPSPMN.getLngID());
										} catch (UMBException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
								    }
								} catch (MalformedURLException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
									blnSendToMemberShipDueAmt= false;
								}
								catch (IOException e) {
							// TODO Auto-generated catch block
									e.printStackTrace();
									blnSendToMemberShipDueAmt= false;
								}
							 }
						 	 
							 
		                }
					 }
				  //End of loop
				   }
				 //End of Members with membership due amount
		
		 
		 return blnSendToMemberShipDueAmt;
	}
	
	
	private static boolean sendPaymentDueServiceRemainderToMembers(EntityPlanMasterView objEPMV) {
		// TODO Auto-generated method stub
		
		List lstPlanExpirySevenBfrMemberList=null;
		boolean blnSendToServiceDue=false;
		
		 Calendar c = Calendar.getInstance();
		 GlobalSettingBean objGSB=(GlobalSettingBean) DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_GLOBAL_SETTING, "", IUMCConstants.GET_ACTIVE_ROWS, "");
		 double dblMinBalanceAmt=0;
		 int intTotalSMSCount=0;
		 int intSMSInterval=0;
		 if(null!=objGSB){
			 dblMinBalanceAmt=objGSB.getDblMinBalanceRemainderAmt();
			 intTotalSMSCount=objGSB.getIntNoOfAlertSMS();
			 intSMSInterval=objGSB.getIntSMSIntervalDays();
		 }
		
		 c.add(Calendar.DATE, intSMSInterval);  // number of days to add
		 String strIntervalDate= WebUtil.formatDate(c.getTime(), IUMCConstants.DB_DATE_FORMAT);
		//To get the selected time zone of different countries
	     strTimeGMT =getTimeZone(objEPMV);
	   //Getting  the time zone of different countries ends
		 
		 String  strMemExpWhereClause=IUMCConstants.GET_ACTIVE_ROWS+" and dblTotalBalance > "+dblMinBalanceAmt+" and dblPayableAmt >0 and intSMSNotification = 1  and intSMSCount <="+intTotalSMSCount+" and (dtlastSMSDate like '"+strIntervalDate+"%' or dtlastSMSDate is null)";
		 lstPlanExpirySevenBfrMemberList=DBUtilStatic.getRecordsFromCenter(objEPMV, IUMCConstants.BN_SERVICE_FORM_VIEW, "", strMemExpWhereClause, "");
		// Get SMS configuration
				 String strWhereSMS=IUMCConstants.GET_ACTIVE_ROWS+" and intSMSType = "+IUMCConstants.TRANSACTIONAL;
				 SMSVariableCodeBean objSMSVar=(SMSVariableCodeBean) DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_SMS_VARIABLE_CODE_BEAN, "", strWhereSMS, "");
				 if(null==objSMSVar) return false;
		 
				//Members with service payment due than the given configuration amount
				 ServiceFormTransactionViewBean objPSPMN= new ServiceFormTransactionViewBean();
				   if(null!=lstPlanExpirySevenBfrMemberList)
				   {
				     //for loop of the list of members
					 for(int i=0;i<lstPlanExpirySevenBfrMemberList.size();i++)
					 {
						 objPSPMN=new ServiceFormTransactionViewBean(); 
						 objPSPMN = (ServiceFormTransactionViewBean)lstPlanExpirySevenBfrMemberList.get(i);
						 
		                if(null!=objPSPMN)
		                {
				           //for each member get the appropriate sms template
		                	String strWhereSMSTemplate=IUMCConstants.GET_ACTIVE_ROWS+" and lngMsgTypeID = "+IUMCConstants.SERVICE_DUE+" and intSend = 1 and lngCompanyID= "+objPSPMN.getLngCompanyID();
		 				    SMSTemplateNotificationBean objSMSTNB=(SMSTemplateNotificationBean)DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_SMS_TEMP_NOTIFICATION, "", strWhereSMSTemplate, "");
		 				   
		 				   String strWhereCompany=IUMCConstants.GET_ACTIVE_ROWS+" and lngID = "+objPSPMN.getLngCompanyID();
		 				   CompanyMasterBean objCMB=(CompanyMasterBean)DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_COMPANY_MASTER_BEAN, "", strWhereCompany, "");
		                   
		 				  //look
						   StringBuilder builder = new StringBuilder();
						   String strSMSAPI = "";
							 if(null!=objSMSVar && objSMSVar.getStrName().trim().length()>0)
							 {
							 strSMSAPI=objSMSVar.getStrName();
							 }
							 String strSMSAPIForCust = strSMSAPI ;
							 URL url = null; 
							 HttpURLConnection uc;
							 //look
		 				    
							 if(null!=objSMSTNB)
							 {
								//look
						         String strContent="";
						         String strSpecificContent = "";
							     strContent =objSMSTNB.getStrContent();
								 strContent = strContent.replaceAll(" ", "%20");
								 strSpecificContent=strContent;
								 
								 //To get the Admin Name of the center
								 String strWhereStaff= IUMCConstants.GET_ACTIVE_ROWS+" and lngID = "+objCMB.getLngAdminID();
								 StaffMasterBean objAdminStaff=(StaffMasterBean) DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_STAFF_MASTER_BEAN, "", strWhereStaff, "");
								 String strAdminName="";
								 if(null!=objAdminStaff && objAdminStaff.getStrName().trim().length()>0)
								 {
									 strAdminName=objAdminStaff.getStrName()+" "+objAdminStaff.getStrLastName();
								 }
							   //look
								 
								 
								 // replace the sms template with actual text : replacing tokens to actual values
								   
									
								 String strName=objPSPMN.getStrMemberName();
						 			if(null!=strName && strName.trim().length()>0)
						 			 {
						 			    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_NAME, strName);
						 			 }
						 			 else
						 			 {
						 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_NAME,"");
						 			 }
						 			    
						 			  strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_ADDRESS, "");
						 			  strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_PH_NO, "");
						 			   
						 			  if(null!=objPSPMN.getStrMemberMobNo() &&  objPSPMN.getStrMemberMobNo().trim().length()>0)
						 			  {
						 			    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_MOB_NO, objPSPMN.getStrMemberMobNo());
						 			  }
						 			  else
						 			  {
						 				 strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_MOB_NO,"");
						 			  }
						 			 if(null!=objPSPMN.getStrMemberEmailID() &&  objPSPMN.getStrMemberEmailID().trim().length()>0)
						 			  {
							 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_EMAIL, objPSPMN.getStrMemberEmailID());
						 			  }
						 			 else
						 			 {
						 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_EMAIL,"");
						 			 }
						 			 
						 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOB,"");
						 		    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOM,"");
						 			
						 		   if(objPSPMN.getDblTotalBalance() >0)
						 			  {
							 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOC_AMT, objPSPMN.getDblTotalBalance()+"");
						 			  }
						 			 else
						 			 {
						 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOC_AMT,"");
						 			 }
						 		   
						 		  if(objPSPMN.getLngReceiptID() >0)
					 			  {
						 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOC_NO, objPSPMN.getLngReceiptID()+"");
					 			  }
					 			 else
					 			 {
					 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOC_NO,"");
					 			 }
						 		  
						 		    
						 			 if(null!=objCMB.getStrName() &&  objCMB.getStrName().trim().length()>0)
						 			  {
							 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NAME, objCMB.getStrName());
						 			  }	
						 			 else
						 			 {
						 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NAME, "");
						 			 }
						 			if(null!=objCMB.getStrPrimaryMobileNumber() &&  objCMB.getStrPrimaryMobileNumber().trim().length()>0)
						 			  {
							 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NO, objCMB.getStrPrimaryMobileNumber());
						 			  }
						 			else
						 			{
						 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NO,"");
						 			}
						 			if(null!=objCMB.getStrAddress() &&  objCMB.getStrAddress().trim().length()>0)
						 			  {
							 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADDRESS, objCMB.getStrAddress());
						 			  }
						 			else
						 			{
						 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADDRESS,"");
						 			}
						 			if(null!=strAdminName &&  strAdminName.trim().length()>0)
						 			  {
							 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADMIN, strAdminName);
						 			  }
						 			else
						 			{
						 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADMIN, "");
						 			}
						 			if(null!=objCMB.getStrContactEmail() &&  objCMB.getStrContactEmail().trim().length()>0)
						 			  {
							 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_EMAIL, objCMB.getStrContactEmail());
						 			  }
						 			else
						 			{
						 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_EMAIL, "");
						 			}
						 			 
						 			 
						 			 
						 			 strSpecificContent = strSpecificContent.replaceAll(" ", "%20");
									 strSMSAPIForCust=strSMSAPIForCust.replace(IUMCConstants.ST_SEND_TO,objPSPMN.getStrMemberMobNo());
									 strSMSAPIForCust = strSMSAPIForCust.replace(IUMCConstants.ST_SMS_TEXT, builder.append(strSpecificContent).toString());
						         
								 System.out.print(strSMSAPIForCust);
							     try {
									url = new URL(strSMSAPIForCust);
									System.out.println(strSMSAPIForCust);
									uc = (HttpURLConnection)url.openConnection();
									uc.getResponseMessage();
								    uc.disconnect();
								    System.out.print(strSMSAPIForCust);
								    blnSendToServiceDue= true;
								     if(blnSendToServiceDue==true )
								    {
								    	try {
								    		
								    		DBUtilStatic.updateRecordsOfCenter(objEPMV, IUMCConstants.BN_SERVICE_FORM,  "intSMSCount=intSMSCount+"+1+",dtlastSMSDate = "+WebUtil.getCurrentDate(strTimeGMT), "lngID="+objPSPMN.getLngID());
								    		//DBUtilStatic.updateRecordsOfCenter(objEPMV,IUMCConstants.BN_SERVICE_FORM, "intSMSCount=intSMSCount+"+1+",dtlastSMSDate = "+WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_TIME_SEC_FORMAT)+"'", "lngID="+objPSPMN.getLngID());
											//new DBUtil(request).updateRecords(IUMCConstants.BN_SERVICE_FORM, "intSMSCount=intSMSCount+"+1+",dtlastSMSDate = "+WebUtil.getCurrentDate(request),"lngID="+objPDNB.getLngPlanRelatedID());
										} catch (UMBException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
								    }
								    
								} catch (MalformedURLException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
									blnSendToServiceDue= false;
								}
								catch (IOException e) {
							// TODO Auto-generated catch block
									e.printStackTrace();
									blnSendToServiceDue= false;
								}
							 }
						 	 
							 
		                }
					 }
				  //End of loop
				   }
				 //End of Members with service due amount
		
		 
		 return blnSendToServiceDue;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	private static boolean sendAppointmentReminderSmsToMember(EntityPlanMasterView objEPMV) {
		
		AppointmentHeaderTransactionBean objAHTB = new AppointmentHeaderTransactionBean();
		MemberMasterBean objMMB = new MemberMasterBean();
		boolean blnSendAppointmentReminder = false;
		int intGblSmsEnabled = 0;
		
		
		 Calendar c = Calendar.getInstance();
		 GlobalSettingBean objGSB=(GlobalSettingBean) DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_GLOBAL_SETTING, "", IUMCConstants.GET_ACTIVE_ROWS, "");
		
		 if(null!=objGSB){
			 intGblSmsEnabled = objGSB.getIntIsSMS();
		 }
		
		 
				
		 
		 
		 Date dtCurrentDate = new Date();
		 Calendar cal = Calendar.getInstance();
		 cal.add(Calendar.DATE, 1);
		 Date dtPrevDate = cal.getTime();
		 String strPrevDate = WebUtil.formatDate(dtPrevDate, IUMCConstants.DB_DATE_FORMAT);
		 
		
		 List lstMembersHavingAppointment = DBUtilStatic.getRecordsFromCenter(objEPMV, IUMCConstants.BN_APPOINTMENT_HEADER_TRANSACTION, "", IUMCConstants.GET_ACTIVE_ROWS+" AND  dtAppointmentDate = '"+strPrevDate+"' AND intIsSmsReminderSent = 0", "");
		 
		 if(lstMembersHavingAppointment != null && lstMembersHavingAppointment.size()>0)
		 for(int i=0;i<lstMembersHavingAppointment.size();i++)
		 {
			 objAHTB = (AppointmentHeaderTransactionBean) lstMembersHavingAppointment.get(i);
			 
			 if(objAHTB.getLngCustomerID() > 0) {
					objMMB = (MemberMasterBean) DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_MEMBER_MASTER_BEAN, "", IUMCConstants.GET_ACTIVE_ROWS+" AND lngID = "+objAHTB.getLngCustomerID(), "");
							
					if(objMMB == null) {
						objMMB = new MemberMasterBean();
					}
			 
					
					if(intGblSmsEnabled == 1 && objAHTB.getIntIsSmsReminderSent() == 0 && objMMB.getIntSMSNotification() == 1 ) {
						
								String strSpecificContent = "";
										
										StaffMasterBean objSMB = new StaffMasterBean();
										SMSTemplateNotificationBean objSTNB = new SMSTemplateNotificationBean();
										AppointmentStatusCodeBean objASCB = new AppointmentStatusCodeBean();
										
										
										if(objAHTB.getLngStaffID() > 0) {
											
											objSMB = (StaffMasterBean) DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_STAFF_MASTER_BEAN, "", IUMCConstants.GET_ACTIVE_ROWS+" AND lngID = "+objAHTB.getLngStaffID(), "");
													
											if(objSMB == null) {
												objSMB = new StaffMasterBean();
											}
										}
										
										objSTNB = (SMSTemplateNotificationBean) DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_SMS_TEMP_NOTIFICATION, "", IUMCConstants.GET_ACTIVE_ROWS+" AND lngMsgTypeID = "+IUMCConstants.MSG_TYPE_APMT_FOR_MEMBER, "");
												
										
										if(objAHTB.getLngAppointmentStatus() > 0) {
											objASCB = (AppointmentStatusCodeBean) DBUtilStatic.getRecordFromCenter(objEPMV, ILabelConstants.BN_APPOINTMENT_STATUS_CD, "", IUMCConstants.GET_ACTIVE_ROWS+" AND lngID = "+objAHTB.getLngAppointmentStatus(), "");
											if(objASCB == null) {
												objASCB = new AppointmentStatusCodeBean();
											}
										}
						
						
										StringBuilder builder = new StringBuilder();
				
										strSpecificContent = objSTNB.getStrContent();
										String strWhereSMS = IUMCConstants.GET_ACTIVE_ROWS + " AND intSMSType = " + IUMCConstants.TRANSACTIONAL;
										SMSVariableCodeBean objSMSVar = (SMSVariableCodeBean) DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_SMS_VARIABLE_CODE_BEAN, "", strWhereSMS, "");
												
				
										// String strSMSAPI =
										// objCMB.getStrSMSAPI();//"http://login.bulksmsgateway.in/sendmessage.php?user=myhomeindustries&password=myhome@2015&mobile={[SEND_TO]}&message={[SMS_TEXT]}&sender=MAHACM&type=3";
										String strSMSAPI = "";
										if (null != objSMSVar && objSMSVar.getStrName().trim().length() > 0) {
											strSMSAPI = objSMSVar.getStrName();
										}
										
										URL  url = null;
										HttpURLConnection uc;
				
										
												String strSMSAPIForCust = strSMSAPI;
												builder = new StringBuilder();
				
												if (null != objAHTB && objAHTB.getStrCustomerName() != null) {
													if (null != objAHTB.getStrCustomerName() && objAHTB.getStrCustomerName().trim().length() > 0) {
														strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_APMT_MEMBER_NAME,
																WebUtil.processString(objAHTB.getStrCustomerName()));
																		
													} else {
														strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_APMT_MEMBER_NAME, "");
													}
													if (null != objAHTB.getStrPhoneNo() && objAHTB.getStrPhoneNo().trim().length() > 0) {
														strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_APMT_MEMBER_PHONE_NUMBER,
																WebUtil.processString(WebUtil.processString(objAHTB.getStrPhoneNo())));
													} else {
														strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_APMT_MEMBER_PHONE_NUMBER, "");
													}
													if (null != objAHTB.getStrEmailID() && objAHTB.getStrEmailID().trim().length() > 0) {
														strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_APMT_MEMBER_EMAIL_ID,
																WebUtil.processString(objAHTB.getStrEmailID()));
													} else {
														strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_APMT_MEMBER_EMAIL_ID, "");
													}
													
													if (null != objAHTB.getDtAppointmentDate() ) {
														strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_APMT_BOOKING_DATE,
																WebUtil.formatDate(objAHTB.getDtAppointmentDate(), IUMCConstants.DATE_FORMAT));
													} else {
														strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_APMT_BOOKING_DATE, "");
													}
													if (null != objAHTB.getDtInvoiceDate() ) {
														strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_APMT_CREATE_DATE,
																WebUtil.formatDate(objAHTB.getDtInvoiceDate(), IUMCConstants.DATE_FORMAT));
													} else {
														strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_APMT_CREATE_DATE, "");
													}
													
													
													
													if (null != objAHTB.getStrServiceName() && objAHTB.getStrServiceName().trim().length() > 0) {
														strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_APMT_SERVICE_NAME,
																WebUtil.processString(WebUtil.processString(objAHTB.getStrServiceName())));
													} else {
														strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_APMT_SERVICE_NAME, "");
													}
													if (null != objAHTB.getStrPackageName() && objAHTB.getStrPackageName().trim().length() > 0) {
														strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_APMT_PACKAGE_NAME,
																WebUtil.processString(objAHTB.getStrPackageName()));
													} else {
														strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_APMT_PACKAGE_NAME, "");
													}
													
													if ( objAHTB.getLngStaffID()  > 0) {
														strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_APMT_STAFF_NAME,
																WebUtil.processString(objSMB.getStrName()));
													} else {
														strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_APMT_STAFF_NAME, "");
													}
													
													if (null != objAHTB.getStrTimeSlot() && objAHTB.getStrTimeSlot().trim().length() > 0) {
														strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_APMT_TIME_SLOT,
																WebUtil.processString(objAHTB.getStrTimeSlot()));
													} else {
														strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_APMT_TIME_SLOT, "");
													}
				
													if ( objAHTB.getLngAppointmentStatus()  > 0) {
														strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_APMT_STATUS,
																WebUtil.processString(objASCB.getStrName()));
													} else {
														strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_APMT_STATUS, "");
													}
													
													
													
													
													System.out.println("before = " + strSpecificContent);
													//strSpecificContent = strSpecificContent.replaceAll(" ", "%20");
													
													try {
														strSpecificContent = URLEncoder.encode(strSpecificContent, "UTF-8");
													} catch (UnsupportedEncodingException e1) {
														// TODO Auto-generated catch block
														e1.printStackTrace();
													}
													
													System.out.println("Final = " + strSpecificContent);
													strSMSAPIForCust = strSMSAPIForCust.replace(IUMCConstants.ST_SEND_TO,
															objAHTB.getStrPhoneNo());
													System.out.println("strSMSAPIForCust send to ----" + strSMSAPIForCust);
													strSMSAPIForCust = strSMSAPIForCust.replace(IUMCConstants.ST_SMS_TEXT,
															builder.append(strSpecificContent).toString());
													System.out.println("strSMSAPIForCust sms text -----" + strSMSAPIForCust);
													
													  try { 
														  url = new URL(strSMSAPIForCust); //working,commented due to sending of sms
														  System.out.println(strSMSAPIForCust); 
														  uc = (HttpURLConnection)url.openConnection(); 
														  uc.getResponseMessage();
														  uc.disconnect(); System.out.print(strSMSAPIForCust); 
														  blnSendAppointmentReminder=true; 
														 }
													  catch (MalformedURLException e) { 
														  // TODO Auto-generated catch block
													 	e.printStackTrace(); } 
													  catch (IOException e) 
													  { 
														  // TODO Auto-generated catch block
													   e.printStackTrace(); 
													   }
													 
												}
											
						
						
						if(blnSendAppointmentReminder) {
							try {
								DBUtilStatic.updateRecordsOfCenter(objEPMV, IUMCConstants.BN_APPOINTMENT_HEADER_TRANSACTION, " intIsSmsSent = 1 ", " lngID = "+objAHTB.getLngID());
							} catch (UMBException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
							
						}
					
					}
			 
		 }
	}
		 
		
		return blnSendAppointmentReminder;
	} 
	
	
	
private static boolean sendAppointmentReminderSmsToStaff(EntityPlanMasterView objEPMV) {
		
		AppointmentHeaderTransactionBean objAHTB = new AppointmentHeaderTransactionBean();
		MemberMasterBean objMMB = new MemberMasterBean();
		StaffMasterBean objSMB = new StaffMasterBean();
		SMSTemplateNotificationBean objSTNB = new SMSTemplateNotificationBean();
		AppointmentHeaderTransactionBean objAHTBMember = new AppointmentHeaderTransactionBean();
		AppointmentStatusCodeBean objASCB = new AppointmentStatusCodeBean();
		
		boolean blnSendAppointmentReminder = false;
		int intGblSmsEnabled = 0;
		List lstMembersHavingAppointment = null;
		String strSpecificContent = "";
		
		 Calendar c = Calendar.getInstance();
		 GlobalSettingBean objGSB=(GlobalSettingBean) DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_GLOBAL_SETTING, "", IUMCConstants.GET_ACTIVE_ROWS, "");
		
		 if(null!=objGSB){
			 intGblSmsEnabled = objGSB.getIntIsSMS();
		 }
		 
		 Date dtCurrentDate = new Date();
		 Calendar cal = Calendar.getInstance();
		 cal.add(Calendar.DATE, 1);
		 Date dtPrevDate = cal.getTime();
		 String strPrevDate = WebUtil.formatDate(dtPrevDate, IUMCConstants.DB_DATE_FORMAT);
		 
		 
		 List lstOfStaff = DBUtilStatic.getRecordsFromCenter(objEPMV, IUMCConstants.BN_APPOINTMENT_HEADER_TRANSACTION, "", 
					IUMCConstants.GET_ACTIVE_ROWS+" AND  dtAppointmentDate = '"+strPrevDate+"' AND lngStaffID > 0 GROUP BY lngStaffID", "");
		 
		 objSTNB = (SMSTemplateNotificationBean) DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_SMS_TEMP_NOTIFICATION, "", IUMCConstants.GET_ACTIVE_ROWS+" AND intSend = 1 AND lngMsgTypeID = "+IUMCConstants.MSG_TYPE_APMT_FOR_STAFF, "");
		 if(objSTNB == null) {
			 objSTNB = new SMSTemplateNotificationBean();
		 }
		 
		 StringBuilder builder = new StringBuilder();
			
			
			String strWhereSMS = IUMCConstants.GET_ACTIVE_ROWS + " AND intSMSType = " + IUMCConstants.TRANSACTIONAL;
			SMSVariableCodeBean objSMSVar = (SMSVariableCodeBean) DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_SMS_VARIABLE_CODE_BEAN, "", strWhereSMS, "");
			
			URL  url = null;
			HttpURLConnection uc;
			
			
			
			
		 
		 if(lstOfStaff != null && lstOfStaff.size()>0) {
			 for(int i = 0; i<lstOfStaff.size(); i++) {
				
				 objAHTB = (AppointmentHeaderTransactionBean) lstOfStaff.get(i);
				
				 objSMB = (StaffMasterBean) DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_STAFF_MASTER_BEAN, "", IUMCConstants.GET_ACTIVE_ROWS+" AND lngID = "+objAHTB.getLngStaffID(), "");
				 
				 if(intGblSmsEnabled == 1 && objSMB.getIntSMSNotification() == 1 ) {
				 
				 if(objSMB != null && objSMB.getLngID()>0) {
					 
					 lstMembersHavingAppointment = DBUtilStatic.getRecordsFromCenter(objEPMV, IUMCConstants.BN_APPOINTMENT_HEADER_TRANSACTION, "", IUMCConstants.GET_ACTIVE_ROWS+" AND  dtAppointmentDate = '"+strPrevDate+"' AND lngStaffID = "+objSMB.getLngID(), "");
					 
					 if(lstMembersHavingAppointment != null && lstMembersHavingAppointment.size()>0) {
						 for(int j=0;j<lstMembersHavingAppointment.size();j++)
						 {  
							 strSpecificContent += objSTNB.getStrContent();
							 objAHTBMember = (AppointmentHeaderTransactionBean) lstMembersHavingAppointment.get(j);
							 
							 if(objAHTBMember.getLngAppointmentStatus() > 0) {
									objASCB = (AppointmentStatusCodeBean) DBUtilStatic.getRecordFromCenter(objEPMV, ILabelConstants.BN_APPOINTMENT_STATUS_CD, "", IUMCConstants.GET_ACTIVE_ROWS+" AND lngID = "+objAHTBMember.getLngAppointmentStatus(), "");
									if(objASCB == null) {
										objASCB = new AppointmentStatusCodeBean();
										objASCB.setStrName("N/A");
									}
								}
							 
							 
							 if (null != objAHTBMember && objAHTBMember.getLngID() >0) {
									if (null != objAHTBMember.getStrCustomerName() && objAHTBMember.getStrCustomerName().trim().length() > 0) {
										strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_APMT_MEMBER_NAME,
												WebUtil.processString(objAHTBMember.getStrCustomerName()));
														
									} else {
										strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_APMT_MEMBER_NAME, "");
									}
									if (null != objAHTBMember.getStrPhoneNo() && objAHTBMember.getStrPhoneNo().trim().length() > 0) {
										strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_APMT_MEMBER_PHONE_NUMBER,
												WebUtil.processString(WebUtil.processString(objAHTBMember.getStrPhoneNo())));
									} else {
										strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_APMT_MEMBER_PHONE_NUMBER, "");
									}
									if (null != objAHTBMember.getStrEmailID() && objAHTBMember.getStrEmailID().trim().length() > 0) {
										strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_APMT_MEMBER_EMAIL_ID,
												WebUtil.processString(objAHTBMember.getStrEmailID()));
									} else {
										strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_APMT_MEMBER_EMAIL_ID, "");
									}
									
									if (null != objAHTBMember.getDtAppointmentDate() ) {
										strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_APMT_BOOKING_DATE,
												WebUtil.formatDate(objAHTBMember.getDtAppointmentDate(), IUMCConstants.DATE_FORMAT));
									} else {
										strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_APMT_BOOKING_DATE, "");
									}
									if (null != objAHTBMember.getDtInvoiceDate() ) {
										strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_APMT_CREATE_DATE,
												WebUtil.formatDate(objAHTBMember.getDtInvoiceDate(), IUMCConstants.DATE_FORMAT));
									} else {
										strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_APMT_CREATE_DATE, "");
									}
									
									
									
									if (null != objAHTBMember.getStrServiceName() && objAHTBMember.getStrServiceName().trim().length() > 0) {
										strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_APMT_SERVICE_NAME,
												WebUtil.processString(WebUtil.processString(objAHTBMember.getStrServiceName())));
									} else {
										strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_APMT_SERVICE_NAME, "");
									}
									if (null != objAHTBMember.getStrPackageName() && objAHTBMember.getStrPackageName().trim().length() > 0) {
										strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_APMT_PACKAGE_NAME,
												WebUtil.processString(objAHTBMember.getStrPackageName()));
									} else {
										strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_APMT_PACKAGE_NAME, "");
									}
									
									if ( objAHTBMember.getLngStaffID()  > 0) {
										strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_APMT_STAFF_NAME,
												WebUtil.processString(objSMB.getStrName()));
									} else {
										strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_APMT_STAFF_NAME, "");
									}
									
									if (null != objAHTBMember.getStrTimeSlot() && objAHTBMember.getStrTimeSlot().trim().length() > 0) {
										strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_APMT_TIME_SLOT,
												WebUtil.processString(objAHTBMember.getStrTimeSlot()));
									} else {
										strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_APMT_TIME_SLOT, "");
									}
									
									if (null != objAHTBMember.getStrFromDate() && objAHTBMember.getStrFromDate().trim().length() > 0) {
										strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_APMT_TIME_FROM,
												WebUtil.processString(objAHTBMember.getStrFromDate()));
									} else {
										strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_APMT_TIME_FROM, "");
									}
									
									if (null != objAHTBMember.getStrToDate() && objAHTBMember.getStrToDate().trim().length() > 0) {
										strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_APMT_TIME_TO,
												WebUtil.processString(objAHTBMember.getStrToDate()));
									} else {
										strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_APMT_TIME_TO, "");
									}

									if ( objAHTBMember.getLngAppointmentStatus()  > 0) {
										strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_APMT_STATUS,
												WebUtil.processString(objASCB.getStrName()));
									} else {
										strSpecificContent = strSpecificContent.replace(IUMCConstants.ST_APMT_STATUS, "");
									}
									
									
									strSpecificContent += "/n";
							
			 
						 }
					 }
						
						 String strSMSAPI = "";
							if (null != objSMSVar && objSMSVar.getStrName().trim().length() > 0) {
								strSMSAPI = objSMSVar.getStrName();
							}
							
							String strSMSAPIForCust = strSMSAPI;
							builder = new StringBuilder();
						 
						System.out.println("before = " + strSpecificContent);
						//strSpecificContent = strSpecificContent.replaceAll(" ", "%20");
						
						try {
							strSpecificContent = URLEncoder.encode(strSpecificContent, "UTF-8");
						} catch (UnsupportedEncodingException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
						System.out.println("Final = " + strSpecificContent);
						strSMSAPIForCust = strSMSAPIForCust.replace(IUMCConstants.ST_SEND_TO,
								objSMB.getStrMobileNumber());
						System.out.println("strSMSAPIForCust send to ----" + strSMSAPIForCust);
						strSMSAPIForCust = strSMSAPIForCust.replace(IUMCConstants.ST_SMS_TEXT,
								builder.append(strSpecificContent).toString());
						System.out.println("strSMSAPIForCust sms text -----" + strSMSAPIForCust);
						
						  try { 
							  url = new URL(strSMSAPIForCust); //working,commented due to sending of sms
							  System.out.println(strSMSAPIForCust); 
							  uc = (HttpURLConnection)url.openConnection(); 
							  uc.getResponseMessage();
							  uc.disconnect(); System.out.print(strSMSAPIForCust); 
							  blnSendAppointmentReminder=true; 
							 }
						  catch (MalformedURLException e) { 
							  // TODO Auto-generated catch block
						 	e.printStackTrace(); } 
						  catch (IOException e) 
						  { 
							  // TODO Auto-generated catch block
						   e.printStackTrace(); 
						   }
			 
			 
				 }
			 
				 }
			 }
		 }
		 
	}
		 
		
		return blnSendAppointmentReminder;
	} 
	
	
	
//Code related to pending Feedback Member
	private static boolean sendPendingFeedbackMemberMail(EntityPlanMasterView objEPMV){
		String strTableName=IUMCConstants.BN_MEMBER_MASTER_BEAN;
		String strSelectList="";
		String strSortClause="";
		String strWhereClause=IUMCConstants.GET_ACTIVE_ROWS+" and dtNextFeedbackDate <= '"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT),IUMCConstants.DB_DATE_FORMAT)+"'";
		List lstPendingFeedbackMember= DBUtilStatic.getRecordsFromCenter(objEPMV, strTableName, strSelectList, strWhereClause, strSortClause);
		
		if(null!=lstPendingFeedbackMember && lstPendingFeedbackMember.size()>0)
		{
			for(int i=0;i<lstPendingFeedbackMember.size();i++)
			{
				MemberMasterBean objMMB=(MemberMasterBean)lstPendingFeedbackMember.get(i);
				
				if(null!=objMMB && objMMB.getIntEmailNotification()==1 &&   objMMB.getStrEmailID().trim().length()>0)
				{
					
					
					try {
						String strMemberID=objMMB.getLngID()+"";
						 String encodedID = DatatypeConverter.printBase64Binary(strMemberID.getBytes());
						
					
				
					MailVariableCodeBean objMVCB=null;
					objMVCB=(MailVariableCodeBean)DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_MAIL_VARIABLE_CODE_BEAN, strSelectList, "", "");
					String strFrom = objMVCB.getStrUserName();		
					String strTo=objMMB.getStrEmailID();		
							
							
							 
							String strSubject = "Feedback form " ; 
							String strDesc = "Hi, <br> Click here to fill the feedback form.<a href='http://localhost:8080/MemberCentrum/Transaction/feedBackFormAll.jsp?"+ILabelConstants.ID+"="+encodedID+"&"+ILabelConstants.FEEDBACKTYPE+"="+IUMCConstants.INT_FEEDBACK_TYPE_MEMBEER+"&"+IUMCConstants.FROM_PAGE+"="+IUMCConstants.INT_FEEDBACK_PENDING+"&PAGE=MAIL_FEEDBACK' target='_blank'> Link </a><br>Regards,<br>MC";
							//get batch list 
							
							
							
								 SendMailToCenter.sendMailToCenter(objEPMV,strFrom,strTo, null, null, strSubject, strDesc,objMVCB);
								//blnMailSent=true;
								
								
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								
								 
							}
				}			
			}
			
		}
		
		return false;
		
	}
	
	
	public static void sendMessageToAdmin(SMSVariableCodeBean objSMSVar,StaffRoleMasterBeanView objSRMBV,EntityPlanMasterView objEPMV){
		
		boolean blnSent = false;
		 String strWhereSMSTemplate=IUMCConstants.GET_ACTIVE_ROWS+" and lngMsgTypeID = "+IUMCConstants.DAILY_SUMMARY+" and intSend = 1 and lngCompanyID ="+objSRMBV.getLngCompanyID();
		   SMSTemplateNotificationBean objSMSTNB=(SMSTemplateNotificationBean)DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_SMS_TEMP_NOTIFICATION, "", strWhereSMSTemplate, "");
		   
		   String strWhereCompany=IUMCConstants.GET_ACTIVE_ROWS+" and lngID = "+objSRMBV.getLngCompanyID();
		   CompanyMasterBean objCMB=(CompanyMasterBean)DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_COMPANY_MASTER_BEAN, "", strWhereCompany, "");
		  
		 //Get the sum of payment
			MemberPaymentTransactionBean objMPTB=new MemberPaymentTransactionBean();
			double dblPayment=DBUtilStatic.getDoubleFromCenter(objEPMV, IUMCConstants.BN_MEMBER_PAYMENT_TRANSACTION, "sum(dbPayingAmount)", IUMCConstants.GET_ACTIVE_ROWS+" and dtReceiptDate = '"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_FORMAT)+"'", "");
			
			//Get Newly joined members
			Long LngNew=(Long)DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_MEMBER_MASTER_BEAN_VIEW, "count(*)", IUMCConstants.GET_ACTIVE_ROWS+"  and dtDOJ = '"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_FORMAT)+"'", "");
			long lngNew=0;
			if(null!=LngNew)
			{
				lngNew=(LngNew).longValue();
			}
			
			//Get Enquiry Members
			
			 String strWhereEnq=IUMCConstants.GET_ACTIVE_ROWS+"  dtEnquiryDate like '"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_FORMAT)+"%'";
			 List  lstEnq=new DBUtilStatic().getRecordsFromCenter(objEPMV, IUMCConstants.BN_ENQUIRY_HEADER_TRANSACTION_BEAN, "", strWhereEnq, "");
			 long lngEnq=0;
			 
				if(null!=lstEnq && lstEnq.size()>0)
				{
					lngEnq=lstEnq.size();
				}
			
			//Attendance Member 
			
			 String strWhereAttendanceMem=IUMCConstants.GET_ACTIVE_ROWS+" and intLoggTypeID ="+IUMCConstants.LOGGED_IS_MEMBER +"  and dtLoginDate like '"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_FORMAT)+"%' group by lngMemberID";
			 List  lstAttendMem=new DBUtilStatic().getRecordsFromCenter(objEPMV, IUMCConstants.BN_ATTENDANCE_GYM_BEAN, "", strWhereAttendanceMem, "");
			 long lngMemAttendance=0;
			 
				if(null!=lstAttendMem && lstAttendMem.size()>0)
				{
					lngMemAttendance=lstAttendMem.size();
				}
				 
			//Attendace Staff	
				String strWhereAttendanceStaff=IUMCConstants.GET_ACTIVE_ROWS+"  and intLoggTypeID ="+IUMCConstants.LOGGED_IS_STAFF +"  and dtLoginDate like '"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_FORMAT)+"%' group by lngMemberID";
				List lstAttendStaff=new DBUtilStatic().getRecordsFromCenter(objEPMV, IUMCConstants.BN_ATTENDANCE_GYM_BEAN, "", strWhereAttendanceStaff, "");		
			   long lngStaffAttendance=0;
				if(null!=lstAttendStaff && lstAttendStaff.size()>0)
				{
					lngStaffAttendance=lstAttendStaff.size();
				}
				
		   //Renewal Today Done
				String strWhereRenewalDone=IUMCConstants.GET_ACTIVE_ROWS+" and "+IUMCConstants.LNG_MEM_RENEW_PAYMENT+" and dtReceiptDate ='"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_FORMAT)+"'";
				List lstRenewalMem=new DBUtilStatic().getRecordsFromCenter(objEPMV, IUMCConstants.BN_MEMBER_PAYMENT_TRANSACTION, "", strWhereRenewalDone, "");
			    long lngRenewalCount=0;
			    
			    if(null!=lstRenewalMem && lstRenewalMem.size()>0)
			    {
			    	lngRenewalCount=lstRenewalMem.size();
			    }
		   
		   //look
		   StringBuilder builder = new StringBuilder();
		   String strSMSAPI = "";
			 if(null!=objSMSVar && objSMSVar.getStrName().trim().length()>0)
			 {
			 strSMSAPI=objSMSVar.getStrName();
			 }
			 String strSMSAPIForCust = strSMSAPI ;
			 URL url = null; 
			 HttpURLConnection uc;
			 //look
		   if(null!=objSMSTNB)
		   {
			   //look
		         String strContent="";
		         String strSpecificContent = "";
			     strContent =objSMSTNB.getStrContent();
				 strContent = strContent.replaceAll(" ", "%20");
				 strSpecificContent=strContent;
				 
				 //To get the Admin Name of the center
				 String strWhereStaff= IUMCConstants.GET_ACTIVE_ROWS+" and lngID = "+objCMB.getLngAdminID();
				 StaffMasterBean objAdminStaff=(StaffMasterBean) DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_STAFF_MASTER_BEAN, "", strWhereStaff, "");
				 String strAdminName="";
				 if(null!=objAdminStaff && objAdminStaff.getStrName().trim().length()>0)
				 {
					 strAdminName=objAdminStaff.getStrName()+" "+objAdminStaff.getStrLastName();
				 }
			   //look
			   
  // replace the sms template with actual text : replacing tokens to actual values
			   
				
		 			 String strName=objSRMBV.getStrName()+" "+objSRMBV.getStrLastName();
		 			 
		 			 if(null!=strName && strName.trim().length()>0)
		 			 {
		 			    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_NAME, strName);
		 			 }
		 			 else
		 			 {
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_NAME,"");
		 			 }
		 			    if(null!=objSRMBV.getStrAddress() &&  objSRMBV.getStrAddress().trim().length()>0)
		 			    {
		 			    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_ADDRESS, objSRMBV.getStrAddress());
		 			    }
		 			    else
		 			    {
		 			    	strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_ADDRESS, "");
		 			    }
		 			    
		 			   if(null!=objSRMBV.getStrLandlineNumber() &&  objSRMBV.getStrLandlineNumber().trim().length()>0)
		 			    {
		 			    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_PH_NO, objSRMBV.getStrLandlineNumber());
		 			    }
		 			   else
		 			   {
		 				  strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_PH_NO, "");
		 			   }
		 			  if(null!=objSRMBV.getStrMobileNumber() &&  objSRMBV.getStrMobileNumber().trim().length()>0)
		 			  {
		 			    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_MOB_NO, objSRMBV.getStrMobileNumber());
		 			  }
		 			  else
		 			  {
		 				 strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_MOB_NO,"");
		 			  }
		 			 if(null!=objSRMBV.getStrEmail() &&  objSRMBV.getStrEmail().trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_EMAIL, objSRMBV.getStrEmail());
		 			  }
		 			 else
		 			 {
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_EMAIL,"");
		 			 }
		 			 if(null!=objSRMBV.getDtDOB())
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOB, objSRMBV.getDtDOB()+"");
		 			  }	
		 			 else
		 			 {
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOB,"");
		 			 }
		 			if(null!=objSRMBV.getDtDOM() )
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOM, objSRMBV.getDtDOM()+"");
		 			  }
		 			else
		 			{
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOM,"");
		 			}
		 			
		 			//Added for Payment Summary
		 			
		 			
		 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_PAYMENT_COLLECTED, dblPayment+"");
		 			
		 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_NEWLY_JOINED, lngNew+"");
		 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_ENQUIRY, lngEnq+"");
		 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_MEMBER_ATTENDANCE, lstAttendMem+"");
		 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_STAFF_ATTENDANCE, lstAttendStaff+"");
		 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_RENEWED, lngRenewalCount+"");
		 			
		 			 if(null!=objCMB.getStrName() &&  objCMB.getStrName().trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NAME, objCMB.getStrName());
		 			  }	
		 			 else
		 			 {
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NAME, "");
		 			 }
		 			if(null!=objCMB.getStrPrimaryMobileNumber() &&  objCMB.getStrPrimaryMobileNumber().trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NO, objCMB.getStrPrimaryMobileNumber());
		 			  }
		 			else
		 			{
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NO,"");
		 			}
		 			if(null!=objCMB.getStrAddress() &&  objCMB.getStrAddress().trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADDRESS, objCMB.getStrAddress());
		 			  }
		 			else
		 			{
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADDRESS,"");
		 			}
		 			if(null!=strAdminName &&  strAdminName.trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADMIN, strAdminName);
		 			  }
		 			else
		 			{
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADMIN, "");
		 			}
		 			if(null!=objCMB.getStrContactEmail() &&  objCMB.getStrContactEmail().trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_EMAIL, objCMB.getStrContactEmail());
		 			  }
		 			else
		 			{
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_EMAIL, "");
		 			}
		 			
		 			 
		 			 
					 strSpecificContent = strSpecificContent.replaceAll(" ", "%20");
					 strSMSAPIForCust=strSMSAPIForCust.replace(IUMCConstants.ST_SEND_TO,objSRMBV.getStrMobileNumber());
					 strSMSAPIForCust = strSMSAPIForCust.replace(IUMCConstants.ST_SMS_TEXT, builder.append(strSpecificContent).toString());
			           
					 System.out.print(strSMSAPIForCust);
						try {
							url = new URL(strSMSAPIForCust);
							System.out.println(strSMSAPIForCust);
							uc = (HttpURLConnection)url.openConnection();
							uc.getResponseMessage();
						    uc.disconnect();
						    System.out.print(strSMSAPIForCust);
						    blnSent= true;
						} catch (MalformedURLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							blnSent= false;
						}
						catch (IOException e) {
					// TODO Auto-generated catch block
							e.printStackTrace();
							blnSent= false;
						}
				 
		   }
	}
	
	public static boolean sendSMSToAdmin(EntityPlanMasterView objEPMV)
	{
		boolean blnSent=false;
		String strWhereClause=IUMCConstants.GET_ACTIVE_ROWS+" and lngRoleTypeID = "+IUMCConstants.INT_ADMIN;
		
		//Getting the time zone of the country
		strTimeGMT=getTimeZone(objEPMV);
		//End of getting time zone of the country
		
		//Get the admin 
		StaffRoleMasterBeanView objSRMBV= new StaffRoleMasterBeanView();
		objSRMBV=(StaffRoleMasterBeanView)DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_STAFF_ROLE_MASTER_BEAN_VIEW, "", strWhereClause, "");
		
		//Get the sum of payment
		MemberPaymentTransactionBean objMPTB=new MemberPaymentTransactionBean();
		double dblPayment=DBUtilStatic.getDoubleFromCenter(objEPMV, IUMCConstants.BN_MEMBER_PAYMENT_TRANSACTION, "sum(dbPayingAmount)", IUMCConstants.GET_ACTIVE_ROWS+" and dtReceiptDate = '"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_FORMAT)+"'", "");
		
		// Get SMS configuration
				 String strWhereSMS=IUMCConstants.GET_ACTIVE_ROWS+" and intSMSType = "+IUMCConstants.TRANSACTIONAL;
				 SMSVariableCodeBean objSMSVar=(SMSVariableCodeBean) DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_SMS_VARIABLE_CODE_BEAN, "", strWhereSMS, "");
				 if(null==objSMSVar) return false;
		
		if(null!=objSRMBV)
		{
		    
			sendMessageToAdmin(objSMSVar,objSRMBV,objEPMV);
			
		  String strWhereSMSTemplate=IUMCConstants.GET_ACTIVE_ROWS+" and lngMsgTypeID = "+IUMCConstants.PAYMENT_SUMMARY+" and intSend = 1 and lngCompanyID ="+objSRMBV.getLngCompanyID();
		   SMSTemplateNotificationBean objSMSTNB=(SMSTemplateNotificationBean)DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_SMS_TEMP_NOTIFICATION, "", strWhereSMSTemplate, "");
		   
		   String strWhereCompany=IUMCConstants.GET_ACTIVE_ROWS+" and lngID = "+objSRMBV.getLngCompanyID();
		   CompanyMasterBean objCMB=(CompanyMasterBean)DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_COMPANY_MASTER_BEAN, "", strWhereCompany, "");
		  
		   //look
		   StringBuilder builder = new StringBuilder();
		   String strSMSAPI = "";
			 if(null!=objSMSVar && objSMSVar.getStrName().trim().length()>0)
			 {
			 strSMSAPI=objSMSVar.getStrName();
			 }
			 String strSMSAPIForCust = strSMSAPI ;
			 URL url = null; 
			 HttpURLConnection uc;
			 //look
		   if(null!=objSMSTNB)
		   {
			   //look
		         String strContent="";
		         String strSpecificContent = "";
			     strContent =objSMSTNB.getStrContent();
				 strContent = strContent.replaceAll(" ", "%20");
				 strSpecificContent=strContent;
				 
				 //To get the Admin Name of the center
				 String strWhereStaff= IUMCConstants.GET_ACTIVE_ROWS+" and lngID = "+objCMB.getLngAdminID();
				 StaffMasterBean objAdminStaff=(StaffMasterBean) DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_STAFF_MASTER_BEAN, "", strWhereStaff, "");
				 String strAdminName="";
				 if(null!=objAdminStaff && objAdminStaff.getStrName().trim().length()>0)
				 {
					 strAdminName=objAdminStaff.getStrName()+" "+objAdminStaff.getStrLastName();
				 }
			   //look
			   
      // replace the sms template with actual text : replacing tokens to actual values
			   
				
		 			 String strName=objSRMBV.getStrName()+" "+objSRMBV.getStrLastName();
		 			 
		 			 if(null!=strName && strName.trim().length()>0)
		 			 {
		 			    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_NAME, strName);
		 			 }
		 			 else
		 			 {
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_NAME,"");
		 			 }
		 			    if(null!=objSRMBV.getStrAddress() &&  objSRMBV.getStrAddress().trim().length()>0)
		 			    {
		 			    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_ADDRESS, objSRMBV.getStrAddress());
		 			    }
		 			    else
		 			    {
		 			    	strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_ADDRESS, "");
		 			    }
		 			    
		 			   if(null!=objSRMBV.getStrLandlineNumber() &&  objSRMBV.getStrLandlineNumber().trim().length()>0)
		 			    {
		 			    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_PH_NO, objSRMBV.getStrLandlineNumber());
		 			    }
		 			   else
		 			   {
		 				  strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_PH_NO, "");
		 			   }
		 			  if(null!=objSRMBV.getStrMobileNumber() &&  objSRMBV.getStrMobileNumber().trim().length()>0)
		 			  {
		 			    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_MOB_NO, objSRMBV.getStrMobileNumber());
		 			  }
		 			  else
		 			  {
		 				 strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_MOB_NO,"");
		 			  }
		 			 if(null!=objSRMBV.getStrEmail() &&  objSRMBV.getStrEmail().trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_EMAIL, objSRMBV.getStrEmail());
		 			  }
		 			 else
		 			 {
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_EMAIL,"");
		 			 }
		 			 if(null!=objSRMBV.getDtDOB())
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOB, objSRMBV.getDtDOB()+"");
		 			  }	
		 			 else
		 			 {
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOB,"");
		 			 }
		 			if(null!=objSRMBV.getDtDOM() )
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOM, objSRMBV.getDtDOM()+"");
		 			  }
		 			else
		 			{
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOM,"");
		 			}
		 			
		 			//Added for Payment Summary
		 			
		 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_PAYMENT_COLLECTED, dblPayment+"");
		 			
		 			 if(null!=objCMB.getStrName() &&  objCMB.getStrName().trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NAME, objCMB.getStrName());
		 			  }	
		 			 else
		 			 {
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NAME, "");
		 			 }
		 			if(null!=objCMB.getStrPrimaryMobileNumber() &&  objCMB.getStrPrimaryMobileNumber().trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NO, objCMB.getStrPrimaryMobileNumber());
		 			  }
		 			else
		 			{
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NO,"");
		 			}
		 			if(null!=objCMB.getStrAddress() &&  objCMB.getStrAddress().trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADDRESS, objCMB.getStrAddress());
		 			  }
		 			else
		 			{
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADDRESS,"");
		 			}
		 			if(null!=strAdminName &&  strAdminName.trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADMIN, strAdminName);
		 			  }
		 			else
		 			{
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADMIN, "");
		 			}
		 			if(null!=objCMB.getStrContactEmail() &&  objCMB.getStrContactEmail().trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_EMAIL, objCMB.getStrContactEmail());
		 			  }
		 			else
		 			{
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_EMAIL, "");
		 			}
		 			
		 			 
		 			 
					 strSpecificContent = strSpecificContent.replaceAll(" ", "%20");
					 strSMSAPIForCust=strSMSAPIForCust.replace(IUMCConstants.ST_SEND_TO,objSRMBV.getStrMobileNumber());
					 strSMSAPIForCust = strSMSAPIForCust.replace(IUMCConstants.ST_SMS_TEXT, builder.append(strSpecificContent).toString());
			           
					 System.out.print(strSMSAPIForCust);
						try {
							url = new URL(strSMSAPIForCust);
							System.out.println(strSMSAPIForCust);
							uc = (HttpURLConnection)url.openConnection();
							uc.getResponseMessage();
						    uc.disconnect();
						    System.out.print(strSMSAPIForCust);
						    blnSent= true;
						} catch (MalformedURLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							blnSent= false;
						}
						catch (IOException e) {
					// TODO Auto-generated catch block
							e.printStackTrace();
							blnSent= false;
						}
				 
		   }
		}
		
		return blnSent;
		
	}
	
	public static boolean sendSMSToAdminDailySummary(EntityPlanMasterView objEPMV)
	{
		boolean blnSent=false;
		String strWhereClause=IUMCConstants.GET_ACTIVE_ROWS+" and lngRoleTypeID = "+IUMCConstants.INT_ADMIN;
		
		//Getting the time zone of the country
		strTimeGMT=getTimeZone(objEPMV);
		//End of getting time zone of the country
		
		//Get the admin 
		StaffRoleMasterBeanView objSRMBV= new StaffRoleMasterBeanView();
		objSRMBV=(StaffRoleMasterBeanView)DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_STAFF_ROLE_MASTER_BEAN_VIEW, "", strWhereClause, "");
		
		//Get the sum of payment
		MemberPaymentTransactionBean objMPTB=new MemberPaymentTransactionBean();
		double dblPayment=DBUtilStatic.getDoubleFromCenter(objEPMV, IUMCConstants.BN_MEMBER_PAYMENT_TRANSACTION, "sum(dbPayingAmount)", IUMCConstants.GET_ACTIVE_ROWS+" and dtReceiptDate = '"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_FORMAT)+"'", "");
		
		//Get Newly joined members
		Long LngNew=(Long)DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_MEMBER_MASTER_BEAN_VIEW, "count(*)", IUMCConstants.GET_ACTIVE_ROWS+"  and dtDOJ = '"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_FORMAT)+"'", "");
		long lngNew=0;
		if(null!=LngNew)
		{
			lngNew=(LngNew).longValue();
		}
		
		//Get Enquiry Members
		
		 String strWhereEnq=IUMCConstants.GET_ACTIVE_ROWS+"  dtEnquiryDate like '"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_FORMAT)+"%'";
		 List  lstEnq=new DBUtilStatic().getRecordsFromCenter(objEPMV, IUMCConstants.BN_ENQUIRY_HEADER_TRANSACTION_BEAN, "", strWhereEnq, "");
		 long lngEnq=0;
		 
			if(null!=lstEnq && lstEnq.size()>0)
			{
				lngEnq=lstEnq.size();
			}
		
		//Attendance Member 
		
		 String strWhereAttendanceMem=IUMCConstants.GET_ACTIVE_ROWS+" and intLoggTypeID ="+IUMCConstants.LOGGED_IS_MEMBER +"  and dtLoginDate like '"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_FORMAT)+"%' group by lngMemberID";
		 List  lstAttendMem=new DBUtilStatic().getRecordsFromCenter(objEPMV, IUMCConstants.BN_ATTENDANCE_GYM_BEAN, "", strWhereAttendanceMem, "");
		 long lngMemAttendance=0;
		 
			if(null!=lstAttendMem && lstAttendMem.size()>0)
			{
				lngMemAttendance=lstAttendMem.size();
			}
			 
		//Attendace Staff	
			String strWhereAttendanceStaff=IUMCConstants.GET_ACTIVE_ROWS+"  and intLoggTypeID ="+IUMCConstants.LOGGED_IS_STAFF +"  and dtLoginDate like '"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_FORMAT)+"%' group by lngMemberID";
			List lstAttendStaff=new DBUtilStatic().getRecordsFromCenter(objEPMV, IUMCConstants.BN_ATTENDANCE_GYM_BEAN, "", strWhereAttendanceStaff, "");		
		   long lngStaffAttendance=0;
			if(null!=lstAttendStaff && lstAttendStaff.size()>0)
			{
				lngStaffAttendance=lstAttendStaff.size();
			}
			
	   //Renewal Today Done
			String strWhereRenewalDone=IUMCConstants.GET_ACTIVE_ROWS+" and "+IUMCConstants.LNG_MEM_RENEW_PAYMENT+" and dtReceiptDate ='"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_FORMAT)+"'";
			List lstRenewalMem=new DBUtilStatic().getRecordsFromCenter(objEPMV, IUMCConstants.BN_MEMBER_PAYMENT_TRANSACTION, "", strWhereRenewalDone, "");
		    long lngRenewalCount=0;
		    
		    if(null!=lstRenewalMem && lstRenewalMem.size()>0)
		    {
		    	lngRenewalCount=lstRenewalMem.size();
		    }
			
			//Today Sales Amount
			//dblSalesTAmt=(Double)DBUtilStatic.getDoubleFromCenter(objEPMV, strTableName, "sum(dblPaidAmount)", strWhereClause, strSortClause)("sum(dblPaidAmount)",IUMCConstants.BN_INVOICE_HEADER,IUMCConstants.GET_ACTIVE_ROWS+"and dtInvoiceDate = '"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_FORMAT)+"'","");
			
			
		// Get SMS configuration
				 String strWhereSMS=IUMCConstants.GET_ACTIVE_ROWS+" and intSMSType = "+IUMCConstants.TRANSACTIONAL;
				 SMSVariableCodeBean objSMSVar=(SMSVariableCodeBean) DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_SMS_VARIABLE_CODE_BEAN, "", strWhereSMS, "");
				 if(null==objSMSVar) return false;
		
		if(null!=objSRMBV)
		{
			sendMessageToAdmin(objSMSVar,objSRMBV,objEPMV);
			
		  String strWhereSMSTemplate=IUMCConstants.GET_ACTIVE_ROWS+" and lngMsgTypeID = "+IUMCConstants.DAILY_SUMMARY+" and intSend = 1 and lngCompanyID ="+objSRMBV.getLngCompanyID();
		   SMSTemplateNotificationBean objSMSTNB=(SMSTemplateNotificationBean)DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_SMS_TEMP_NOTIFICATION, "", strWhereSMSTemplate, "");
		   
		   String strWhereCompany=IUMCConstants.GET_ACTIVE_ROWS+" and lngID = "+objSRMBV.getLngCompanyID();
		   CompanyMasterBean objCMB=(CompanyMasterBean)DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_COMPANY_MASTER_BEAN, "", strWhereCompany, "");
		  
		   //look
		   StringBuilder builder = new StringBuilder();
		   String strSMSAPI = "";
			 if(null!=objSMSVar && objSMSVar.getStrName().trim().length()>0)
			 {
			 strSMSAPI=objSMSVar.getStrName();
			 }
			 String strSMSAPIForCust = strSMSAPI ;
			 URL url = null; 
			 HttpURLConnection uc;
			 //look
		   if(null!=objSMSTNB)
		   {
			   //look
		         String strContent="";
		         String strSpecificContent = "";
			     strContent =objSMSTNB.getStrContent();
				 strContent = strContent.replaceAll(" ", "%20");
				 strSpecificContent=strContent;
				 
				 //To get the Admin Name of the center
				 String strWhereStaff= IUMCConstants.GET_ACTIVE_ROWS+" and lngID = "+objCMB.getLngAdminID();
				 StaffMasterBean objAdminStaff=(StaffMasterBean) DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_STAFF_MASTER_BEAN, "", strWhereStaff, "");
				 String strAdminName="";
				 if(null!=objAdminStaff && objAdminStaff.getStrName().trim().length()>0)
				 {
					 strAdminName=objAdminStaff.getStrName()+" "+objAdminStaff.getStrLastName();
				 }
			   //look
			   
      // replace the sms template with actual text : replacing tokens to actual values
			   
				
		 			 String strName=objSRMBV.getStrName()+" "+objSRMBV.getStrLastName();
		 			 
		 			 if(null!=strName && strName.trim().length()>0)
		 			 {
		 			    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_NAME, strName);
		 			 }
		 			 else
		 			 {
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_NAME,"");
		 			 }
		 			    if(null!=objSRMBV.getStrAddress() &&  objSRMBV.getStrAddress().trim().length()>0)
		 			    {
		 			    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_ADDRESS, objSRMBV.getStrAddress());
		 			    }
		 			    else
		 			    {
		 			    	strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_ADDRESS, "");
		 			    }
		 			    
		 			   if(null!=objSRMBV.getStrLandlineNumber() &&  objSRMBV.getStrLandlineNumber().trim().length()>0)
		 			    {
		 			    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_PH_NO, objSRMBV.getStrLandlineNumber());
		 			    }
		 			   else
		 			   {
		 				  strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_PH_NO, "");
		 			   }
		 			  if(null!=objSRMBV.getStrMobileNumber() &&  objSRMBV.getStrMobileNumber().trim().length()>0)
		 			  {
		 			    strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_MOB_NO, objSRMBV.getStrMobileNumber());
		 			  }
		 			  else
		 			  {
		 				 strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_MOB_NO,"");
		 			  }
		 			 if(null!=objSRMBV.getStrEmail() &&  objSRMBV.getStrEmail().trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_EMAIL, objSRMBV.getStrEmail());
		 			  }
		 			 else
		 			 {
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CUST_EMAIL,"");
		 			 }
		 			 if(null!=objSRMBV.getDtDOB())
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOB, objSRMBV.getDtDOB()+"");
		 			  }	
		 			 else
		 			 {
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOB,"");
		 			 }
		 			if(null!=objSRMBV.getDtDOM() )
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOM, objSRMBV.getDtDOM()+"");
		 			  }
		 			else
		 			{
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_DOM,"");
		 			}
		 			
		 			//Added for Payment Summary
		 			
		 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_PAYMENT_COLLECTED, dblPayment+"");
		 			
		 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_NEWLY_JOINED, lngNew+"");
		 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_ENQUIRY, lngEnq+"");
		 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_MEMBER_ATTENDANCE, lstAttendMem+"");
		 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_STAFF_ATTENDANCE, lstAttendStaff+"");
		 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_RENEWED, lngRenewalCount+"");
		 			
		 			 if(null!=objCMB.getStrName() &&  objCMB.getStrName().trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NAME, objCMB.getStrName());
		 			  }	
		 			 else
		 			 {
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NAME, "");
		 			 }
		 			if(null!=objCMB.getStrPrimaryMobileNumber() &&  objCMB.getStrPrimaryMobileNumber().trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NO, objCMB.getStrPrimaryMobileNumber());
		 			  }
		 			else
		 			{
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_NO,"");
		 			}
		 			if(null!=objCMB.getStrAddress() &&  objCMB.getStrAddress().trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADDRESS, objCMB.getStrAddress());
		 			  }
		 			else
		 			{
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADDRESS,"");
		 			}
		 			if(null!=strAdminName &&  strAdminName.trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADMIN, strAdminName);
		 			  }
		 			else
		 			{
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_ADMIN, "");
		 			}
		 			if(null!=objCMB.getStrContactEmail() &&  objCMB.getStrContactEmail().trim().length()>0)
		 			  {
			 			strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_EMAIL, objCMB.getStrContactEmail());
		 			  }
		 			else
		 			{
		 				strSpecificContent=strSpecificContent.replace(IUMCConstants.ST_CENTER_EMAIL, "");
		 			}
		 			
		 			 
		 			 
					 strSpecificContent = strSpecificContent.replaceAll(" ", "%20");
					 strSMSAPIForCust=strSMSAPIForCust.replace(IUMCConstants.ST_SEND_TO,objSRMBV.getStrMobileNumber());
					 strSMSAPIForCust = strSMSAPIForCust.replace(IUMCConstants.ST_SMS_TEXT, builder.append(strSpecificContent).toString());
			           
					 System.out.print(strSMSAPIForCust);
						try {
							url = new URL(strSMSAPIForCust);
							System.out.println(strSMSAPIForCust);
							uc = (HttpURLConnection)url.openConnection();
							uc.getResponseMessage();
						    uc.disconnect();
						    System.out.print(strSMSAPIForCust);
						    blnSent= true;
						} catch (MalformedURLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							blnSent= false;
						}
						catch (IOException e) {
					// TODO Auto-generated catch block
							e.printStackTrace();
							blnSent= false;
						}
				 
		   }
		}
		
		return blnSent;
		
	}
	
	private static boolean isEmailEnabledForCenter(EntityPlanMasterView objEPMV) {
		String strTableName = IUMCConstants.BN_GLOBAL_SETTING;
		String strSelectList = "";
		String  strWhereClause = IUMCConstants.GET_ACTIVE_ROWS;
		String strSortClause = "strName, strLastName";
		
		GlobalSettingBean objGSB = (GlobalSettingBean)	DBUtilStatic.getRecordFromCenter(objEPMV, strTableName, strSelectList, strWhereClause, "");
		
		if( null!=objGSB && objGSB.getIntIsEmail()==1 )
		{
			return true;
		}
		else
		{
		return false;
		}
		
		
	}
	
	
}
