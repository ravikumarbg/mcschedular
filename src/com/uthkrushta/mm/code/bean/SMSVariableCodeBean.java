package com.uthkrushta.mm.code.bean;

public class SMSVariableCodeBean {

	private long lngID;
	private String strName;
	private int intSMSType;
	private long lngCompanyID;
	private int intStatus;
	
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public String getStrName() {
		return strName;
	}
	public void setStrName(String strName) {
		this.strName = strName;
	}
	public int getIntSMSType() {
		return intSMSType;
	}
	public void setIntSMSType(int intSMSType) {
		this.intSMSType = intSMSType;
	}
	public long getLngCompanyID() {
		return lngCompanyID;
	}
	public void setLngCompanyID(long lngCompanyID) {
		this.lngCompanyID = lngCompanyID;
	}
	public int getIntStatus() {
		return intStatus;
	}
	public void setIntStatus(int intStatus) {
		this.intStatus = intStatus;
	}
	
	
}
