package com.uthkrushta.mm.configuration.bean;

public class TimeZoneConfigurationBean {

	private long lngID;
	private String strCountryName;
	private String strTimeZone;
	private int intStatus;
	
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public String getStrCountryName() {
		return strCountryName;
	}
	public void setStrCountryName(String strCountryName) {
		this.strCountryName = strCountryName;
	}
	public String getStrTimeZone() {
		return strTimeZone;
	}
	public void setStrTimeZone(String strTimeZone) {
		this.strTimeZone = strTimeZone;
	}
	public int getIntStatus() {
		return intStatus;
	}
	public void setIntStatus(int intStatus) {
		this.intStatus = intStatus;
	}
	
	
}
