package com.uthkrushta.mm.transaction.bean;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="tr_appointment_header")
public class AppointmentHeaderTransactionBean {
	

	private long lngID;
	private long lngInvoiceNo;
	private String strInvoiceNo;
	private Date dtInvoiceDate;
	private String strCustomerName;
	private String strPhoneNo;
	private String strEmailID;
	
	
	private String strServiceName;
	private String strHSNSACCode;
	private double dblAmount;
	private double dblTaxRate;
	private double dblTaxAmount;
	private double dblCGSTRate;
	private double dblSGSTRate;
	private double dblIGSTRate;
	private double dblCGSTAmount;
	private double dblSGSTAmount;
	private double dblIGSTAmount;
	private double dblNetTotalAmount;
	private double dblRoundOffAmount;
	private double dblTotalPayableAmount;
	private double dblTotalPaidAmount;
	private long lngPaymentMethodID;
	private String strPaymentMemo;
	private String strTimeSlot;
	private Date dtAppointmentDate;
	private String strTransactionID;
	
	private long lngTransactionType;
	private String strFromDate;
	private String strToDate;
	
	private Date dtCreatedOn;
	private long lngFinancialYearID;
	private int intStatus;
	
	private long lngCustomerID;
	private long lngServiceID;
	private long lngAppointmentStatus;
	
	private long lngStaffID;
	
	private String strStaffNote;
	private String strCustomerNote;
	
	private long lngPackageID;
	private long lngServiceRequestID;
	
	private String strPackageName;
	private int intForReview;
	
	private int intIsSmsSent;
	private int intIsSmsReminderSent;
	
	@Column(name="pk_id")
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long getLngID() {
		return lngID;
	}
	public void setLngID(Long lngID) {
		this.lngID = lngID;
	}
	
	@Column(name="invoice_id")
	public long getLngInvoiceNo() {
		return lngInvoiceNo;
	}
	public void setLngInvoiceNo(long lngInvoiceNo) {
		this.lngInvoiceNo = lngInvoiceNo;
	}
	
	@Column(name="invoice_no")
	public String getStrInvoiceNo() {
		return strInvoiceNo;
	}
	public void setStrInvoiceNo(String strInvoiceNo) {
		this.strInvoiceNo = strInvoiceNo;
	}
	
	@Column(name="invoice_date")
	public Date getDtInvoiceDate() {
		return dtInvoiceDate;
	}
	public void setDtInvoiceDate(Date dtInvoiceDate) {
		this.dtInvoiceDate = dtInvoiceDate;
	}
	
	@Column(name="customer_name")
	public String getStrCustomerName() {
		return strCustomerName;
	}
	public void setStrCustomerName(String strCustomerName) {
		this.strCustomerName = strCustomerName;
	}
	
	@Column(name="phone_no")
	public String getStrPhoneNo() {
		return strPhoneNo;
	}
	public void setStrPhoneNo(String strPhoneNo) {
		this.strPhoneNo = strPhoneNo;
	}
	
	@Column(name="email_id")
	public String getStrEmailID() {
		return strEmailID;
	}
	public void setStrEmailID(String strEmailID) {
		this.strEmailID = strEmailID;
	}
	
	@Column(name="service_name")
	public String getStrServiceName() {
		return strServiceName;
	}
	public void setStrServiceName(String strServiceName) {
		this.strServiceName = strServiceName;
	}
	
	@Column(name="hsn_sac_code")
	public String getStrHSNSACCode() {
		return strHSNSACCode;
	}
	public void setStrHSNSACCode(String strHSNSACCode) {
		this.strHSNSACCode = strHSNSACCode;
	}
	
	@Column(name="amount")
	public double getDblAmount() {
		return dblAmount;
	}
	public void setDblAmount(double dblAmount) {
		this.dblAmount = dblAmount;
	}
	
	@Column(name="tax_rate")
	public double getDblTaxRate() {
		return dblTaxRate;
	}
	public void setDblTaxRate(double dblTaxRate) {
		this.dblTaxRate = dblTaxRate;
	}
	
	@Column(name="tax_amount")
	public double getDblTaxAmount() {
		return dblTaxAmount;
	}
	public void setDblTaxAmount(double dblTaxAmount) {
		this.dblTaxAmount = dblTaxAmount;
	}
	
	@Column(name="cgst_rate")
	public double getDblCGSTRate() {
		return dblCGSTRate;
	}
	public void setDblCGSTRate(double dblCGSTRate) {
		this.dblCGSTRate = dblCGSTRate;
	}
	
	@Column(name="sgst_rate")
	public double getDblSGSTRate() {
		return dblSGSTRate;
	}
	public void setDblSGSTRate(double dblSGSTRate) {
		this.dblSGSTRate = dblSGSTRate;
	}
	
	@Column(name="igst_rate")
	public double getDblIGSTRate() {
		return dblIGSTRate;
	}
	public void setDblIGSTRate(double dblIGSTRate) {
		this.dblIGSTRate = dblIGSTRate;
	}
	
	@Column(name="cgst_amount")
	public double getDblCGSTAmount() {
		return dblCGSTAmount;
	}
	public void setDblCGSTAmount(double dblCGSTAmount) {
		this.dblCGSTAmount = dblCGSTAmount;
	}
	
	@Column(name="sgst_amount")
	public double getDblSGSTAmount() {
		return dblSGSTAmount;
	}
	public void setDblSGSTAmount(double dblSGSTAmount) {
		this.dblSGSTAmount = dblSGSTAmount;
	}
	
	@Column(name="igst_amount")
	public double getDblIGSTAmount() {
		return dblIGSTAmount;
	}
	public void setDblIGSTAmount(double dblIGSTAmount) {
		this.dblIGSTAmount = dblIGSTAmount;
	}
	
	@Column(name="net_total_amount")
	public double getDblNetTotalAmount() {
		return dblNetTotalAmount;
	}
	public void setDblNetTotalAmount(double dblNetTotalAmount) {
		this.dblNetTotalAmount = dblNetTotalAmount;
	}
	
	@Column(name="round_off_amount")
	public double getDblRoundOffAmount() {
		return dblRoundOffAmount;
	}
	public void setDblRoundOffAmount(double dblRoundOffAmount) {
		this.dblRoundOffAmount = dblRoundOffAmount;
	}
	
	@Column(name="total_payable_amount")
	public double getDblTotalPayableAmount() {
		return dblTotalPayableAmount;
	}
	public void setDblTotalPayableAmount(double dblTotalPayableAmount) {
		this.dblTotalPayableAmount = dblTotalPayableAmount;
	}
	
	@Column(name="total_paid_amount")
	public double getDblTotalPaidAmount() {
		return dblTotalPaidAmount;
	}
	public void setDblTotalPaidAmount(double dblTotalPaidAmount) {
		this.dblTotalPaidAmount = dblTotalPaidAmount;
	}
	
	@Column(name="fk_payment_method_id")
	public long getLngPaymentMethodID() {
		return lngPaymentMethodID;
	}
	public void setLngPaymentMethodID(long lngPaymentMethodID) {
		this.lngPaymentMethodID = lngPaymentMethodID;
	}
	
	@Column(name="payment_memo")
	public String getStrPaymentMemo() {
		return strPaymentMemo;
	}
	public void setStrPaymentMemo(String strPaymentMemo) {
		this.strPaymentMemo = strPaymentMemo;
	}
	
	@Column(name="created_on")
	public Date getDtCreatedOn() {
		return dtCreatedOn;
	}
	public void setDtCreatedOn(Date dtCreatedOn) {
		this.dtCreatedOn = dtCreatedOn;
	}
	
	@Column(name="fk_financial_year_id")
	public long getLngFinancialYearID() {
		return lngFinancialYearID;
	}
	public void setLngFinancialYearID(long lngFinancialYearID) {
		this.lngFinancialYearID = lngFinancialYearID;
	}
	
	@Column(name="status")
	public int getIntStatus() {
		return intStatus;
	}
	public void setIntStatus(int intStatus) {
		this.intStatus = intStatus;
	}
	
	@Column(name="transaction_id")
	public String getStrTransactionID() {
		return strTransactionID;
	}
	public void setStrTransactionID(String strTransactionID) {
		this.strTransactionID = strTransactionID;
	}
	
	@Column(name="time_slot")
	public String getStrTimeSlot() {
		return strTimeSlot;
	}
	public void setStrTimeSlot(String strTimeSlot) {
		this.strTimeSlot = strTimeSlot;
	}
	
	@Column(name="appointment_date")
	public Date getDtAppointmentDate() {
		return dtAppointmentDate;
	}
	public void setDtAppointmentDate(Date dtAppointmentDate) {
		this.dtAppointmentDate = dtAppointmentDate;
	}
	
	@Column(name="transaction_type")
	public long getLngTransactionType() {
		return lngTransactionType;
	}
	public void setLngTransactionType(long lngTransactionType) {
		this.lngTransactionType = lngTransactionType;
	}
	
	@Column(name="from_date")
	public String getStrFromDate() {
		return strFromDate;
	}
	public void setStrFromDate(String strFromDate) {
		this.strFromDate = strFromDate;
	}
	
	@Column(name="to_date")
	public String getStrToDate() {
		return strToDate;
	}
	public void setStrToDate(String strToDate) {
		this.strToDate = strToDate;
	}
	
	
	@Column(name="fk_customer_id")
	public long getLngCustomerID() {
		return lngCustomerID;
	}
	public void setLngCustomerID(long lngCustomerID) {
		this.lngCustomerID = lngCustomerID;
	}
	@Column(name="fk_service_id")
	public long getLngServiceID() {
		return lngServiceID;
	}
	public void setLngServiceID(long lngServiceID) {
		this.lngServiceID = lngServiceID;
	}
	@Column(name="fk_appointment_status")
	public long getLngAppointmentStatus() {
		return lngAppointmentStatus;
	}
	public void setLngAppointmentStatus(long lngAppointmentStatus) {
		this.lngAppointmentStatus = lngAppointmentStatus;
	}
	@Column(name="fk_staff_id")
	public long getLngStaffID() {
		return lngStaffID;
	}
	public void setLngStaffID(long lngStaffID) {
		this.lngStaffID = lngStaffID;
	}
	@Column(name="staff_note")
	public String getStrStaffNote() {
		return strStaffNote;
	}
	public void setStrStaffNote(String strStaffNote) {
		this.strStaffNote = strStaffNote;
	}
	@Column(name="customer_note")
	public String getStrCustomerNote() {
		return strCustomerNote;
	}
	public void setStrCustomerNote(String strCustomerNote) {
		this.strCustomerNote = strCustomerNote;
	}
	@Column(name="fk_package_id")
	public long getLngPackageID() {
		return lngPackageID;
	}
	public void setLngPackageID(long lngPackageID) {
		this.lngPackageID = lngPackageID;
	}
	@Column(name="fk_service_request_id")
	public long getLngServiceRequestID() {
		return lngServiceRequestID;
	}
	public void setLngServiceRequestID(long lngServiceRequestID) {
		this.lngServiceRequestID = lngServiceRequestID;
	}
	@Column(name="package_name")
	public String getStrPackageName() {
		return strPackageName;
	}
	public void setStrPackageName(String strPackageName) {
		this.strPackageName = strPackageName;
	}
	@Column(name="for_review")
	public int getIntForReview() {
		return intForReview;
	}
	public void setIntForReview(int intForReview) {
		this.intForReview = intForReview;
	}
	@Column(name="is_sms_sent")
	public int getIntIsSmsSent() {
		return intIsSmsSent;
	}
	public void setIntIsSmsSent(int intIsSmsSent) {
		this.intIsSmsSent = intIsSmsSent;
	}
	@Column(name="is_sms_reminder_sent")
	public int getIntIsSmsReminderSent() {
		return intIsSmsReminderSent;
	}
	public void setIntIsSmsReminderSent(int intIsSmsReminderSent) {
		this.intIsSmsReminderSent = intIsSmsReminderSent;
	}
	
	

}
