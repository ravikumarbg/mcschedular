package com.uthkrushta.mm.frameWork.bean;

public class OutputManagerBean {
	

		private long lngActivityID;
		private String strReportPath;
		private String strFileName;
		private String strFileLocation;
		private String strFileExt;
		private int intFileFormat;
		private int intStatus;
		
		
		public long getLngActivityID() {
			return lngActivityID;
		}
		public void setLngActivityID(long lngActivityID) {
			this.lngActivityID = lngActivityID;
		}
		public String getStrReportPath() {
			return strReportPath;
		}
		public void setStrReportPath(String strReportPath) {
			this.strReportPath = strReportPath;
		}
		public String getStrFileName() {
			return strFileName;
		}
		public void setStrFileName(String strFileName) {
			this.strFileName = strFileName;
		}
		public String getStrFileLocation() {
			return strFileLocation;
		}
		public void setStrFileLocation(String strFileLocation) {
			this.strFileLocation = strFileLocation;
		}
		public String getStrFileExt() {
			return strFileExt;
		}
		public void setStrFileExt(String strFileExt) {
			this.strFileExt = strFileExt;
		}
		public int getIntFileFormat() {
			return intFileFormat;
		}
		public void setIntFileFormat(int intFileFormat) {
			this.intFileFormat = intFileFormat;
		}
		public int getIntStatus() {
			return intStatus;
		}
		public void setIntStatus(int intStatus) {
			this.intStatus = intStatus;
		}
		
		
	

}
