package com.uthkrushta.basis.constants;

public interface ISessionAttributes {
	public static final String ROLE_ID = "ROLE_ID";
	public static final String DISP_NAME = "DISP_NAME";
	public static final String USER_ID = "USER_ID";
	public static final String MESSAGES = "MESSAGES";
	public static final String ERR_LIST = "ERR_LIST";
	public static final String LOGIN_ID = "LOGIN_ID";
	public static final String LOGIN_USER = "LOGIN_USER";
	public static final String CENTER_CODE = "CENTER_CODE";
	public static final String ACTIVE_FINANCIAL_YEAR="ACTIVE_FINANCIAL_YEAR";
	
}
