package com.uthkrushta.mm.transaction.bean;

import java.util.Date;

public class PlanSubsPaidMemNameTransactionViewBean {


	private long lngID;
	private long lngMemID;
	private long lngPlanID;
	private long lngSubsID;
	private Date dtFromDate;
	private Date dtToDate;
	private double dblTotalAmt;
	private double dblPaidAmt;
	private double dblBalanceAmt;
	private int intIsCurrent;
	private String strMemberName;
	private String strMemberLastName;
	private String strPlanName;
	private String strSubscriptionName;
	private long lngCompanyID;
	private int intStatus;
	//Added on 16DEC
	private String strContactNo;
	private Date dtLastAttendedDate;
	private String strMemberCode;
	private String strCompanyName;
	//Added on 6JAN
	private String strEmailID;
	private int intSMSNotification;
	private int intMailNotification;
	
    private String strNamePrefix;
	
	//Photo
	private String strMemPhotoPath;
	
	 private int intSMSCount;
	  private Date dtlastSMSDate;
	
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public long getLngMemID() {
		return lngMemID;
	}
	public void setLngMemID(long lngMemID) {
		this.lngMemID = lngMemID;
	}
	public long getLngPlanID() {
		return lngPlanID;
	}
	public void setLngPlanID(long lngPlanID) {
		this.lngPlanID = lngPlanID;
	}
	public long getLngSubsID() {
		return lngSubsID;
	}
	public void setLngSubsID(long lngSubsID) {
		this.lngSubsID = lngSubsID;
	}
	public Date getDtFromDate() {
		return dtFromDate;
	}
	public void setDtFromDate(Date dtFromDate) {
		this.dtFromDate = dtFromDate;
	}
	public Date getDtToDate() {
		return dtToDate;
	}
	public void setDtToDate(Date dtToDate) {
		this.dtToDate = dtToDate;
	}
	public double getDblTotalAmt() {
		return dblTotalAmt;
	}
	public void setDblTotalAmt(double dblTotalAmt) {
		this.dblTotalAmt = dblTotalAmt;
	}
	public double getDblPaidAmt() {
		return dblPaidAmt;
	}
	public void setDblPaidAmt(double dblPaidAmt) {
		this.dblPaidAmt = dblPaidAmt;
	}
	public double getDblBalanceAmt() {
		return dblBalanceAmt;
	}
	public void setDblBalanceAmt(double dblBalanceAmt) {
		this.dblBalanceAmt = dblBalanceAmt;
	}
	public int getIntIsCurrent() {
		return intIsCurrent;
	}
	public void setIntIsCurrent(int intIsCurrent) {
		this.intIsCurrent = intIsCurrent;
	}
	public String getStrMemberName() {
		return strMemberName;
	}
	public void setStrMemberName(String strMemberName) {
		this.strMemberName = strMemberName;
	}
	public String getStrMemberLastName() {
		return strMemberLastName;
	}
	public void setStrMemberLastName(String strMemberLastName) {
		this.strMemberLastName = strMemberLastName;
	}
	public int getIntStatus() {
		return intStatus;
	}
	public void setIntStatus(int intStatus) {
		this.intStatus = intStatus;
	}
	public String getStrPlanName() {
		return strPlanName;
	}
	public void setStrPlanName(String strPlanName) {
		this.strPlanName = strPlanName;
	}
	public String getStrSubscriptionName() {
		return strSubscriptionName;
	}
	public void setStrSubscriptionName(String strSubscriptionName) {
		this.strSubscriptionName = strSubscriptionName;
	}
	public long getLngCompanyID() {
		return lngCompanyID;
	}
	public void setLngCompanyID(long lngCompanyID) {
		this.lngCompanyID = lngCompanyID;
	}
	public String getStrContactNo() {
		return strContactNo;
	}
	public void setStrContactNo(String strContactNo) {
		this.strContactNo = strContactNo;
	}
	public Date getDtLastAttendedDate() {
		return dtLastAttendedDate;
	}
	public void setDtLastAttendedDate(Date dtLastAttendedDate) {
		this.dtLastAttendedDate = dtLastAttendedDate;
	}
	public String getStrMemberCode() {
		return strMemberCode;
	}
	public void setStrMemberCode(String strMemberCode) {
		this.strMemberCode = strMemberCode;
	}
	public String getStrCompanyName() {
		return strCompanyName;
	}
	public void setStrCompanyName(String strCompanyName) {
		this.strCompanyName = strCompanyName;
	}
	public String getStrEmailID() {
		return strEmailID;
	}
	public void setStrEmailID(String strEmailID) {
		this.strEmailID = strEmailID;
	}
	public int getIntSMSNotification() {
		return intSMSNotification;
	}
	public void setIntSMSNotification(int intSMSNotification) {
		this.intSMSNotification = intSMSNotification;
	}
	public int getIntMailNotification() {
		return intMailNotification;
	}
	public void setIntMailNotification(int intMailNotification) {
		this.intMailNotification = intMailNotification;
	}
	public String getStrNamePrefix() {
		return strNamePrefix;
	}
	public void setStrNamePrefix(String strNamePrefix) {
		this.strNamePrefix = strNamePrefix;
	}
	public String getStrMemPhotoPath() {
		return strMemPhotoPath;
	}
	public void setStrMemPhotoPath(String strMemPhotoPath) {
		this.strMemPhotoPath = strMemPhotoPath;
	}
	public int getIntSMSCount() {
		return intSMSCount;
	}
	public void setIntSMSCount(int intSMSCount) {
		this.intSMSCount = intSMSCount;
	}
	public Date getDtlastSMSDate() {
		return dtlastSMSDate;
	}
	public void setDtlastSMSDate(Date dtlastSMSDate) {
		this.dtlastSMSDate = dtlastSMSDate;
	}
	
	
	
	
}
