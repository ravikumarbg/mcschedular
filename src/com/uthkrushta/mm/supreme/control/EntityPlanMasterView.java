package com.uthkrushta.mm.supreme.control;

import java.util.Date;

public class EntityPlanMasterView {
	long lngID;
	String strGymCode;
	String strUrl;
	String strDriverClass;
	String strUserName;
	String strPassword;
	long lngPoolSize;
	long lngPlanID; 
	Date dtPlanStartDate;
	Date dtPlanEndDate;
	String strContactNumber;
	String strContactPersonName;
	String strEmailID;
	int intIsActive;
	long lngGracePeriod;
	String strMessage;
	long lngMessageType;//(error,alert,success)
	String strDbName;
	String strCompanyName;
	long lngSubscriptionID;
	String strSms;
	String strMail;
	
	String strName;
	long lngBranchCount;
	long intIsTransactionalSMS;
	int intIsMail;
	int intIsPromotionalSMS;
	long lngMemberCount;
	long lngLoginCount;
	long lngPlanValidity;
	long lngAttendanceTypeID;
	long lngAdminUserCount;
	int intPlanStatus;
	int intEnableRewardSystem;
	
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public String getStrGymCode() {
		return strGymCode;
	}
	public void setStrGymCode(String strGymCode) {
		this.strGymCode = strGymCode;
	}
	public String getStrUrl() {
		return strUrl;
	}
	public void setStrUrl(String strUrl) {
		this.strUrl = strUrl;
	}
	public String getStrDriverClass() {
		return strDriverClass;
	}
	public void setStrDriverClass(String strDriverClass) {
		this.strDriverClass = strDriverClass;
	}
	public String getStrUserName() {
		return strUserName;
	}
	public void setStrUserName(String strUserName) {
		this.strUserName = strUserName;
	}
	public String getStrPassword() {
		return strPassword;
	}
	public void setStrPassword(String strPassword) {
		this.strPassword = strPassword;
	}
	public long getLngPoolSize() {
		return lngPoolSize;
	}
	public void setLngPoolSize(long lngPoolSize) {
		this.lngPoolSize = lngPoolSize;
	}
	public long getLngPlanID() {
		return lngPlanID;
	}
	public void setLngPlanID(long lngPlanID) {
		this.lngPlanID = lngPlanID;
	}
	public Date getDtPlanStartDate() {
		return dtPlanStartDate;
	}
	public void setDtPlanStartDate(Date dtPlanStartDate) {
		this.dtPlanStartDate = dtPlanStartDate;
	}
	public Date getDtPlanEndDate() {
		return dtPlanEndDate;
	}
	public void setDtPlanEndDate(Date dtPlanEndDate) {
		this.dtPlanEndDate = dtPlanEndDate;
	}
	public String getStrContactNumber() {
		return strContactNumber;
	}
	public void setStrContactNumber(String strContactNumber) {
		this.strContactNumber = strContactNumber;
	}
	public String getStrContactPersonName() {
		return strContactPersonName;
	}
	public void setStrContactPersonName(String strContactPersonName) {
		this.strContactPersonName = strContactPersonName;
	}
	public String getStrEmailID() {
		return strEmailID;
	}
	public void setStrEmailID(String strEmailID) {
		this.strEmailID = strEmailID;
	}
	public int getIntIsActive() {
		return intIsActive;
	}
	public void setIntIsActive(int intIsActive) {
		this.intIsActive = intIsActive;
	}
	public long getLngGracePeriod() {
		return lngGracePeriod;
	}
	public void setLngGracePeriod(long lngGracePeriod) {
		this.lngGracePeriod = lngGracePeriod;
	}
	public String getStrMessage() {
		return strMessage;
	}
	public void setStrMessage(String strMessage) {
		this.strMessage = strMessage;
	}
	public long getLngMessageType() {
		return lngMessageType;
	}
	public void setLngMessageType(long lngMessageType) {
		this.lngMessageType = lngMessageType;
	}
	public String getStrDbName() {
		return strDbName;
	}
	public void setStrDbName(String strDbName) {
		this.strDbName = strDbName;
	}
	public String getStrCompanyName() {
		return strCompanyName;
	}
	public void setStrCompanyName(String strCompanyName) {
		this.strCompanyName = strCompanyName;
	}
	public long getLngSubscriptionID() {
		return lngSubscriptionID;
	}
	public void setLngSubscriptionID(long lngSubscriptionID) {
		this.lngSubscriptionID = lngSubscriptionID;
	}
	public String getStrSms() {
		return strSms;
	}
	public void setStrSms(String strSms) {
		this.strSms = strSms;
	}
	public String getStrMail() {
		return strMail;
	}
	public void setStrMail(String strMail) {
		this.strMail = strMail;
	}
	public String getStrName() {
		return strName;
	}
	public void setStrName(String strName) {
		this.strName = strName;
	}
	public long getLngBranchCount() {
		return lngBranchCount;
	}
	public void setLngBranchCount(long lngBranchCount) {
		this.lngBranchCount = lngBranchCount;
	}
	public long getIntIsTransactionalSMS() {
		return intIsTransactionalSMS;
	}
	public void setIntIsTransactionalSMS(long intIsTransactionalSMS) {
		this.intIsTransactionalSMS = intIsTransactionalSMS;
	}
	public int getIntIsMail() {
		return intIsMail;
	}
	public void setIntIsMail(int intIsMail) {
		this.intIsMail = intIsMail;
	}
	public int getIntIsPromotionalSMS() {
		return intIsPromotionalSMS;
	}
	public void setIntIsPromotionalSMS(int intIsPromotionalSMS) {
		this.intIsPromotionalSMS = intIsPromotionalSMS;
	}
	public long getLngMemberCount() {
		return lngMemberCount;
	}
	public void setLngMemberCount(long lngMemberCount) {
		this.lngMemberCount = lngMemberCount;
	}
	public long getLngLoginCount() {
		return lngLoginCount;
	}
	public void setLngLoginCount(long lngLoginCount) {
		this.lngLoginCount = lngLoginCount;
	}
	public long getLngPlanValidity() {
		return lngPlanValidity;
	}
	public void setLngPlanValidity(long lngPlanValidity) {
		this.lngPlanValidity = lngPlanValidity;
	}
	public long getLngAttendanceTypeID() {
		return lngAttendanceTypeID;
	}
	public void setLngAttendanceTypeID(long lngAttendanceTypeID) {
		this.lngAttendanceTypeID = lngAttendanceTypeID;
	}
	public long getLngAdminUserCount() {
		return lngAdminUserCount;
	}
	public void setLngAdminUserCount(long lngAdminUserCount) {
		this.lngAdminUserCount = lngAdminUserCount;
	}
	public int getIntPlanStatus() {
		return intPlanStatus;
	}
	public void setIntPlanStatus(int intPlanStatus) {
		this.intPlanStatus = intPlanStatus;
	}
	public int getIntEnableRewardSystem() {
		return intEnableRewardSystem;
	}
	public void setIntEnableRewardSystem(int intEnableRewardSystem) {
		this.intEnableRewardSystem = intEnableRewardSystem;
	}
	
	

}
