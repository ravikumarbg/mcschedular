package com.uthkrushta.basis.constants;


public interface ILabelConstants {

	
	
	String BN_APPOINTMENT_STATUS_CD = "AppointmentStatusCodeBean"; 
	String BN_CONGF_GLB_URL_SETTING = "GlobalUrlSettingBean";
	
	
	
	
	
	
	//oct 7th
	String txtServiceAmount = "txtServiceAmount";
	String selManufacturer = "selManufacturer";
	String selServicedBy = "selServicedBy";
	String selCurrentLocation = "selCurrentLocation";
	String txtServiceName = "txtServiceName";
	String txtServiceEmail = "txtServiceEmail"; 
	String selServiceType ="selServiceType";
	String txtAmount = "txtAmount";
	String selVendor = "selVendor";
	String txtPurchaseDate = "txtPurchaseDate";
	String selPurchaseType = "selPurchaseType";
	String selAssetsGroup = "selAssetsGroup";
	String chkDiffComp= "chkDiffComp";
	String txtOccupation = "txtOccupation";
	String txtMessage = "txtMessage";
	String txtCustomerComments = "txtCustomerComments";
	String txtNextFollowupTm ="txtNextFollowupTm" ;
	
	
	//memberShip Management
	String txtCode = "txtCode";
	String hdnCode = "hdnCode";
	String hdnStaffLogo = "hdnStaffLogo";
	String hdnMemberLogo = "hdnMemberLogo";
	String selUomForHeight = "selUomForHeight";
	String selUomForWeight = "selUomForWeight";
	String NAME = "NAME";
	String DATE_FROM = "DATE_FROM";
	String DATE_TO = "DATE_TO";
	String STAFF_CODE = "STAFF_CODE";
	String txtDOM = "txtDOM";
	String selList = "selList";
	String FETCH_LIST ="FETCH_LIST";
	String selLoggType ="selLoggType";
	
	String selPaymentType = "selPaymentType";
	String txtAttendanceCode = "txtAttendanceCode";
	String chkLogType = "chkLogType";
	String selReason = "selReason";
	String selMember = "selMember";
	String selCompanyFrom = "selCompanyFrom";
	String selCompanyTo = "selCompanyTo";
	String hdnRenewalDate= "hdnRenewalDate";
	String hdnExtendedID ="hdnExtendedID";
	String hdnPlanIDForSubscription = "hdnPlanIDForSubscription";
	String txtMaxExtensionDays_="txtMaxExtensionDays_";
	String hdnRemainingDays = "hdnRemainingDays";
	String hdnPaused = "hdnPaused";
	String hdnPauseEditID = "hdnPauseEditID";
	String hdnPauseID = "hdnPauseID";	
	String txtPaused = "txtPaused";
	String txtToDate = "txtToDate";
	String txtFromDate = "txtFromDate";
	String txtFees = "txtFees";
	String txtReason = "txtReason";
	String txtExtensionDateUpto = "txtExtensionDateUpto";
	String selPause = "selPause";
	String txtFreezeNo = "txtFreezeNo";
	String txtExtensionNo ="txtExtensionNo";
	String txtEnqDateFrmFilter ="txtEnqDateFrmFilter";
	String txtEnqDateToFilter = "txtEnqDateToFilter";
	String selConversionFilter = "selConversionFilter";
	String selEnuiryFilter ="selEnuiryFilter";
	String selBatchFilter = "selBatchFilter";
	String selPlanFilter ="selPlanFilter";
	String selGenderFilter = "selGenderFilter";
	String selMemberFilter = "selMemberFilter";
	String hdnActivityCounter_ = "hdnActivityCounter_";
	String hdnMenuCounter = "hdnMenuCounter";
	String selActivityID = "selActivityID";
	String selActivityIDMem = "selActivityIDMem";
	String txtRole_ = "txtRole_";
	String txtRole_String = "txtRole_";
	String selRoleType_  = "selRoleType_";
	String hdnTaxTypeSize = "hdnTaxTypeSize";
	String hdnSelTracker ="hdnSelTracker";
	String hdnIDBatch_ = "hdnIDBatch_";
	String serviceTax2_ = "serviceTax2_";
	String salesTax1_ = "salesTax1_";
	String MEMBER_AUTO_INC_NO ="MEMBER_AUTO_INC_NO";
	String selFinancialyear = "selFinancialyear";
	String hdnAutoReset_ = "hdnAutoReset_";
	String chkResetAutoNo_ = "chkResetAutoNo_";
	String hdnAutoNo_ ="hdnAutoNo_";
	String chkAutoNo_ = "chkAutoNo_";
	String txtStartNumber_ = "txtStartNumber_";
	String referredTypeID = "referredTypeID";
	String selReferrer = "selReferrer";
	String selReferredType = "selReferredType";
	String txtEnquiryID = "txtEnquiryID";
	String strComments = "strComments";
	String hdnEnquiryID = "hdnEnquiryID";
	String txtNextFollowupDt = "txtNextFollowupDt";
	String selEnquryStatus = "selEnquryStatus";
	String selConversion = "selConversion";
	String selStaff = "selStaff";
	String selStaff_="selStaff_";
	String selSource = "selSource";
	String txtEnquiryDt = "txtEnquiryDt";
	String txtMemberCode = "txtMemberCode";
	String txtMemberID = "txtMemberID";
	String selOccupation = "selOccupation";
	String txtMedicalHistory = "txtMedicalHistory";
	String txtBMI = "txtBMI";
	String txtHeight = "txtHeight";
	String txtWeight = "txtWeight";
	String txtLastAttendenDate = "txtLastAttendenDate";
	String txtRenewalDate = "txtRenewalDate";
	String selSubscription = "selSubscription";
	String txtICENumber = "txtICENumber";
	String selRelation = "selRelation";
	String selBloodGrp = "selBloodGrp";
	String selGender = "selGender";
	String txtICEName = "txtICEName";
	String txtLandLineNumber = "txtLandLineNumber";
	String txtRelievedComments = "txtRelievedComments";
	String txtRelievedDate = "txtRelievedDate";
	String selStatus = "selStatus";
	String txtStaffCode = "txtStaffCode";
	String txtStaffID ="txtStaffID";
	String filePhoto = "filePhoto";
	String txtSalary = "txtSalary";
	String selBatch = "selBatch";
	String selCompany = "selCompany";
	String txtMemID="txtMemID";
	String txtProductName = "txtProductName";
	String txtCompanyEmail = "txtCompanyEmail";
	String txtProjectCode = "txtProjectCode";
	String txtPrimaryPhoneNumber = "txtPrimaryPhoneNumber";
	String txtVersion = "txtVersion";
	String txtSecondaryPhoneNumber = "txtSecondaryPhoneNumber";
	String txtCopyright = "txtCopyright";
	String txtProjectType = "txtProjectType";
	String txtRecordsPerPage = "txtRecordsPerPage";
	String COMPANY_TYPE = "COMPANY_TYPE";
	String selCountryFilter = "selCountryFilter";
	String selCountry_ = "selCountry_";
	String txtAddress = "txtAddress";
	String selCountry = "selCountry";
	String txtPincode = "txtPincode";
	String txtPrimaryTelephone = "txtPrimaryTelephone";
	String txtSecondaryTelephone = "txtSecondaryTelephone";
	String txtPrimaryMobile = "txtPrimaryMobile";
	String txtSecondaryMobile = "txtSecondaryMobile";
	String txtContactPersonNumber = "txtContactPersonNumber";
	String txtTinNo = "txtTinNo";
	String txtTinDate = "txtTinDate";
	String txtCstNo = "txtCstNo";
	String txtCstDate = "txtCstDate";
	String txtKstNo = "txtKstNo";
	String txtKstDate = "txtKstDate";
	String txtECCNo = "txtECCNo";
	String txtFaxNo = "txtFaxNo";
	String txtTanNo = "txtTanNo";
	String txtServiceNo = "txtServiceNo";
	String txtPanNo = "txtPanNo";
	String txtPFNo = "txtPFNo";
	String txtEsiNo = "txtEsiNo";
	String txtCentralExciseReg="txtCentralExciseReg";
	String txtComm="txtComm";
	String txtRange = "txtRange";
	String txtDivision = "txtDivision";
	String hdnCompanyLogo = "hdnCompanyLogo";
	String selSupplierFilter="selSupplierFilter";
	String hdnLoginID = "hdnLoginID";
	String txtSecurityQues = "txtSecurityQues";
	String txtSecurityAns = "txtSecurityAns";
	String hdnCreatedon="hdnCreatedon";
	String hdnupdatedOn="hdnupdatedOn";
	String txtCompanyID = "txtCompanyID";
	String txtDisplayName = "txtDisplayName";
	String txtSecurityQuesId ="txtSecurityQuesId";
	String txtValidEmail = "txtValidEmail";
	String USER_TYPE = "USER_TYPE";
	String hdnUserID = "hdnUserID";
	String selRoleID = "selRoleID";
	String txtRoleTypeID = "txtRoleTypeID";
	String selEmployeeType = "selEmployeeType";
	String selNamePrefix = "selNamePrefix";
	String selSecurityQuestion = "selSecurityQuestion";
	String txtDateOfBirth = "txtDateOfBirth";
	String txtComments = "txtComments";
	String txaCurrentAddress = "txaCurrentAddress";
	String txapermanentAddress = "txapermanentAddress";
	String selEmployeeStatus="selEmployeeStatus";
	String txtEmailFilter = "txtEmailFilter";
	String txtEmployeeNumberFilter="txtEmployeeNumberFilter";
	String txtPrimaryMobileFilter = "txtPrimaryMobileFilter";
	String selCustomerFilter="selCustomerFilter";
	String selEmployeeFilter = "selEmployeeFilter";
	String txtIsActive_="txtIsActive_";
	String selStaffName="selStaffName";
	String txtEnquiryDate="txtEnquiryDate";
	String selTaxType_ = "selTaxType_";
	String txtRate_ = "txtRate_";
	String selRoleTypeFilter = "selRoleTypeFilter";
	String txtAdminEmail="txtAdminEmail";
	String txtOutgoingEmail="txtOutgoingEmail";
	String txtContactEmail="txtContactEmail";
	String selArea="selArea";
	String chkIsParentCompany="chkIsParentCompany";
	String selParentCompany="selParentCompany";
	String selCompanyType="selCompanyType";
	String txtCompanyCode="txtCompanyCode";
	String selAdmin="selAdmin";
	String fileLogo="fileLogo";
	String selTimeZone="selTimeZone";
	String selLanguage="selLanguage";
	String selCurrency="selCurrency";
	String rdIsActive="rdIsActive";
	String txtActivationCode="txtActivationCode";
	String selPlan="selPlan";
	String rdIsPayer="rdIsPayer";
	String txtPlanStartDate="txtPlanStartDate";
	String txtPlanEndDate="txtPlanEndDate";
	String txtNotificationCounter="txtNotificationCounter";
	String txtEncorporationNumber="txtEncorporationNumber";
	String txtEncorporationDate="txtEncorporationDate";
	String txaSelfComment="txaSelfComment";
	String txtReceiptNo="txtReceiptNo";
	String txtReceiptDate="txtReceiptDate";
	String selMembersWithDue="selMembersWithDue";
	String txtPayable="txtPayable";
	String txtPayingAmount="txtPayingAmount";
	String  txtBalance="txtBalance";
	String selPaymentMode="selPaymentMode";
	String txaMemo="txaMemo";
	String txaPaymentMemo="txaPaymentMemo";
	String selAllMembers="selAllMembers";
	String txtDiscountPercent="txtDiscountPercent";
	String txtDiscountAmount="txtDiscountAmount";
	String txtPlanAmount="txtPlanAmount";
	String selTaxType="selTaxType";
	String hdnTaxTypeID="hdnTaxTypeID";
	String txtTaxRate="txtTaxRate";
	String txtAmtAfterDiscount="txtAmtAfterDiscount";
	String txtTaxAmt="txtTaxAmt";
	String txtPlanID="txtPlanID";
	String hdnSubscriptionID="hdnSubscriptionID";
	String hdnSubscriptionID_="hdnSubscriptionID_";
	String hdnPlanID="hdnPlanID";
	String hdnPageType="hdnPageType";
	String txtJoinFee="txtJoinFee";
	String txtSubscriptionAmount_="txtSubscriptionAmount_";
	String txtFromTime="txtFromTime";
	String txtFromTime_="txtFromTime_";
	String txtToTime="txtToTime";
	String txtToTime_="txtToTime_";
	String txtNotes_="txtNotes_";
	String txtDays_="txtDays_";
	String txtSubscriptionAmount="txtSubscriptionAmount";
	
	//Newly Added 
	String selFromHH_="selFromHH_";
	String selToHH_="selToHH_";
	String selFromMM_="selFromMM_";
	String selToMM_="selToMM_";
	String selFromFormat_="selFromFormat_";
	String selToFormat_="selToFormat_";
	
	//Newly Added 10/08/
	String hdnUpdatedBy_="hdnUpdatedBy_";
	String hdnUpdatedOn_="hdnUpdatedOn_";
	String selCompanyID="selCompanyID";
	String hdnUpdatedBy="hdnUpdatedBy";
	String hdnUpdatedOn="hdnUpdatedOn";
	
	String selIsTallyBillEntered_ = "selIsTallyBillEntered_";
	String txtTallyBill_ = "txtTallyBill_";
	String LID = "LID";
	String selOBBankSourceID = "selOBBankSourceID";
	String selOBCashSourceID = "selOBCashSourceID";
	String txtOBBankAmount = "txtOBBankAmount";
	String txtOBCashAmount = "txtOBCashAmount";
	String hdnTotalIncomingAmount = "hdnTotalIncomingAmount";
	String hdnTotalOutgoingAmount = "hdnTotalOutgoingAmount";
	String txtCBDate = "txtCBDate";
	String hdnTotalCBAmount = "hdnTotalCBAmount";
	String hdnApprovedOn = "hdnApprovedOn";
	String hdnFinancialYearID = "hdnFinancialYearID";
	String txtCFAmount = "txtCFAmount";
	String txtTotalOBAmount = "txtTotalOBAmount";
	String selLedgerID_ = "selLedgerID_";
	String txtIncomingAmount_ = "txtIncomingAmount_";
	String txtOutgoingAmount_ = "txtOutgoingAmount_";
	String selCategory_ = "selCategory_";
	String selExpenseTypeID_ = "selExpenseTypeID_";
	String selEmpID_ = "selEmpID_";
	String selPayMethodID_ = "selPayMethodID_";
	String   hdnTallyPosted_ = "hdnTallyPosted_";
	String hdnEntryVerified_ = "hdnEntryVerified_";
	String chkIsEntryVerified_ = "chkIsEntryVerified_";
	String txtTallyBillNo_ = "txtTallyBillNo_";
	String hdnDocumentPath_ = "hdnDocumentPath_";
	String txtComments_ = "txtComments_";
	String selTags_ = "selTags_";
	String txtCBDateFilter = "txtCBDateFilter";
	String selCBStatusFilter = "selCBStatusFilter";
	String selProjectFilter = "selProjectFilter";
	String selLedgerFilter ="selLedgerFilter";
	String selTagsFilter = "selTagsFilter";
	String selExpenseSubCategoryFilter = "selExpenseSubCategoryFilter";
	String selExpenseCategoryFilter = "selExpenseCategoryFilter";
	String selIsApprovedFilter = "selIsApprovedFilter";
	String selIsTallyBillEntered = "selIsTallyBillEntered";
	String selPaymentMethod = "selPaymentMethod";
	String txtCBFromDateFilter = "txtCBFromDateFilter";
	String txtCBToDateFilter = "txtCBToDateFilter";
	String selTempTags = "selTempTags";
	String selPayMethodID = "selPayMethodID";
	String selLedgerID = "selLedgerID";
	String selCategory = "selCategory";
	String txtOutgoingAmount = "txtOutgoingAmount";
	String txtIncomingAmount = "txtIncomingAmount";
	String hdnItemID = "hdnItemID";
	String hdnEntryVerified = "hdnEntryVerified";
	String hdnTallyPosted = "hdnTallyPosted";
	String tdTags = "tdTags";
	String selTags = "selTags";
	String hdnCompanyID = "hdnCompanyID";
	String hdnCBPageID = "hdnCBPageID";
	String txtTallyBill = "txtTallyBill";
	String txtAllocationDate = "txtAllocationDate";
	String txtFromDateFilter = "txtFromDateFilter";
	String txtToDateFilter = "txtToDateFilter";
	String txtRemarksFilter = "txtRemarksFilter";
	String txtAllocationDate_ = "txtAllocationDate_";
	String hdnIsTallyEnteredStatusName = "hdnIsTallyEnteredStatusName";
	String hdnLedgerName = "hdnLedgerName";
	String hdnEmployeeName = "hdnEmployeeName";
	String hdnTagsName = "hdnTagsName";
	String hdnIsApprovedStatusName = "hdnIsApprovedStatusName";
	String hdnCategoryName = "hdnCategoryName";
	String hdnPayMethodName = "hdnPayMethodName";
	
	//Newly Added 12/08
	String txtReceiptDateFrmFilter="txtReceiptDateFrmFilter";
	String txtReceiptDateToFilter="txtReceiptDateToFilter";

	//for quest app 
	String strAdminName = "strAdminName";
	String lngAdminNumber = "lngAdminNumber";
	String strAdminEmail = "strAdminEmail";
	String strWebsite = "strWebsite";
	String lngPhoneNumber = "lngPhoneNumber";
	String lngMobileNumber = "lngMobileNumber";
	String strEmailId = "strEmailId";
	String strAddress = "strAddress";
	String txtQualification="txtQualification";
	String ACTION = "ACTION";
	
	
	String lngBatchID = "lngBatchID";
	String lngChapterID = "lngChapterID";
	String lngEducationalID = "lngEducationalID";
	String lngStudentStatusID = "lngStudentStatusID";
	String strChapterName = "strChapterName";
	String hiddenSelectedID = "hiddenSelectedID";
	String selChapter = "selChapter";
	String txaQuestion = "txaQuestion";
	String txaAnswer = "txaAnswer";
	String txtMobileNo = "txtMobileNo";
	String selRole = "selRole";
	String selSecurityQuestions = "selSecurityQuestions";
	String txtSecurityAnswer = "txtSecurityAnswer";
	
	String dtStartDate = "dtStartDate";
	String dtEndDate = "dtEndDate";
	
	String txtName = "txtName";
	String txtAge = "txtAge";
	String selEmpStatus = "selEmpStatus";
	String hdnID = "hdnID";
	String ID = "ID";
	String strStatus ="strStatus";
	String txtDeptId = "longDept";
	String txtManagerID= "lngManagerID";
	String selEmpID = "selEmpID";
	String txtDOJ="DOJ";
	String txtManager="manager";
	String txtDepartment = "department";
	String selEducationalInstitute = "selEducationalInstitute";
	String chkIsActive = "chkIsActive";
	String hdnMemberID="hdnMemberID";
	
	String txtNumber = "txtNumber";
	String txtEmail = "txtEmail";
	String selState = "selState";
	String selDistrict = "selDistrict";
	String txtQuantity = "txtQuantity";
	String txtRemarks = "txtRemarks";
	
	
	String txtLoginDateStart = "txtLoginDateStart";
	String txtLoginDateEnd = "txtLoginDateEnd";
	String txtLastName = "txtLastName";
	String txtFirstName = "txtFirstName";
	String txtMail = "txtMail";
	String txtContactNumber = "txtContactNumber";
	String txtContactPerson = "txtContactPerson";
	String txtWebsite = "txtWebsite";
	String hdnLogoURL = "hdnLogoURL";
	String txaComments = "txaComments";
	String txaAddress = "txaAddress";
	String selCity = "selCity";
	String txtContactInfoFilter = "txtContactInfoFilter";
	String selPropertyTypeFilter = "selPropertyTypeFilter";
	String rdlGender = "rdlGender";
	String selUserRole = "selUserRole";
	String txtUserName = "txtUserName";
	String txtConfirmPassword = "txtConfirmPassword";
	String txtPassword = "txtPassword";
	String txtPasswordHint = "txtPasswordHint";
	String selBuilder = "selBuilder";
	String selLocality = "selLocality";
	String txtLocalityOthers ="txtLocalityOthers";
	String selPropertyType = "selPropertyType";
	String selLocalityFilter  = "selLocalityFilter";
	String selLoanStatus = "selLoanStatus";
	String selBank = "selBank";
	String selExecutive = "selExecutive";
	String selUserRoleFilter = "selUserRoleFilter";
	String selProduct_ = "selProduct_";
	String selProduct = "selProduct";
	
	String selLoanType = "selLoanType";
	String selPossessionStatus = "selPossessionStatus";
	String txtPriceLow = "txtPriceLow";
	String txtPriceHigh = "txtPriceHigh";
	String txtPrice = "txtPrice";
	String txtCityOthers = "txtCityOthers";
	String hdnProductName = "hdnProductName";
	String hdnAgentName = "hdnAgentName";
	String hdnAgentNumber = "hdnAgentName";
	String hdnStateName = "hdnStateName";
	String hdnDistrictName = "hdnDistrictName";
	
	String chkHomePageDiaplay = "chkHomePageDiaplay";
	String txaProjectDetails = "txaProjectDetails";
	String txtLandMark = "txtLandMark";
	String flLocalityMap = "flLocalityMap";
	String flProjectBluePrint = "flProjectBluePrint";
	String flMainImage = "flMainImage";
	String flImage1 = "flImage1";
	String flImage2 = "flImage2";
	String flImage3 = "flImage3";
	String txtTag = "txtTag";
	String txtSize = "txtSize";
	String txtListSize = "txtListSize";
	
	String txtDisplayOrder = "txtDisplayOrder";
	String selHomePageDisplayType = "selHomePageDisplayType";
	String chkHighlightDisplay = "chkHighlightDisplay";
	
	String txaMailContent = "txaMailContent";
	String txtMailSubject = "txtMailSubject";
	String flToMailIDExcel = "flToMailIDExcel";
	String txtFromMailID = "txtFromMailID";
	
	String hdnCreatedBy_ = "hdnCreatedBy_";
	String hdnCreatedOn_ = "hdnCreatedOn_";
	
	String hdnRowCounter = "hdnRowCounter";
	String hdnUserAction = "hdnUserAction";
	String hdnSelectedID = "hdnSelectedID";
	String hdnSelectedItemID = "hdnSelectedItemID";
	String hdnCreatedBy = "hdnCreatedBy";
	String hdnCreatedOn = "hdnCreatedOn";
	String hdnApprovalStatus = "hdnApprovalStatus";
	String hdnApprovedBy = "hdnApprovedBy";
	String hdnReminderSent = "hdnReminderSent";
	String hdnAjaxSubmit = "hdnAjaxSubmit";
	String hdnCurrentPage = "hdnCurrentPage";
	String hdnPageEdited = "hdnPageEdited";
	String hdnActivationCode = "hdnActivationCode";
	String hdnEmpNumber = "hdnEmpNumber";
	String hdnEmpID = "hdnEmpID";
	String hdnRefNumber = "hdnRefNumber";
	String hdnRefID = "hdnRefID";
	
	String txtDOB = "txtDOB";
	
	String txtNameFilter = "txtNameFilter";	
	String txtAgentNameFilter = "txtAgentNameFilter";
	String selCityFilter = "selCityFilter";
	String txtAgentNumberFilter = "txtAgentNumberFilter";
	String selStatusFilter ="selStatusFilter";
	String txtAgentEmailFilter = "txtAgentEmailFilter";
	String selStateFilter = "selStateFilter";
	String selEducationalInstituteFilter = "selEducationalInstituteFilter";
	String hdnReceiptID="hdnReceiptID";
	
	//QuestionPop
	
	String txtMobileNoFilter="txtMobileNoFilter";
	String txtEmailIDFilter="txtEmailIDFilter";
	String selRoleFilter="selRoleFilter";
	String selChapterIDFilter="selChapterIDFilter";
	String txaQuestionFilter="txaQuestionFilter";
	String txaAnswerFilter="txaAnswerFilter";
	String txtStartDateFilter="txtStartDateFilter";
	String txtEndDateFilter="txtEndDateFilter";
	String chkActiveFilter="chkActiveFilter";
	String selStartDate_="selStartDate_";
	String selEndDate_="selEndDate_";
	
	
	String txtLoginDate = "txtLoginDate";
	String txtCustomerName = "txtCustomerName";
	String txtAppliedAmount = "txtAppliedAmount";
	String txtSanctionedAmount = "txtSanctionedAmount";
	String txtProposedROI = "txtProposedROI";
	String txtApplicableROI = "txtApplicableROI";
	String txtProposedTenure = "txtProposedTenure";
	String txtApplicableTenure = "txtApplicableTenure";
	String txtProcessingFee = "txtProcessingFee";
	String txtProcessingPeriod = "txtProcessingPeriod";
	String txtPreClosureCharges = "txtPreClosureCharges";
	String txtEMIAmount = "txtEMIAmount";
	String txtChequeLeaves = "txtChequeLeaves";
	String rdlPartPaymentAllowed = "rdlPartPaymentAllowed";
	String txaPartPaymentCondition = "txaPartPaymentCondition";
	String txtLoanAccountNumber = "txtLoanAccountNumber";
	String txtCompanyName = "txtCompanyName";
	String txaRejectionReason = "txaRejectionReason";
	
	String txtApplicableTenureFilter = "txtApplicableTenureFilter";
	String txtAppliedAmountFilter = "txtAppliedAmountFilter";
	String txtSanctionedAmountFilter = "txtSanctionedAmountFilter";
	
	String SEL_ID = "SEL_ID";
	String UN_SEL_ID = "UN_SEL_ID";
	String IS_AJAX_SUBMIT = "IS_AJAX_SUBMIT";
	String USER_ACTION = "USER_ACTION";
	String ROW_COUNTER = "ROW_COUNTER";
	String CURRENT_PAGE = "CURRENT_PAGE";
	String PAGE_TYPE = "PAGE_TYPE";
	
	String chkMain = "chkMain";
	String chkChild_ = "chkChild_";
	
	String hdnID_ =  "hdnID_";
	String txtName_ = "txtName_";
	String selState_ = "selState_";
	String txtAgentName_ = "txtAgentName_";
	String txtAgentNumber_ = "txtAgentNumber_";
	String txtAgentEmail_ = "txtAgentEmail_";
	String selEducationalInstitute_ = "selEducationalInstitute_";
	
	//QuestionPop
	
		String txaAddress_="txaAddress_";
		String txtMobileNo_="txtMobileNo_";
		String txtEmail_="txtEmail_";
		String selRole_="selRole_";
		String txtDOJ_="txtDOJ_";
		String txtUserName_="txtUserName_";
		String txtPassword_="txtPassword_";
		String selSecurityQuestions_="selSecurityQuestions_";
		String txtSecurityAnswer_="txtSecurityAnswer_";
		String selChapter_="selChapter_";
		String txaQuestion_="txaQuestion_";
		String txaAnswer_="txaAnswer_";
		String txtStartDate_="txtStartDate_";
		String txtEndDate_="txtEndDate_";
		String chkIsActive_="chkIsActive_";
	    
	
	String txtDesc_ = "txtDesc_";
	String txtContactNum_	= "txtContactNum_";
	String txtMail_	= "txtMail_";
	String txtPrefix_ = "txtPrefix_";
	String txtPIN_ = "txtPIN_";
	String selCity_ = "selCity_";
	String selLoanType_ = "selLoanType_";
	String selLoanTypeFilter = "selLoanTypeFilter";
		
	String selPropertyType_ = "selPropertyType_";
	String txtBHK_ = "txtBHK_";
	String txtBathRooms_ = "txtBathRooms_";
	String txtSize_ = "txtSize_";
	String txtPrice_ = "txtPrice_";
	String flFloorPlanURL_ = "flFloorPlanURL_";
	String hdnItemID_ = "hdnItemID_";
	String hdnFloorPlanURL_ = "hdnFloorPlanURL_";
	String hdnBuilderLogoURL = "hdnBuilderLogoURL";
	
	String hdnProjectBluePrintURL = "hdnProjectBluePrintURL";
	String hdnProjectLocationURL = "hdnProjectLocationURL";
	String hdnProjectMainImageURL = "hdnProjectMainImageURL";
	String hdnProjectImage2URL = "hdnProjectImage2URL";
	String hdnProjectImage3URL = "hdnProjectImage3URL";
	String hdnProjectImage1URL = "hdnProjectImage1URL";
	
	
	String hdnPropertyMainImageURL = "hdnPropertyMainImageURL";
	String hdnPropertyImage2URL = "hdnPropertyImage2URL";
	String hdnPropertyImage3URL = "hdnPropertyImage3URL";
	String hdnPropertyImage1URL = "hdnPropertyImage1URL";
	
	String selBudgetLow = "selBudgetLow";
	String selBudgetHigh = "selBudgetHigh";
	String selBedRooms = "selBedRooms";
	
	String selBathRooms = "selBathRooms";
	String txtStartDate = "txtStartDate";
	String txaBannerText = "txaBannerText";
	String txtEndDate = "txtEndDate";
	
	String txtPostedBy ="txtPostedBy";
    String txtPropertyNumber ="txtPropertyNumber";
    String txtPropertyNumberFilter= "txtPropertyNumberFilter";
	String txtApprovalDate ="txtApprovalDate";
	
	String hdnFilePath = "hdnFilePath";
	
	String hdnServiceImageURL="hdnServiceImageURL";
	
		//Newly added on 27-08-2015
		String PAYMENT_TYPE="PAYMENT_TYPE";
		String txtRenewBalance="txtRenewBalance";
		String txtNetAmt="txtNetAmt";
		
		//Newly added on 7-09-2015
		String txtActualAmt="txtActualAmt";
		
		//Newly added on 9-09-2015
		String selBranch="selBranch";
		
		//Newly added on 10-09-2015
		String hdnPaymentType_="hdnPaymentType_";
		
		//Newly added on 15-09-2015
				String selMonth="selMonth";
				String selEvent="selEvent";
		//22-09
		  String txtBatchCapacity_="txtBatchCapacity_";		
		  String chkIsChecked_="chkIsChecked_";
		 
		//23/09
		  String txtZonalNameFilter="txtZonalNameFilter";
		  String txtAreaFilter="txtAreaFilter";
		  String txtZonalName_="txtZonalName_";
		  String selArea_="selArea_";
		  String hdnAllSelectedAreas="hdnAllSelectedAreas";
		  String hdnSelectedAreas_="hdnSelectedAreas_";
		  
		//5/10
		  String txtDate="txtDate";
		
		//12/10
		  String txtReferenceNo="txtReferenceNo";
		  String txaTo="txaTo";
		  String txtSubject="txtSubject";
		  String txaContents="txaContents";
		  
		//17/10
		  String txtHeader="txtHeader";
		  String txtTo="txtTo";
		  String txaSubject="txaSubject";
		  String txaContent="txaContent";
		  String txtSign="txtSign";
		  
		 //27/10
		  String txtPincode_="txtPincode_";
		  
		  //4/11
		  String selGroup="selGroup";
		  String selSupplier="selSupplier";
		  String txtExpiryDate="txtExpiryDate";
		  String txtMRP="txtMRP";
		  String txtBuyingPrice="txtBuyingPrice";
		  String txtSellingPrice="txtSellingPrice";
		  String txtMaxDiscount="txtMaxDiscount";
		  String chkIsIncluive="chkIsIncluive";
		  String selUOM="selUOM";
		  String txaDescription="txaDescription";
		  String txtMaxDiscountPercent="txtMaxDiscountPercent";
		  
		  //6/11
		  String txtInvoiceNo="txtInvoiceNo";
		  String txtInvoiceDate="txtInvoiceDate";
		  String hdnBillToID="hdnBillToID";
		  String hdnItemRowCounter="hdnItemRowCounter";
		  String selBillTo="selBillTo";
		  
		  //7/11
		  String selPerson="selPerson";
		  String txtOutsiders="txtOutsiders";
		  String hdnInvItemID="hdnInvItemID";
		  String hdnMatID="hdnMatID";
		  String txtQuantity_="txtQuantity_";
		  String txtAmount_="txtAmount_";
		  String selMaterialID_="selMaterialID_";
		  String selTaxID_="selTaxID_";
		  String txtItemTaxAmount_="txtItemTaxAmount_";
		  String chkAllocate_="chkAllocate_";
		  String hdnWorkorderItemID_="hdnWorkorderItemID_";
		  String selMaterial_="selMaterial_";
		  String txtLength_="txtLength_";
		  String txtPaytmentMemo="txtPaytmentMemo";
		  String txaRemarks="txaRemarks";
		  String txtSubTotalAmount="txtSubTotalAmount";
		  String txtDiscountPercentage="txtDiscountPercentage";
		  String txtAmountAfterDiscount="txtAmountAfterDiscount";
		  String selMaterial="selMaterial";
		  String txtAmountafterdiscount="txtAmountafterdiscount";
		  String selTaxID="selTaxID";
		  String txtTaxAmount="txtTaxAmount";
		  String txtTotal="txtTotal";
		  String txtAdvanceAmount="txtAdvanceAmount";
		  String chkRoundOff="chkRoundOff";
		  String txtRoundOffAmount="txtRoundOffAmount";
		  String txtAmountPayable="txtAmountPayable";
		  String selTaxRate="selTaxRate";
		  String txtSubTotal="txtSubTotal";
		  String txtConveyanceCharges="txtConveyanceCharges";
		  String hdnTaxRate="hdnTaxRate";
		  String txtGrandAmount="txtGrandAmount";
		  String txtWorkOrderDate="txtWorkOrderDate";
		  String hdnMatID_="hdnMatID_";
		  String hdnOutputID_="hdnOutputID_";
		  String hdnOutputID="hdnOutputID";
		  String hdnTotalBudget="hdnTotalBudget";
		  String txtPaymentTerms="txtPaymentTerms";
		  String txaInvantoryComment_="txaInvantoryComment_";
		  String hdnInvoiceID="hdnInvoiceID";
		  
		  
		  //9/11/2015
		  String hdnInvItemID_="hdnInvItemID_";
		  String txtDiscountPercentage_="txtDiscountPercentage_";
		  String txtDiscountAmount_="txtDiscountAmount_";
		  String txtAmountAfterDiscount_="txtAmountAfterDiscount_";
		  String selItemTaxID_="selItemTaxID_";
		  String txtItemAmount_="txtItemAmount_";
		  String hdnWorkOrderID_="hdnWorkOrderID_";
		  String hdnWorkOrderItemID_="hdnWorkOrderItemID_";
		  String hdnQuantity_="hdnQuantity_";
		  String selCustomer = "selCustomer";
		  String selAdvance = "selAdvance";
		  String txtCreatedOn = "txtCreatedOn";
		  String txtCreatedBy="txtCreatedBy";
		  String txtNetAmount = "txtNetAmount";
		  String txtTotalTaxAmount = "txtTotalTaxAmount";
		  String txtTotalDiscountAmount = "txtTotalDiscountAmount";
		  String txtPaidAmount = "txtPaidAmount";
		  String txtBalanceAmount = "txtBalanceAmount";
		  String txtCollectedAmount = "txtCollectedAmount";
		  String txtDonationAmount = "txtDonationAmount";
		  String txtInvoiceDateToFilter = "txtInvoiceDateToFilter";
		  String txtInvoiceDateFrmFilter = "txtInvoiceDateFrmFilter";
		  
		  
		  //Added on 26NOV
		  
		  String hdnDiscountPercentage_="hdnDiscountPercentage_";
		  String hdnDiscountAmount_="hdnDiscountAmount_";
		  
		  //Added on 27NOV
		  
		  String chkStock = "chkStock";
		  
		  //Added on 30NOV
		  String txtReturnAmount="txtReturnAmount";
		  
		  //Added on 1DEC
		  String txtCurrentlyPaid = "txtCurrentlyPaid";
		  String chkIsLess = "chkIsLess";
		  String txtCashPayment="txtCashPayment";
		  String txtCardPayment="txtCardPayment";
		  
		  //Added on 2DEC
		  String txtMinimumStockQty="txtMinimumStockQty";
		  String chkMinStock="chkMinStock";
		  String chkOutOfStock="chkOutOfStock";
		  String txtOutsidersNo="txtOutsidersNo";
		  String txtOutsidersEmail="txtOutsidersEmail";
		  
		  //Added on 8DEC
		  String rdlMartialStatus="rdlMartialStatus";
		  
		  //Added on 14DEC
		  String txtRepeatPassword="txtRepeatPassword";
		  
		  //Added on 15DEC
		  String chkTaxInclusive="chkTaxInclusive";
		  String hdnDesktopLogo="hdnDesktopLogo";
		  String rdlFetchDay="rdlFetchDay";
		  
		  //Added on 17DEC
		  String hdnSelStaff_="hdnSelStaff_";
		  
		  //Added on 18DEC
		  String chkSMSNotification="chkSMSNotification";
		  String chkEmailNotification="chkEmailNotification";
		  
		  //Added on 19DEC
		  String hdnActualPalnAmt="hdnActualPalnAmt";
		  String hdnActualPalnAmtWithAdmis="hdnActualPalnAmtWithAdmis";
		  
		  //Added on 29DEC
		  String txtReviewDate="txtReviewDate";
		  String txtFat="txtFat";
		  String txtMusWeight="txtMusWeight";
		  String txtBone="txtBone";
		  String txtVisceralFat="txtVisceralFat";
		  String txtWaterContent="txtWaterContent";
		  String txtBMR="txtBMR";
		  String txtMetabolicAge="txtMetabolicAge";
		  
		  //Added on 30DEC
		  String hdnMemberName="hdnMemberName";
		  String hdnCompanyName="hdnCompanyName";
		  
		  String selTransaction_="selTransaction_";
		  String chkIsDefaultTC_="chkIsDefaultTC_";
		  String selTransactionTypeFilter="selTransactionTypeFilter";
		  
		  //Added on DEC31
		  String selSmsType="selSmsType";
		  
		  //Added on 1JAN
		  String selPromotionType="selPromotionType";
		  String selSmsTemplate="selSmsTemplate";
		  String txaSmsContent="txaSmsContent";
		  String hdnMsgType_="hdnMsgType_";
		  
		  //Added on 8JAN
		  String chkSend="chkSend";
		  //Added on 11JAN
		  String selSmsCategory="selSmsCategory";
		  
		  //Added on JAN13
		  String txtSMSAPI="txtSMSAPI";
		  
		  //Added on JAN18
		  String rdlSendTo="rdlSendTo";
		  
		  //Added on JAN18
		  String selMem="selMem";
		  
		  //Added on JAN23
		  String selSendTo="selSendTo";
		  
		  //Added on JAN25
		  String hdnEmailID="hdnEmailID";
		  
		  //Added on FEB1
		  String hdnDashBoardID_="hdnDashBoardID_";
		  String chkAssign_="chkAssign_";
		  
		  //Added on FEB15
		  String selClient="selClient";
		  String selActivity="selActivity";
		  String rdlFetchActivity="rdlFetchActivity";
		  String selMemberShipStatus="selMemberShipStatus";
		  
		  String txtEnqFollowupDays="txtEnqFollowupDays";
		  String chkIsSendSMS="chkIsSendSMS";
		  
		  String txtContinuousAbsentDays="txtContinuousAbsentDays";
		  String txtNoOfAlertSMS="txtNoOfAlertSMS";
		  String txtSMSIntervalDays="txtSMSIntervalDays";
		  
		  //Added for QuickProfile
		  String selQuickNamePrefix = "selQuickNamePrefix";
		  String txtQuickFirstName="txtQuickFirstName";
		  String txtQuickLastName="txtQuickLastName";
		  String selquickProfile="selquickProfile";
		  String hdnQuickEnquiryID="hdnQuickEnquiryID";
		  String hdnQuickCompanyID="hdnQuickCompanyID";
		  String selQuickCompany="selQuickCompany";
		  String txtQuickMobileNo="txtQuickMobileNo";
		  String selQuickStatus="selQuickStatus";
		  String hdnQuickPlanIDForSubscription="hdnQuickPlanIDForSubscription";
		  String selQuickPlan="selQuickPlan";
		  String selQuickBatch="selQuickBatch";
		  String selQuickSubscription="selQuickSubscription";
		  String selQuickStaff="selQuickStaff";
		  String txtQuickDOJ="txtQuickDOJ";
		  String rdlQuickGender="rdlQuickGender";
		  String txtQuickEmail="txtQuickEmail";
		  String selQuickBloodGrp="selQuickBloodGrp";
		  String txaQuickComments="txaQuickComments";
		  String txtQuickDOB="txtQuickDOB";
		  String txtQuickAddress="txtQuickAddress";
		  String txtQuickICEName="txtQuickICEName";
		  String txtQuickICENumber="txtQuickICENumber";
        
		  
		  String FEEDBACKTYPE="FEEDBACKTYPE";
}

