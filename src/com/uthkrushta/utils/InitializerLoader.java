package com.uthkrushta.utils;

import javax.servlet.http.HttpServlet;

public class InitializerLoader extends HttpServlet{
	private Initializer initializer;
	public void init( javax.servlet.ServletConfig config )  throws javax.servlet.ServletException
    {
        // log.info("In Init:==>(called through load on startup)."  );
      initializer = new Initializer();
}
}
