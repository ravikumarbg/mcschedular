package com.uthkrushta.utils;

import java.util.List;



import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.uthkrushta.basis.bean.MessageBean;
import com.uthkrushta.basis.constants.IUMCConstants;
import com.uthkrushta.mm.supreme.control.EntityPlanMasterView;

public class DBUtilStatic {

	//Static calls
	
	@SuppressWarnings("rawtypes")
	public static List getRecords(String strCenterCode,String strTableName, String strWhereClause, String strSortClause) {
		return getRecords(strCenterCode, null,  strTableName,  strWhereClause,  strSortClause);
	}

	@SuppressWarnings("rawtypes")
	public  static List getRecords(String strCenterCode,String strSelectList, String strTableName, String strWhereClause, String strSortClause) {
		Session session = null;
		List lstReturn = null;
		try{
			session = new HibernateUtil().getSessionFactory(strCenterCode).openSession();
			String SQL_QUERY = null;
			if (null == strSelectList || strSelectList.equals("")) {
				SQL_QUERY = "";
			} else {
				SQL_QUERY = "select " + strSelectList;
			}
			SQL_QUERY = SQL_QUERY + " from " + strTableName;
			if (null != strWhereClause && !strWhereClause.equalsIgnoreCase("")) {
				SQL_QUERY = SQL_QUERY + "  where " + strWhereClause + " ";
			}
			if (null != strSortClause && !strSortClause.equalsIgnoreCase("")) {
				SQL_QUERY = SQL_QUERY + " order by " + strSortClause;
			}
			Query query = session.createQuery(SQL_QUERY);
             lstReturn = query.list();
			
			if (null != lstReturn && lstReturn.size() > 0) {
				return lstReturn;
			}
		}catch(HibernateException e){
			e.printStackTrace();
		}finally{
			if(null != session){
				session.flush();
				session.close();
			}
		}
		return null;
	}

	public static Object getRecord(String strCenterCode,String strTableName, String strWhereClause, String strSortClause) {
		return getRecord(strCenterCode,null, strTableName, strWhereClause, strSortClause);
	}
	
	
	public static Object getRecord(String strCenterCode,String strSelectList, String strTableName, String strWhereClause, String strSortClause) {
		Session session = null;
		List lstReturn = null;
		try{
		session = new HibernateUtil().getSessionFactory(strCenterCode).openSession();
		String SQL_QUERY = null;
		if (null == strSelectList || strSelectList.equals("")) {
			SQL_QUERY = "";
		} else {
			SQL_QUERY = "select " + strSelectList;
		}
		SQL_QUERY = SQL_QUERY + " from " + strTableName;
		if (null != strWhereClause && !strWhereClause.equalsIgnoreCase("")) {
			SQL_QUERY = SQL_QUERY + "  where " + strWhereClause + " ";
		}
		if (null != strSortClause && !strSortClause.equalsIgnoreCase("")) {
			SQL_QUERY = SQL_QUERY + " order by " + strSortClause;
		}
		Query query = session.createQuery(SQL_QUERY);
		
		
		
        lstReturn = query.list();
		
		if (null != lstReturn && lstReturn.size() > 0) {
			return lstReturn.get(0);
		}
		}catch(HibernateException e){
			
		}finally{
			if(null != session){
				session.flush();
				session.close();
			}
		}
		return null;
	}
	
	
	public static List getActiveCenters(String strTableName) {
		Session session = HibernateUtil.getSessionFactorySupreme().openSession();
		String SQL_QUERY = "";
		List lstReturn = null;
		SQL_QUERY = SQL_QUERY + " from " + strTableName + "  where intIsActive=1";
		Query query = session.createQuery(SQL_QUERY);
        lstReturn= query.list();
		
		if (null != lstReturn && lstReturn.size() > 0) {
			return lstReturn;
		}
		if(null != session){
			session.flush();
			session.close();
		}
		return null;
	}
	

	public static List getRecordsFromCenter(EntityPlanMasterView objEPMV,String strTableName,String strSelectList,String strWhereClause, String strSortClause) {
		Session session = new HibernateUtil().getSessionFactoryForCenter(objEPMV).openSession();
		String SQL_QUERY = null;
		List lstReturn = null;
		if (null == strSelectList || strSelectList.equals("")) {
			SQL_QUERY = "";
		} else {
			SQL_QUERY = "select " + strSelectList;
		}
		SQL_QUERY = SQL_QUERY + " from " + strTableName;
		if (null != strWhereClause && !strWhereClause.equalsIgnoreCase("")) {
			SQL_QUERY = SQL_QUERY + "  where " + strWhereClause + " ";
		}
		if (null != strSortClause && !strSortClause.equalsIgnoreCase("")) {
			SQL_QUERY = SQL_QUERY + " order by " + strSortClause;
		}
		Query query = session.createQuery(SQL_QUERY);
         lstReturn = query.list();
		
		if (null != lstReturn && lstReturn.size() > 0) {
			return lstReturn;
		}
		if(null != session){
			session.flush();
			session.close();
		}
		return null;
		
	}

	public static Object getRecordFromCenter(EntityPlanMasterView objEPMV,String strTableName,String strSelectList,String strWhereClause, String strSortClause) {
		Session session = new HibernateUtil().getSessionFactoryForCenter(objEPMV).openSession();
		String SQL_QUERY = null;
		List lstReturn = null;
		if (null == strSelectList || strSelectList.equals("")) {
			SQL_QUERY = "";
		} else {
			SQL_QUERY = "select " + strSelectList;
		}
		SQL_QUERY = SQL_QUERY + " from " + strTableName;
		if (null != strWhereClause && !strWhereClause.equalsIgnoreCase("")) {
			SQL_QUERY = SQL_QUERY + "  where " + strWhereClause + " ";
		}
		if (null != strSortClause && !strSortClause.equalsIgnoreCase("")) {
			SQL_QUERY = SQL_QUERY + " order by " + strSortClause;
		}
		Query query = session.createQuery(SQL_QUERY);
            lstReturn = query.list();
		
		if (null != lstReturn && lstReturn.size() > 0) {
			return lstReturn.get(0);
		}
		if(null != session){
			session.flush();
			session.close();
		}
		return null;
		
	}
	
	
	public static boolean updateRecordsOfCenter(EntityPlanMasterView objEPMV,String strTableName, String strUpdateClause, String strWhereClause) throws UMBException {
		String SQL_QUERY = " update " + strTableName;
		Session session = null;
		MessageBean msg = null;
		try {
			session = new HibernateUtil().getSessionFactoryForCenter(objEPMV).openSession();
			if (null != strUpdateClause && !strUpdateClause.equalsIgnoreCase("")) {
				SQL_QUERY = SQL_QUERY + "  set " + strUpdateClause + " ";
			}
			if (null != strWhereClause && !strWhereClause.equalsIgnoreCase("")) {
				SQL_QUERY = SQL_QUERY + "  where " + strWhereClause + " ";
			}
			Transaction tx = session.beginTransaction();
			Query strqu = session.createQuery(SQL_QUERY);
			strqu.executeUpdate();
			tx.commit();
			
		} catch (HibernateException ex) {
			msg = new MessageBean();
			msg.setStrMsgType(IUMCConstants.ERROR);
			msg.setStrMsgText(ex.getMessage());
			return false;
			//throw new UMBException(msg.getStrMsgText(), ex, msg);
		}finally{
		
			if(null != session){
				session.flush();
				session.close();
			}
		}
		return true;
	}
	
	public static double getDoubleFromCenter(EntityPlanMasterView objEPMV,String strTableName,String strSelectList,String strWhereClause, String strSortClause) {
		Session session = new HibernateUtil().getSessionFactoryForCenter(objEPMV).openSession();
		String SQL_QUERY = null;
		List lstReturn = null;
		if (null == strSelectList || strSelectList.equals("")) {
			SQL_QUERY = "";
		} else {
			SQL_QUERY = "select " + strSelectList;
		}
		SQL_QUERY = SQL_QUERY + " from " + strTableName;
		if (null != strWhereClause && !strWhereClause.equalsIgnoreCase("")) {
			SQL_QUERY = SQL_QUERY + "  where " + strWhereClause + " ";
		}
		if (null != strSortClause && !strSortClause.equalsIgnoreCase("")) {
			SQL_QUERY = SQL_QUERY + " order by " + strSortClause;
		}
		Query query = session.createQuery(SQL_QUERY);
        lstReturn = query.list();
		
		if (null != lstReturn && lstReturn.size() > 0) {
			return lstReturn.get(0) != null ? ((Double)lstReturn.get(0)).doubleValue() : 0;
		}
		if(null != session){
			session.flush();
			session.close();
		}
		return 0;
		
	}
	
	//Added new Method for getting the object
	public static Object getRecordFromSup(String strTableName) {
		Session session = HibernateUtil.getSessionFactorySupreme().openSession();
		List lstReturn = null;
		String SQL_QUERY = "";
 
		SQL_QUERY = SQL_QUERY + " from " + strTableName + "  where intStatus=2";
		Query query = session.createQuery(SQL_QUERY);
		 lstReturn = query.list();
			
			if (null != lstReturn && lstReturn.size() > 0) {
				return lstReturn.get(0);
			}
		if(null != session){
			session.flush();
			session.close();
		}
		return null;
	}
	
	//Added new Method for getting the object including where and sort clause
		public static Object getRecordFromSup(String strTableName, String strWhereClause, String strSortClause ) {
			Session session = HibernateUtil.getSessionFactorySupreme().openSession();
			List lstReturn = null;
			String SQL_QUERY = "";
	 
			SQL_QUERY = SQL_QUERY + " from " + strTableName;
			if (null != strWhereClause && !strWhereClause.equalsIgnoreCase("")) {
				SQL_QUERY = SQL_QUERY + "  where " + strWhereClause + " ";
			}
			if (null != strSortClause && !strSortClause.equalsIgnoreCase("")) {
				SQL_QUERY = SQL_QUERY + " order by " + strSortClause;
			}
			
			Query query = session.createQuery(SQL_QUERY);
			 lstReturn = query.list();
				
				if (null != lstReturn && lstReturn.size() > 0) {
					return lstReturn.get(0);
				}
			if(null != session){
				session.flush();
				session.close();
			}
			return null;
		}
	
	
	
	// Modify record generic
		public static boolean modifyRecord(EntityPlanMasterView objEPMV,Object objRecord) throws UMBException {
			Session session = null;
			UMBException exUAVS = null;
			MessageBean msg = null;
			try {
				session = new HibernateUtil().getSessionFactoryForCenter(objEPMV).openSession();
				Transaction tx = null;
				tx = session.beginTransaction();
				session.saveOrUpdate(objRecord);
				tx.commit();
				
			} catch (HibernateException ex) {
				msg = new MessageBean();
				msg.setStrMsgText(ex.getMessage());
				exUAVS = new UMBException("Hibernate Exception", ex, msg);
				throw exUAVS;
			} finally {
				if (null != session) {
					session.flush();
					session.close();
				}
			}
			return true;
		}
	
}
