package com.uthkrushta.mm.master.bean;

import java.util.Date;

public class MasterMemberBeanView {
	private long lngID;
	private long lngNamePrefixID;
	private String strName;
	private long lngCompanyID;
	private long lngMemberStatus;
	private String strMemberCode;
	private long lngMemberID;
	private Date dtDOJ;
	private String strPhotoPath;
	private long lngBatchID;
	private long lngSubscriptionID;
	private long lngPlanID;
	private long lngStaffID;
	private Date dtNextRenewalDate;
	private int intNotificationCounter;
	private Date dtLastAttendedDate;
	private double dbMemeberWeight;
	private double dbMemberHeight;
	private double dbMemberBmi;
	private long lngBloodGroupID;
	private String strMedicalHistory;
	private long lngGenderID;
	private Date dtDOB;
	private long lngOccupationID;
	private String strEmailID;
	private String strMobileNumber;
	private String strLandlineNumber;
	private String strAddress;
	private long lngAreaID;
	private long lngCityID;
	private long lngStateID;
	private long lngCountryID;
	private String strICEName;
	private String strICENumber;
	private long lngICERelationshipID;
	private String strUserName;
	private String strPassword;
	private long lngSecurityQuestionID;
	private String strSecurityAnswer;
	private long lngEnquiryID;
	private int intStatus;
	private long lngCreatedBy;
	private Date dtCreatedOn;
	private long lngUpdatedBy;
	private Date dtUpdatedOn;
	private double dbAmountPayable;
	private double dbAmountPaid;
	private double dbActualAmount;
	private String strPincode;
	
	int intIsPaused;
	long lngPauseTypeID;
	
	//Added field 28MARCH
	String strLastName;
	//Added newly
    String strCompanyName;
    String strMemberName;
    Date dtPlanEndDate;
	
    long lngFinancialYearID;
    Date dtDOM;
	
	 //Added on DEC8
	 int intMartialStatus;
	 
	 //Added on DEC18
	 int intSMSNotification;
	 int intEmailNotification;
	 
	 //Added on MARCH25
	 int intSMSSentCount;
	 Date dtLastSentSMS;
	 
	 //Added for rewards JUN1
	 double dblRewardPoints;
	 
	 //Added for salesTrack Jun2
	 double dblSalesTrack;
	 
	 //Added due to biometric ID
	 long lngBiometricID;
	 
	 
	 //Added for feedback frequency
	 long lngFrequencyDays;
	 Date dtNextFeedbackDate;
	 
	 
	 private int intBMI;
		
		private int intQuestionaire;
		private int intFitnessTest;
		private int intMeasurements;
		private int intCounseling;
		private int intWorkoutCard;
		private String strNote;
		private int intDone;
	    private long lngMemAssessmntID;	
	    private long lngMemAssessCreatedBy;
	    private Date dtMemAssessCreatedOn;
	    
	    //Added  new fields for memberview related to serviceform
	    private long lngCurServiceCount;
	    
	    private String strServiceType;
	    private String strServiceStaffID;
	    
	    private double dblMemBalanceAmt;
	    private long lngRefStaffId;
	    private String strRefStaffCode;
	    private String strRefStaffName;
		private Date dtMemberAddedDate;
		
		//Added for service due amount
		private double dblTotalServicePaymentDue;
		
		private int intIsVIP;
    
	public String getStrLastName() {
		return strLastName;
	}
	public void setStrLastName(String strLastName) {
		this.strLastName = strLastName;
	}
	public int getIntIsPaused() {
		return intIsPaused;
	}
	public void setIntIsPaused(int intIsPaused) {
		this.intIsPaused = intIsPaused;
	}
	public long getLngPauseTypeID() {
		return lngPauseTypeID;
	}
	public void setLngPauseTypeID(long lngPauseTypeID) {
		this.lngPauseTypeID = lngPauseTypeID;
	}
	String strNamePrefix;
	String strMemStatus;
	String strBatchName;
	String strSubscriptionName;
	String strPlanName;
	String strStaffName;
	String strGenderName;
	String strOccupName;
	String strAreaName;
	String strCityName;
	String strStateName;
	String strCountryName;
	String strIceRelName;
	
	
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public long getLngNamePrefixID() {
		return lngNamePrefixID;
	}
	public void setLngNamePrefixID(long lngNamePrefixID) {
		this.lngNamePrefixID = lngNamePrefixID;
	}
	public String getStrName() {
		return strName;
	}
	public void setStrName(String strName) {
		this.strName = strName;
	}
	public long getLngCompanyID() {
		return lngCompanyID;
	}
	public void setLngCompanyID(long lngCompanyID) {
		this.lngCompanyID = lngCompanyID;
	}
	public long getLngMemberStatus() {
		return lngMemberStatus;
	}
	public void setLngMemberStatus(long lngMemberStatus) {
		this.lngMemberStatus = lngMemberStatus;
	}
	public String getStrMemberCode() {
		return strMemberCode;
	}
	public void setStrMemberCode(String strMemberCode) {
		this.strMemberCode = strMemberCode;
	}
	public long getLngMemberID() {
		return lngMemberID;
	}
	public void setLngMemberID(long lngMemberID) {
		this.lngMemberID = lngMemberID;
	}
	public Date getDtDOJ() {
		return dtDOJ;
	}
	public void setDtDOJ(Date dtDOJ) {
		this.dtDOJ = dtDOJ;
	}
	public String getStrPhotoPath() {
		return strPhotoPath;
	}
	public void setStrPhotoPath(String strPhotoPath) {
		this.strPhotoPath = strPhotoPath;
	}
	public long getLngBatchID() {
		return lngBatchID;
	}
	public void setLngBatchID(long lngBatchID) {
		this.lngBatchID = lngBatchID;
	}
	public long getLngSubscriptionID() {
		return lngSubscriptionID;
	}
	public void setLngSubscriptionID(long lngSubscriptionID) {
		this.lngSubscriptionID = lngSubscriptionID;
	}
	public long getLngPlanID() {
		return lngPlanID;
	}
	public void setLngPlanID(long lngPlanID) {
		this.lngPlanID = lngPlanID;
	}
	public long getLngStaffID() {
		return lngStaffID;
	}
	public void setLngStaffID(long lngStaffID) {
		this.lngStaffID = lngStaffID;
	}
	public Date getDtNextRenewalDate() {
		return dtNextRenewalDate;
	}
	public void setDtNextRenewalDate(Date dtNextRenewalDate) {
		this.dtNextRenewalDate = dtNextRenewalDate;
	}
	public int getIntNotificationCounter() {
		return intNotificationCounter;
	}
	public void setIntNotificationCounter(int intNotificationCounter) {
		this.intNotificationCounter = intNotificationCounter;
	}
	public Date getDtLastAttendedDate() {
		return dtLastAttendedDate;
	}
	public void setDtLastAttendedDate(Date dtLastAttendedDate) {
		this.dtLastAttendedDate = dtLastAttendedDate;
	}
	public double getDbMemeberWeight() {
		return dbMemeberWeight;
	}
	public void setDbMemeberWeight(double dbMemeberWeight) {
		this.dbMemeberWeight = dbMemeberWeight;
	}
	public double getDbMemberHeight() {
		return dbMemberHeight;
	}
	public void setDbMemberHeight(double dbMemberHeight) {
		this.dbMemberHeight = dbMemberHeight;
	}
	public double getDbMemberBmi() {
		return dbMemberBmi;
	}
	public void setDbMemberBmi(double dbMemberBmi) {
		this.dbMemberBmi = dbMemberBmi;
	}
	public long getLngBloodGroupID() {
		return lngBloodGroupID;
	}
	public void setLngBloodGroupID(long lngBloodGroupID) {
		this.lngBloodGroupID = lngBloodGroupID;
	}
	public String getStrMedicalHistory() {
		return strMedicalHistory;
	}
	public void setStrMedicalHistory(String strMedicalHistory) {
		this.strMedicalHistory = strMedicalHistory;
	}
	public long getLngGenderID() {
		return lngGenderID;
	}
	public void setLngGenderID(long lngGenderID) {
		this.lngGenderID = lngGenderID;
	}
	public Date getDtDOB() {
		return dtDOB;
	}
	public void setDtDOB(Date dtDOB) {
		this.dtDOB = dtDOB;
	}
	public long getLngOccupationID() {
		return lngOccupationID;
	}
	public void setLngOccupationID(long lngOccupationID) {
		this.lngOccupationID = lngOccupationID;
	}
	public String getStrEmailID() {
		return strEmailID;
	}
	public void setStrEmailID(String strEmailID) {
		this.strEmailID = strEmailID;
	}
	public String getStrMobileNumber() {
		return strMobileNumber;
	}
	public void setStrMobileNumber(String strMobileNumber) {
		this.strMobileNumber = strMobileNumber;
	}
	public String getStrLandlineNumber() {
		return strLandlineNumber;
	}
	public void setStrLandlineNumber(String strLandlineNumber) {
		this.strLandlineNumber = strLandlineNumber;
	}
	public String getStrAddress() {
		return strAddress;
	}
	public void setStrAddress(String strAddress) {
		this.strAddress = strAddress;
	}
	public long getLngAreaID() {
		return lngAreaID;
	}
	public void setLngAreaID(long lngAreaID) {
		this.lngAreaID = lngAreaID;
	}
	public long getLngCityID() {
		return lngCityID;
	}
	public void setLngCityID(long lngCityID) {
		this.lngCityID = lngCityID;
	}
	public long getLngStateID() {
		return lngStateID;
	}
	public void setLngStateID(long lngStateID) {
		this.lngStateID = lngStateID;
	}
	public long getLngCountryID() {
		return lngCountryID;
	}
	public void setLngCountryID(long lngCountryID) {
		this.lngCountryID = lngCountryID;
	}
	public String getStrICEName() {
		return strICEName;
	}
	public void setStrICEName(String strICEName) {
		this.strICEName = strICEName;
	}
	public String getStrICENumber() {
		return strICENumber;
	}
	public void setStrICENumber(String strICENumber) {
		this.strICENumber = strICENumber;
	}
	public long getLngICERelationshipID() {
		return lngICERelationshipID;
	}
	public void setLngICERelationshipID(long lngICERelationshipID) {
		this.lngICERelationshipID = lngICERelationshipID;
	}
	public String getStrUserName() {
		return strUserName;
	}
	public void setStrUserName(String strUserName) {
		this.strUserName = strUserName;
	}
	public String getStrPassword() {
		return strPassword;
	}
	public void setStrPassword(String strPassword) {
		this.strPassword = strPassword;
	}
	public long getLngSecurityQuestionID() {
		return lngSecurityQuestionID;
	}
	public void setLngSecurityQuestionID(long lngSecurityQuestionID) {
		this.lngSecurityQuestionID = lngSecurityQuestionID;
	}
	public String getStrSecurityAnswer() {
		return strSecurityAnswer;
	}
	public void setStrSecurityAnswer(String strSecurityAnswer) {
		this.strSecurityAnswer = strSecurityAnswer;
	}
	public long getLngEnquiryID() {
		return lngEnquiryID;
	}
	public void setLngEnquiryID(long lngEnquiryID) {
		this.lngEnquiryID = lngEnquiryID;
	}
	public int getIntStatus() {
		return intStatus;
	}
	public void setIntStatus(int intStatus) {
		this.intStatus = intStatus;
	}
	public long getLngCreatedBy() {
		return lngCreatedBy;
	}
	public void setLngCreatedBy(long lngCreatedBy) {
		this.lngCreatedBy = lngCreatedBy;
	}
	public Date getDtCreatedOn() {
		return dtCreatedOn;
	}
	public void setDtCreatedOn(Date dtCreatedOn) {
		this.dtCreatedOn = dtCreatedOn;
	}
	public long getLngUpdatedBy() {
		return lngUpdatedBy;
	}
	public void setLngUpdatedBy(long lngUpdatedBy) {
		this.lngUpdatedBy = lngUpdatedBy;
	}
	public Date getDtUpdatedOn() {
		return dtUpdatedOn;
	}
	public void setDtUpdatedOn(Date dtUpdatedOn) {
		this.dtUpdatedOn = dtUpdatedOn;
	}
	public double getDbAmountPayable() {
		return dbAmountPayable;
	}
	public void setDbAmountPayable(double dbAmountPayable) {
		this.dbAmountPayable = dbAmountPayable;
	}
	public double getDbAmountPaid() {
		return dbAmountPaid;
	}
	public void setDbAmountPaid(double dbAmountPaid) {
		this.dbAmountPaid = dbAmountPaid;
	}
	public double getDbActualAmount() {
		return dbActualAmount;
	}
	public void setDbActualAmount(double dbActualAmount) {
		this.dbActualAmount = dbActualAmount;
	}
	public String getStrPincode() {
		return strPincode;
	}
	public void setStrPincode(String strPincode) {
		this.strPincode = strPincode;
	}
	public String getStrNamePrefix() {
		return strNamePrefix;
	}
	public void setStrNamePrefix(String strNamePrefix) {
		this.strNamePrefix = strNamePrefix;
	}
	public String getStrMemStatus() {
		return strMemStatus;
	}
	public void setStrMemStatus(String strMemStatus) {
		this.strMemStatus = strMemStatus;
	}
	public String getStrBatchName() {
		return strBatchName;
	}
	public void setStrBatchName(String strBatchName) {
		this.strBatchName = strBatchName;
	}
	public String getStrSubscriptionName() {
		return strSubscriptionName;
	}
	public void setStrSubscriptionName(String strSubscriptionName) {
		this.strSubscriptionName = strSubscriptionName;
	}
	public String getStrPlanName() {
		return strPlanName;
	}
	public void setStrPlanName(String strPlanName) {
		this.strPlanName = strPlanName;
	}
	public String getStrStaffName() {
		return strStaffName;
	}
	public void setStrStaffName(String strStaffName) {
		this.strStaffName = strStaffName;
	}
	public String getStrGenderName() {
		return strGenderName;
	}
	public void setStrGenderName(String strGenderName) {
		this.strGenderName = strGenderName;
	}
	public String getStrOccupName() {
		return strOccupName;
	}
	public void setStrOccupName(String strOccupName) {
		this.strOccupName = strOccupName;
	}
	public String getStrAreaName() {
		return strAreaName;
	}
	public void setStrAreaName(String strAreaName) {
		this.strAreaName = strAreaName;
	}
	public String getStrCityName() {
		return strCityName;
	}
	public void setStrCityName(String strCityName) {
		this.strCityName = strCityName;
	}
	public String getStrStateName() {
		return strStateName;
	}
	public void setStrStateName(String strStateName) {
		this.strStateName = strStateName;
	}
	public String getStrCountryName() {
		return strCountryName;
	}
	public void setStrCountryName(String strCountryName) {
		this.strCountryName = strCountryName;
	}
	public String getStrIceRelName() {
		return strIceRelName;
	}
	public void setStrIceRelName(String strIceRelName) {
		this.strIceRelName = strIceRelName;
	}
	public String getStrCompanyName() {
		return strCompanyName;
	}
	public void setStrCompanyName(String strCompanyName) {
		this.strCompanyName = strCompanyName;
	}
	public String getStrMemberName() {
		return strMemberName;
	}
	public void setStrMemberName(String strMemberName) {
		this.strMemberName = strMemberName;
	}
	public Date getDtPlanEndDate() {
		return dtPlanEndDate;
	}
	public void setDtPlanEndDate(Date dtPlanEndDate) {
		this.dtPlanEndDate = dtPlanEndDate;
	}
	public long getLngBiometricID() {
		return lngBiometricID;
	}
	public void setLngBiometricID(long lngBiometricID) {
		this.lngBiometricID = lngBiometricID;
	}
	public long getLngFinancialYearID() {
		return lngFinancialYearID;
	}
	public void setLngFinancialYearID(long lngFinancialYearID) {
		this.lngFinancialYearID = lngFinancialYearID;
	}
	public Date getDtDOM() {
		return dtDOM;
	}
	public void setDtDOM(Date dtDOM) {
		this.dtDOM = dtDOM;
	}
	public int getIntMartialStatus() {
		return intMartialStatus;
	}
	public void setIntMartialStatus(int intMartialStatus) {
		this.intMartialStatus = intMartialStatus;
	}
	public int getIntSMSNotification() {
		return intSMSNotification;
	}
	public void setIntSMSNotification(int intSMSNotification) {
		this.intSMSNotification = intSMSNotification;
	}
	public int getIntEmailNotification() {
		return intEmailNotification;
	}
	public void setIntEmailNotification(int intEmailNotification) {
		this.intEmailNotification = intEmailNotification;
	}
	public int getIntSMSSentCount() {
		return intSMSSentCount;
	}
	public void setIntSMSSentCount(int intSMSSentCount) {
		this.intSMSSentCount = intSMSSentCount;
	}
	public Date getDtLastSentSMS() {
		return dtLastSentSMS;
	}
	public void setDtLastSentSMS(Date dtLastSentSMS) {
		this.dtLastSentSMS = dtLastSentSMS;
	}
	public double getDblRewardPoints() {
		return dblRewardPoints;
	}
	public void setDblRewardPoints(double dblRewardPoints) {
		this.dblRewardPoints = dblRewardPoints;
	}
	public double getDblSalesTrack() {
		return dblSalesTrack;
	}
	public void setDblSalesTrack(double dblSalesTrack) {
		this.dblSalesTrack = dblSalesTrack;
	}
	public long getLngFrequencyDays() {
		return lngFrequencyDays;
	}
	public void setLngFrequencyDays(long lngFrequencyDays) {
		this.lngFrequencyDays = lngFrequencyDays;
	}
	public Date getDtNextFeedbackDate() {
		return dtNextFeedbackDate;
	}
	public void setDtNextFeedbackDate(Date dtNextFeedbackDate) {
		this.dtNextFeedbackDate = dtNextFeedbackDate;
	}
	public int getIntBMI() {
		return intBMI;
	}
	public void setIntBMI(int intBMI) {
		this.intBMI = intBMI;
	}
	public int getIntQuestionaire() {
		return intQuestionaire;
	}
	public void setIntQuestionaire(int intQuestionaire) {
		this.intQuestionaire = intQuestionaire;
	}
	public int getIntFitnessTest() {
		return intFitnessTest;
	}
	public void setIntFitnessTest(int intFitnessTest) {
		this.intFitnessTest = intFitnessTest;
	}
	public int getIntMeasurements() {
		return intMeasurements;
	}
	public void setIntMeasurements(int intMeasurements) {
		this.intMeasurements = intMeasurements;
	}
	public int getIntCounseling() {
		return intCounseling;
	}
	public void setIntCounseling(int intCounseling) {
		this.intCounseling = intCounseling;
	}
	public int getIntWorkoutCard() {
		return intWorkoutCard;
	}
	public void setIntWorkoutCard(int intWorkoutCard) {
		this.intWorkoutCard = intWorkoutCard;
	}
	public String getStrNote() {
		return strNote;
	}
	public void setStrNote(String strNote) {
		this.strNote = strNote;
	}
	public int getIntDone() {
		return intDone;
	}
	public void setIntDone(int intDone) {
		this.intDone = intDone;
	}
	public long getLngMemAssessmntID() {
		return lngMemAssessmntID;
	}
	public void setLngMemAssessmntID(long lngMemAssessmntID) {
		this.lngMemAssessmntID = lngMemAssessmntID;
	}
	public long getLngMemAssessCreatedBy() {
		return lngMemAssessCreatedBy;
	}
	public void setLngMemAssessCreatedBy(long lngMemAssessCreatedBy) {
		this.lngMemAssessCreatedBy = lngMemAssessCreatedBy;
	}
	public Date getDtMemAssessCreatedOn() {
		return dtMemAssessCreatedOn;
	}
	public void setDtMemAssessCreatedOn(Date dtMemAssessCreatedOn) {
		this.dtMemAssessCreatedOn = dtMemAssessCreatedOn;
	}
	public long getLngCurServiceCount() {
		return lngCurServiceCount;
	}
	public void setLngCurServiceCount(long lngCurServiceCount) {
		this.lngCurServiceCount = lngCurServiceCount;
	}
	public String getStrServiceType() {
		return strServiceType;
	}
	public void setStrServiceType(String strServiceType) {
		this.strServiceType = strServiceType;
	}
	public String getStrServiceStaffID() {
		return strServiceStaffID;
	}
	public void setStrServiceStaffID(String strServiceStaffID) {
		this.strServiceStaffID = strServiceStaffID;
	}
	public double getDblMemBalanceAmt() {
		return dblMemBalanceAmt;
	}
	public void setDblMemBalanceAmt(double dblMemBalanceAmt) {
		this.dblMemBalanceAmt = dblMemBalanceAmt;
	}
	public long getLngRefStaffId() {
		return lngRefStaffId;
	}
	public void setLngRefStaffId(long lngRefStaffId) {
		this.lngRefStaffId = lngRefStaffId;
	}
	public String getStrRefStaffCode() {
		return strRefStaffCode;
	}
	public void setStrRefStaffCode(String strRefStaffCode) {
		this.strRefStaffCode = strRefStaffCode;
	}
	public String getStrRefStaffName() {
		return strRefStaffName;
	}
	public void setStrRefStaffName(String strRefStaffName) {
		this.strRefStaffName = strRefStaffName;
	}
	public Date getDtMemberAddedDate() {
		return dtMemberAddedDate;
	}
	public void setDtMemberAddedDate(Date dtMemberAddedDate) {
		this.dtMemberAddedDate = dtMemberAddedDate;
	}
	public double getDblTotalServicePaymentDue() {
		return dblTotalServicePaymentDue;
	}
	public void setDblTotalServicePaymentDue(double dblTotalServicePaymentDue) {
		this.dblTotalServicePaymentDue = dblTotalServicePaymentDue;
	}
	public int getIntIsVIP() {
		return intIsVIP;
	}
	public void setIntIsVIP(int intIsVIP) {
		this.intIsVIP = intIsVIP;
	}
	
	
	
	
	

}
