package com.uthkrushta.basis.constants;

public interface ITextConstants {
	
	public static final String MSG_TRANSFER_FAILED = "Member Transfer Failed!";
	public static final String MSG_TRANSFER_SUCCESS = "Member Transferred Successfully!";
//Newly Added on 10/08
	public static final String MSG_APPROVAL_ERROR = "Error occured while Approving ";
	public static final String MSG_APPROVED = "Approved Successfully";
	//public static final String MSG_DELETE_ERROR = "Error occured while Deleting selected record, please try again.";
	public static final String MSG_ACTION_ERROR = "Error occured while performing the action, please try again.";

	
	public static final String MSG_DELETE_ERROR = " Delete Error";
	public static final String MSG_MAIL_ERROR = " Mail sending failed";
	public static final String MSG_EMAIL_MATCHING_FAILED = "Email Not Matching";
	public static final String MSG_RESET_EMAIL = "Reset Link sent to your email";
	public static final String MSG_PASSWORD_RESET_SUCCESS = "Password has been Reseted Successfully";
	public static final String MSG_DELETE_SUCCESS = "Deleted Successfully";
	public static final String MSG_DELETE_FAILED = "Delete Failed";
	public static final String MSG_SECURITY_ANSWER = " Answer did not match,please try again.";
	public static final String STR_MOD = " modifed.";
	public static final String STR_DEL = " deleted.";
	public static final String STR_TWO_COLONS = " :: ";
	public static final String STR_SEL_IDS = " selected ID's " + STR_TWO_COLONS;
	
	public static final String MSG_SIGN_IN_SUCCESS = "Signed In Successfully";
	public static final String MSG_SIGN_OUT_SUCCESS = "Signed Out Successfully";
	
	public static final String MSG_SIGN_IN_FAILED = "Sign In Failed!";
	public static final String MSG_SIGN_OUT_FAILED = "Sign Out Failed!";
	
	public static final String MSG_SAVED_SUCCESS = "Saved Successfully";
	public static final String MSG_DELETED_SUCCESS = "Deleted Successfully";
	public static final String MSG_DELETED_ACTIVE_ROW = "Deleted Successfully Excluding Active Batch.";
	
	public static final String MSG_SAVED_FAILED = "Save Failed!";
	public static final String MSG_DELETED_FAILED = "Delete Failed!";
	public static final String MSG_DELETED_ACTIVE = "Active Batches can't be deleted";
	public static final String MSG_GET_FAILED = "Action Failed! Try Again.";
	public static final String MSG_ACTION_FAILED = "Action Failed! Try Again.";
	
	public static final String MSG_DUPLICATE_ENTRY = " already exists. ";
	public static final String MSG_REJECTED_SUCCESS = " Selected records Rejected Successfully";
	public static final String MSG_APPROVED_SUCCESSFULLY = " Selected records Approved Successfully";
	public static final String MSG_DUPLICATE_USERNAME = " User Name already exists";
	public static final String MSG_MAIL_SENT = " Mail Sent Successfully to ";
	
	public static final String MSG_INVALID_LOGIN = "Invalid User Name or Password or Code";
	public static final String MSG_INVALID_USER = "Invalid User Name";
	
	public static final String MSG_SAVED_ERROR = "Error occured while Saving, please try again.";
	
	public static final String MSG_COULD_NOT_CANCEL="Could not cancel the invoice";
	public static final String MSG_CANCELLED_SUCCESS = "Cancelled Successfully";
	
	//Added on DEC3
	public static final String MSG_MAIL_FAILED = " Mail Sending Failed !!";
	
	//Added on DEC18
	public static final String MSG_ROLE_ASSIGNED =" ERROR:Could not delete the selected role assigned to Staff ";
    //Added on DEC22
	public static final String MSG_MAIL_SENT_SUCCESS="Mail Sent Successfully";
	//Added on 1JAN
	public static final String MSG_SAVED_AND_SENT_SUCCESS="Message Saved and Sent Successfully";
	public static final String MSG_SENDING_FAILED = "Message Sending Failed";
	public static final String MSG_ACTION_ACTIVATE_SUCCESS = "Successfully Activated";
	public static final String MSG_ACTION_ACTIVATE_FAILED = "Activation Failed";
	public static final String MSG_ACTION_DEACTIVATE_SUCCESS = "Successfully Deactivated";
	public static final String MSG_ACTION_DEACTIVATE_FAILED = "Deactivation Failed";
}
