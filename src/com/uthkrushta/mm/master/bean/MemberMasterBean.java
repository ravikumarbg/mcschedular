package com.uthkrushta.mm.master.bean;

import java.util.Date;

public class MemberMasterBean {

	public double getDblRewardPoints() {
		return dblRewardPoints;
	}

	public void setDblRewardPoints(double dblRewardPoints) {
		this.dblRewardPoints = dblRewardPoints;
	}

	public double getDblSalesTrack() {
		return dblSalesTrack;
	}

	public void setDblSalesTrack(double dblSalesTrack) {
		this.dblSalesTrack = dblSalesTrack;
	}

	private long lngID;
	private long lngNamePrefixID;
	 String strName;
	 String strLastName;//include
	private long lngCompanyID;
	private long lngMemberStatus;
	private String strMemberCode;
	private long lngMemberID;
	private Date dtDOJ;
	private String strPhotoPath;
	private long lngBatchID;
	private long lngSubscriptionID;
	private long lngPlanID;
	private long lngStaffID;
	private Date dtNextRenewalDate;
	private int intNotificationCounter;
	private Date dtLastAttendedDate;
	private double dbMemeberWeight;
	private double dbMemberHeight;
	private double dbMemberBmi;
	private long lngBloodGroupID;
	private String strMedicalHistory;
	private long lngGenderID;
	private Date dtDOB;
	private long lngOccupationID;
	private String strEmailID;
	private String strMobileNumber;
	private String strLandlineNumber;
	private String strAddress;
	private long lngAreaID;
	private long lngCityID;
	private long lngStateID;
	private long lngCountryID;
	private String strICEName;
	private String strICENumber;
	private long lngICERelationshipID;
	private String strUserName;
	private String strPassword;
	private long lngSecurityQuestionID;
	private String strSecurityAnswer;
	private long lngEnquiryID;
	private int intStatus;
	private long lngCreatedBy;
	private Date dtCreatedOn;
	private long lngUpdatedBy;
	private Date dtUpdatedOn;
	private double dbAmountPayable;
	private double dbAmountPaid;
	private double dbActualAmount;
	String strPincode;
	long lngFinancialYearID;
	
	
	
	int intIsPaused;
	long lngRemainingDays;
	long lngPauseTypeID;
	
	long lngUomWieght;
	long lngUomHieght;
	
	String strComments;
	
	Date dtDOM;
	
	 //Added on DEC8
	 int intMartialStatus;
	 
	 //Added on DEC18
	 int intSMSNotification;
	 int intEmailNotification;
	 
	 //Added on MARCH25
	 int intSMSSentCount;
	 Date dtLastSentSMS;
	 //Added for rewards JUN1
	 double dblRewardPoints;
	 
	 //Added for salesTrack Jun2
	 double dblSalesTrack;
	 
	 
	 //Added due to biometric ID
	 long lngBiometricID;
	 
	 //Added for feedback frequency
	 long lngFrequencyDays;
	 Date dtNextFeedbackDate;
	
	public int getIntSMSSentCount() {
		return intSMSSentCount;
	}

	public void setIntSMSSentCount(int intSMSSentCount) {
		this.intSMSSentCount = intSMSSentCount;
	}

	public Date getDtLastSentSMS() {
		return dtLastSentSMS;
	}

	public void setDtLastSentSMS(Date dtLastSentSMS) {
		this.dtLastSentSMS = dtLastSentSMS;
	}

	public long getLngFinancialYearID() {
		return lngFinancialYearID;
	}

	public void setLngFinancialYearID(long lngFinancialYearID) {
		this.lngFinancialYearID = lngFinancialYearID;
	}
	
	public String getStrComments() {
		return strComments;
	}

	public void setStrComments(String strComments) {
		this.strComments = strComments;
	}

	public long getLngUomWieght() {
		return lngUomWieght;
	}

	public void setLngUomWieght(long lngUomWieght) {
		this.lngUomWieght = lngUomWieght;
	}

	public long getLngUomHieght() {
		return lngUomHieght;
	}

	public void setLngUomHieght(long lngUomHieght) {
		this.lngUomHieght = lngUomHieght;
	}

	public Date getDtDOM() {
		return dtDOM;
	}

	public void setDtDOM(Date dtDOM) {
		this.dtDOM = dtDOM;
	}

	public long getLngPauseTypeID() {
		return lngPauseTypeID;
	}

	public void setLngPauseTypeID(long lngPauseTypeID) {
		this.lngPauseTypeID = lngPauseTypeID;
	}

	public long getLngRemainingDays() {
		return lngRemainingDays;
	}

	public void setLngRemainingDays(long lngRemainingDays) {
		this.lngRemainingDays = lngRemainingDays;
	}

	public int getIntIsPaused() {
		return intIsPaused;
	}

	public void setIntIsPaused(int intIsPaused) {
		this.intIsPaused = intIsPaused;
	}

	public String getStrPincode() {
		return strPincode;
	}

	public void setStrPincode(String strPincode) {
		this.strPincode = strPincode;
	}
	
	
	
	public MemberMasterBean() {
		super();
		strName = strLastName = strMemberCode = strPhotoPath = strPincode = strMedicalHistory = strEmailID = strMobileNumber = strLandlineNumber = strAddress = strICEName
				= strICENumber = strUserName = strPassword = strSecurityAnswer = strComments = "";
		//dtDOJ = WebUtil.getCurrentDate(request);

	}
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public long getLngNamePrefixID() {
		return lngNamePrefixID;
	}
	public void setLngNamePrefixID(long lngNamePrefixID) {
		this.lngNamePrefixID = lngNamePrefixID;
	}
	public String getStrName() {
		return strName;
	}

	public void setStrName(String strName) {
		this.strName = strName;
	}

	public String getStrLastName() {
		return strLastName;
	}

	public void setStrLastName(String strLastName) {
		this.strLastName = strLastName;
	}

	public long getLngCompanyID() {
		return lngCompanyID;
	}
	public void setLngCompanyID(long lngCompanyID) {
		this.lngCompanyID = lngCompanyID;
	}
	public long getLngMemberStatus() {
		return lngMemberStatus;
	}
	public void setLngMemberStatus(long lngMemberStatus) {
		this.lngMemberStatus = lngMemberStatus;
	}
	public String getStrMemberCode() {
		return strMemberCode;
	}
	public void setStrMemberCode(String strMemberCode) {
		this.strMemberCode = strMemberCode;
	}
	public long getLngMemberID() {
		return lngMemberID;
	}
	public void setLngMemberID(long lngMemberID) {
		this.lngMemberID = lngMemberID;
	}
	
	public String getStrPhotoPath() {
		return strPhotoPath;
	}
	public void setStrPhotoPath(String strPhotoPath) {
		this.strPhotoPath = strPhotoPath;
	}
	public long getLngBatchID() {
		return lngBatchID;
	}
	public void setLngBatchID(long lngBatchID) {
		this.lngBatchID = lngBatchID;
	}
	public long getLngSubscriptionID() {
		return lngSubscriptionID;
	}
	public void setLngSubscriptionID(long lngSubscriptionID) {
		this.lngSubscriptionID = lngSubscriptionID;
	}
	public long getLngPlanID() {
		return lngPlanID;
	}
	public void setLngPlanID(long lngPlanID) {
		this.lngPlanID = lngPlanID;
	}
	public long getLngStaffID() {
		return lngStaffID;
	}
	public void setLngStaffID(long lngStaffID) {
		this.lngStaffID = lngStaffID;
	}
	public Date getDtNextRenewalDate() {
		return dtNextRenewalDate;
	}
	public void setDtNextRenewalDate(Date dtNextRenewalDate) {
		this.dtNextRenewalDate = dtNextRenewalDate;
	}
	public int getIntNotificationCounter() {
		return intNotificationCounter;
	}
	public void setIntNotificationCounter(int intNotificationCounter) {
		this.intNotificationCounter = intNotificationCounter;
	}
	public Date getDtLastAttendedDate() {
		return dtLastAttendedDate;
	}
	public void setDtLastAttendedDate(Date dtLastAttendedDate) {
		this.dtLastAttendedDate = dtLastAttendedDate;
	}
	public double getDbMemeberWeight() {
		return dbMemeberWeight;
	}
	public void setDbMemeberWeight(double dbMemeberWeight) {
		this.dbMemeberWeight = dbMemeberWeight;
	}
	public double getDbMemberHeight() {
		return dbMemberHeight;
	}
	public void setDbMemberHeight(double dbMemberHeight) {
		this.dbMemberHeight = dbMemberHeight;
	}
	public double getDbMemberBmi() {
		return dbMemberBmi;
	}
	public void setDbMemberBmi(double dbMemberBmi) {
		this.dbMemberBmi = dbMemberBmi;
	}
	public long getLngBloodGroupID() {
		return lngBloodGroupID;
	}
	public void setLngBloodGroupID(long lngBloodGroupID) {
		this.lngBloodGroupID = lngBloodGroupID;
	}
	public String getStrMedicalHistory() {
		return strMedicalHistory;
	}
	public void setStrMedicalHistory(String strMedicalHistory) {
		this.strMedicalHistory = strMedicalHistory;
	}
	public long getLngGenderID() {
		return lngGenderID;
	}
	public void setLngGenderID(long lngGenderID) {
		this.lngGenderID = lngGenderID;
	}
	
	public long getLngOccupationID() {
		return lngOccupationID;
	}
	public void setLngOccupationID(long lngOccupationID) {
		this.lngOccupationID = lngOccupationID;
	}
	public String getStrEmailID() {
		return strEmailID;
	}
	public void setStrEmailID(String strEmailID) {
		this.strEmailID = strEmailID;
	}
	public String getStrMobileNumber() {
		return strMobileNumber;
	}
	public void setStrMobileNumber(String strMobileNumber) {
		this.strMobileNumber = strMobileNumber;
	}
	public String getStrLandlineNumber() {
		return strLandlineNumber;
	}
	public void setStrLandlineNumber(String strLandlineNumber) {
		this.strLandlineNumber = strLandlineNumber;
	}
	public String getStrAddress() {
		return strAddress;
	}
	public void setStrAddress(String strAddress) {
		this.strAddress = strAddress;
	}
	public long getLngAreaID() {
		return lngAreaID;
	}
	public void setLngAreaID(long lngAreaID) {
		this.lngAreaID = lngAreaID;
	}
	public long getLngCityID() {
		return lngCityID;
	}
	public void setLngCityID(long lngCityID) {
		this.lngCityID = lngCityID;
	}
	public long getLngStateID() {
		return lngStateID;
	}
	public void setLngStateID(long lngStateID) {
		this.lngStateID = lngStateID;
	}
	public long getLngCountryID() {
		return lngCountryID;
	}
	public void setLngCountryID(long lngCountryID) {
		this.lngCountryID = lngCountryID;
	}
	
	
	public Date getDtDOJ() {
		return dtDOJ;
	}
	public void setDtDOJ(Date dtDOJ) {
		this.dtDOJ = dtDOJ;
	}
	public Date getDtDOB() {
		return dtDOB;
	}
	public void setDtDOB(Date dtDOB) {
		this.dtDOB = dtDOB;
	}
	public String getStrICEName() {
		return strICEName;
	}
	public void setStrICEName(String strICEName) {
		this.strICEName = strICEName;
	}
	public String getStrICENumber() {
		return strICENumber;
	}
	public void setStrICENumber(String strICENumber) {
		this.strICENumber = strICENumber;
	}
	public long getLngICERelationshipID() {
		return lngICERelationshipID;
	}
	public void setLngICERelationshipID(long lngICERelationshipID) {
		this.lngICERelationshipID = lngICERelationshipID;
	}
	public String getStrUserName() {
		return strUserName;
	}
	public void setStrUserName(String strUserName) {
		this.strUserName = strUserName;
	}
	public String getStrPassword() {
		return strPassword;
	}
	public void setStrPassword(String strPassword) {
		this.strPassword = strPassword;
	}
	public long getLngSecurityQuestionID() {
		return lngSecurityQuestionID;
	}
	public void setLngSecurityQuestionID(long lngSecurityQuestionID) {
		this.lngSecurityQuestionID = lngSecurityQuestionID;
	}
	public String getStrSecurityAnswer() {
		return strSecurityAnswer;
	}
	public void setStrSecurityAnswer(String strSecurityAnswer) {
		this.strSecurityAnswer = strSecurityAnswer;
	}
	public long getLngEnquiryID() {
		return lngEnquiryID;
	}
	public void setLngEnquiryID(long lngEnquiryID) {
		this.lngEnquiryID = lngEnquiryID;
	}
	public int getIntStatus() {
		return intStatus;
	}
	public void setIntStatus(int intStatus) {
		this.intStatus = intStatus;
	}
	public long getLngCreatedBy() {
		return lngCreatedBy;
	}
	public void setLngCreatedBy(long lngCreatedBy) {
		this.lngCreatedBy = lngCreatedBy;
	}
	public Date getDtCreatedOn() {
		return dtCreatedOn;
	}
	public void setDtCreatedOn(Date dtCreatedOn) {
		this.dtCreatedOn = dtCreatedOn;
	}
	public long getLngUpdatedBy() {
		return lngUpdatedBy;
	}
	public void setLngUpdatedBy(long lngUpdatedBy) {
		this.lngUpdatedBy = lngUpdatedBy;
	}
	public Date getDtUpdatedOn() {
		return dtUpdatedOn;
	}
	public void setDtUpdatedOn(Date dtUpdatedOn) {
		this.dtUpdatedOn = dtUpdatedOn;
	}
	public double getDbAmountPayable() {
		return dbAmountPayable;
	}
	public void setDbAmountPayable(double dbAmountPayable) {
		this.dbAmountPayable = dbAmountPayable;
	}
	public double getDbAmountPaid() {
		return dbAmountPaid;
	}
	public void setDbAmountPaid(double dbAmountPaid) {
		this.dbAmountPaid = dbAmountPaid;
	}
	public double getDbActualAmount() {
		return dbActualAmount;
	}
	public void setDbActualAmount(double dbActualAmount) {
		this.dbActualAmount = dbActualAmount;
	}

	public int getIntMartialStatus() {
		return intMartialStatus;
	}

	public void setIntMartialStatus(int intMartialStatus) {
		this.intMartialStatus = intMartialStatus;
	}

	public int getIntSMSNotification() {
		return intSMSNotification;
	}

	public void setIntSMSNotification(int intSMSNotification) {
		this.intSMSNotification = intSMSNotification;
	}

	public int getIntEmailNotification() {
		return intEmailNotification;
	}

	public void setIntEmailNotification(int intEmailNotification) {
		this.intEmailNotification = intEmailNotification;
	}

	public long getLngBiometricID() {
		return lngBiometricID;
	}

	public void setLngBiometricID(long lngBiometricID) {
		this.lngBiometricID = lngBiometricID;
	}

	public long getLngFrequencyDays() {
		return lngFrequencyDays;
	}

	public void setLngFrequencyDays(long lngFrequencyDays) {
		this.lngFrequencyDays = lngFrequencyDays;
	}

	public Date getDtNextFeedbackDate() {
		return dtNextFeedbackDate;
	}

	public void setDtNextFeedbackDate(Date dtNextFeedbackDate) {
		this.dtNextFeedbackDate = dtNextFeedbackDate;
	}
	
	
	
	
}
