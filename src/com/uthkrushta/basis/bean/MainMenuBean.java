package com.uthkrushta.basis.bean;

public class MainMenuBean {
	
	private long lngID;
	private long lngMenuID;
	private long lngActivityID;
	
	private int intRoleID;
	private int intIsSecured;
	
	private String strMenu;
	private String strActivity;
	private String strPageURL;
	
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public long getLngMenuID() {
		return lngMenuID;
	}
	public void setLngMenuID(long lngMenuID) {
		this.lngMenuID = lngMenuID;
	}
	public long getLngActivityID() {
		return lngActivityID;
	}
	public void setLngActivityID(long lngActivityID) {
		this.lngActivityID = lngActivityID;
	}
	public int getIntRoleID() {
		return intRoleID;
	}
	public void setIntRoleID(int intRoleID) {
		this.intRoleID = intRoleID;
	}
	public String getStrMenu() {
		return strMenu;
	}
	public void setStrMenu(String strMenu) {
		this.strMenu = strMenu;
	}
	public String getStrActivity() {
		return strActivity;
	}
	public void setStrActivity(String strActivity) {
		this.strActivity = strActivity;
	}
	public String getStrPageURL() {
		return strPageURL;
	}
	public void setStrPageURL(String strPageURL) {
		this.strPageURL = strPageURL;
	}
	
	@Override
	public String toString() {
		return "MainMenuBean [lngID=" + lngID + ", lngMenuID=" + lngMenuID
				+ ", lngActivityID=" + lngActivityID + ", intRoleID="
				+ intRoleID + ", strMenu=" + strMenu + ", strActivity="
				+ strActivity + ", strPageURL=" + strPageURL + "]";
	}
	public int getIntIsSecured() {
		return intIsSecured;
	}
	public void setIntIsSecured(int intIsSecured) {
		this.intIsSecured = intIsSecured;
	}
	
	
	
	
	
}
