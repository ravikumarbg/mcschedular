package com.uthkrushta.mm.transaction.bean;

import java.util.Date;

public class MemberPlanSubsFromToTransactionBean {

	private long lngID;
	private long lngMemID;
	private long lngPlanID;
	private long lngSubsID;
	private Date dtFromDate;
	private Date dtToDate;
	private double dblTotalAmt;
	private double dblPaidAmt;
	private double dblBalanceAmt;
	private int intIsCurrent;
	private int intStatus;
	private int intSMSCount;
	private Date dtlastSMSDate;
	
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public long getLngMemID() {
		return lngMemID;
	}
	public void setLngMemID(long lngMemID) {
		this.lngMemID = lngMemID;
	}
	public long getLngPlanID() {
		return lngPlanID;
	}
	public void setLngPlanID(long lngPlanID) {
		this.lngPlanID = lngPlanID;
	}
	public long getLngSubsID() {
		return lngSubsID;
	}
	public void setLngSubsID(long lngSubsID) {
		this.lngSubsID = lngSubsID;
	}
	public Date getDtFromDate() {
		return dtFromDate;
	}
	public void setDtFromDate(Date dtFromDate) {
		this.dtFromDate = dtFromDate;
	}
	public Date getDtToDate() {
		return dtToDate;
	}
	public void setDtToDate(Date dtToDate) {
		this.dtToDate = dtToDate;
	}
	public int getIntStatus() {
		return intStatus;
	}
	public void setIntStatus(int intStatus) {
		this.intStatus = intStatus;
	}
	public double getDblTotalAmt() {
		return dblTotalAmt;
	}
	public void setDblTotalAmt(double dblTotalAmt) {
		this.dblTotalAmt = dblTotalAmt;
	}
	public double getDblPaidAmt() {
		return dblPaidAmt;
	}
	public void setDblPaidAmt(double dblPaidAmt) {
		this.dblPaidAmt = dblPaidAmt;
	}
	public double getDblBalanceAmt() {
		return dblBalanceAmt;
	}
	public void setDblBalanceAmt(double dblBalanceAmt) {
		this.dblBalanceAmt = dblBalanceAmt;
	}
	public int getIntIsCurrent() {
		return intIsCurrent;
	}
	public void setIntIsCurrent(int intIsCurrent) {
		this.intIsCurrent = intIsCurrent;
	}
	
	public Date getDtlastSMSDate() {
		return dtlastSMSDate;
	}
	public void setDtlastSMSDate(Date dtlastSMSDate) {
		this.dtlastSMSDate = dtlastSMSDate;
	}
	public int getIntSMSCount() {
		return intSMSCount;
	}
	public void setIntSMSCount(int intSMSCount) {
		this.intSMSCount = intSMSCount;
	}
	
	
	
}
