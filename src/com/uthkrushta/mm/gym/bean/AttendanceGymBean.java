package com.uthkrushta.mm.gym.bean;

import java.util.Date;

public class AttendanceGymBean {
	long lngID;
	String strMemberCode;
	String strName;
	long lngMemberID;
	Date dtLoginDate;
	Date dtLogoutDate;
	int intStatus;
	int intLoggTypeID;
	int intIsLogged;
	long lngBatchID;
	long lngFinancialYearID;
	long lngCompanyID;
	int intIsDiffComp;
	//Added newly
	int intIsAutoLogout;
	
	
	 
	public int getIntIsAutoLogout() {
		return intIsAutoLogout;
	}
	public void setIntIsAutoLogout(int intIsAutoLogout) {
		this.intIsAutoLogout = intIsAutoLogout;
	}
	public int getIntIsDiffComp() {
		return intIsDiffComp;
	}
	public void setIntIsDiffComp(int intIsDiffComp) {
		this.intIsDiffComp = intIsDiffComp;
	}
	public long getLngCompanyID() {
		return lngCompanyID;
	}
	public void setLngCompanyID(long lngCompanyID) {
		this.lngCompanyID = lngCompanyID;
	}
	public long getLngFinancialYearID() {
		return lngFinancialYearID;
	}
	public void setLngFinancialYearID(long lngFinancialYearID) {
		this.lngFinancialYearID = lngFinancialYearID;
	}
	public long getLngBatchID() {
		return lngBatchID;
	}
	public void setLngBatchID(long lngBatchID) {
		this.lngBatchID = lngBatchID;
	}
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public String getStrName() {
		return strName;
	}
	public void setStrName(String strName) {
		this.strName = strName;
	}
	public long getLngMemberID() {
		return lngMemberID;
	}
	public void setLngMemberID(long lngMemberID) {
		this.lngMemberID = lngMemberID;
	}
	public String getStrMemberCode() {
		return strMemberCode;
	}
	public void setStrMemberCode(String strMemberCode) {
		this.strMemberCode = strMemberCode;
	}
	public Date getDtLoginDate() {
		return dtLoginDate;
	}
	public void setDtLoginDate(Date dtLoginDate) {
		this.dtLoginDate = dtLoginDate;
	}
	public Date getDtLogoutDate() {
		return dtLogoutDate;
	}
	public void setDtLogoutDate(Date dtLogoutDate) {
		this.dtLogoutDate = dtLogoutDate;
	}
	public int getIntStatus() {
		return intStatus;
	}
	public void setIntStatus(int intStatus) {
		this.intStatus = intStatus;
	}
	public int getIntLoggTypeID() {
		return intLoggTypeID;
	}
	public void setIntLoggTypeID(int intLoggTypeID) {
		this.intLoggTypeID = intLoggTypeID;
	}
	public int getIntIsLogged() {
		return intIsLogged;
	}
	public void setIntIsLogged(int intIsLogged) {
		this.intIsLogged = intIsLogged;
	}

	
}
