package com.uthkrushta.mm.utility.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ser.BeanPropertyWriter;

import com.ibm.icu.text.DateFormat;
import com.ibm.icu.text.SimpleDateFormat;
import com.ibm.icu.util.Calendar;
import com.uthkrushta.basis.constants.ILabelConstants;
import com.uthkrushta.basis.constants.IUMCConstants;
import com.uthkrushta.mm.configuration.bean.GlobalSettingBean;
import com.uthkrushta.mm.configuration.bean.RewardPointsConfigurationBean;
import com.uthkrushta.mm.configuration.bean.TimeZoneConfigurationBean;
import com.uthkrushta.mm.frameWork.bean.DateRangeBean;
import com.uthkrushta.mm.gym.bean.AttendanceGymBean;
import com.uthkrushta.mm.master.bean.BiometricUserBean;
import com.uthkrushta.mm.master.bean.DeviceControllerBean;
import com.uthkrushta.mm.master.bean.FinancialYearMasterBean;
import com.uthkrushta.mm.master.bean.MemberMasterBean;
import com.uthkrushta.mm.supreme.control.EntityPlanMasterView;
import com.uthkrushta.mm.supreme.control.GlobalUrlSettingBean;
import com.uthkrushta.mm.supreme.control.TriggerSuperConfigurationBean;
import com.uthkrushta.mm.transaction.bean.LoggTransactionBean;
import com.uthkrushta.mm.transaction.bean.MemberPauseTransactionBean;
import com.uthkrushta.mm.transaction.bean.PlanSubsPaidMemNameTransactionViewBean;
import com.uthkrushta.utils.DBUtil;
import com.uthkrushta.utils.DBUtilStatic;
import com.uthkrushta.utils.UMBException;
import com.uthkrushta.utils.WebUtil;

public class SchedularController {
	GlobalSettingBean objGSB =null;
	public static String strTimeGMT="";
	
	
	
	
	
	//static String strUrl = "http://localhost:8090/device-controller/biometric-int";
	ObjectMapper mapper;
	
	
	
	
	public static boolean delegateCall(EntityPlanMasterView objEPMV){
		boolean blnExecuted = false;
		boolean blnSendExpiryReminder=false;
		boolean blnSendAbsenceMembersList=false;
		
		
		
		/*//Get the end Day of the week 
		Date dtCurrent = WebUtil.getCurrentDate(request);
		Calendar c = Calendar.getInstance();
		c.setTime(dtCurrent);
		int dayOfWeek = c.get(Calendar.DAY_OF_WEEK) - c.getFirstDayOfWeek();
		c.add(Calendar.DAY_OF_MONTH, -dayOfWeek);
		c.add(Calendar.DAY_OF_MONTH, 7); 
		Date weekEnd = c.getTime();
		//Ends
*/		
		
		
		
		boolean blnActivateDeactivateMembers = activateDeactivateMembers(objEPMV);
		// To activate And Deactivate Extended Members and their biometric information
				try {
					blnExecuted=activateAndDeactivateExtendedMembers(objEPMV);
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
		
		
		//checking whether center has enabled email
		
		
		
		
		//Here goes the code for Birthday/aniversary and weekly attendance rewardpoint update
	   boolean blnAttendaceReward=attendanceRewards(objEPMV);
	    boolean blnBAReward=birthdayAniversaryRewards(objEPMV);
	//Ends the  logic for Birthday/aniversary and weekly attendance;
	
	
	
	 
	//Send SMS is center has enabled SMS notification
	if(!isSMSEnabledForCenter(objEPMV)) return true;
		
		
		
		
		//isAutoLogout is center has enabled SMS notification
		if(!isAutoLogoutEnabledForCenter(objEPMV)) return true;
		
		blnExecuted =autoLogout(objEPMV);
		
		return blnExecuted;
	}
	
	

	  //This code related to reward points for birthday and aniversary
		private static boolean birthdayAniversaryRewards(EntityPlanMasterView objEPMV) {
			// TODO Auto-generated method stub
			String strTableName = IUMCConstants.BN_GLOBAL_SETTING;
			String strSelectList = "";
			String  strWhereClause = IUMCConstants.GET_ACTIVE_ROWS;
			String strSortClause = "strName, strLastName";
			boolean blnUpdatedReward=false;
			
			GlobalSettingBean objGSB = (GlobalSettingBean)	DBUtilStatic.getRecordFromCenter(objEPMV, strTableName, strSelectList, strWhereClause, "");
			
			if( null!=objGSB && objGSB.getIntIsRewards()==1 )
			{
				String strStartDate="";
				String strEndDate="";
			   
				//To get the selected time zone of different countries
				     strTimeGMT =getTimeZone(objEPMV);
				//Getting  the time zone of different countries ends
				
				TriggerSuperConfigurationBean objTSCB=(TriggerSuperConfigurationBean) DBUtilStatic.getRecordFromSup(IUMCConstants.BN_TRIGGERSUPCONFIG);
				
				//For Week to trigger
				if(null!=objTSCB && objTSCB.getLngTriggerControl()==IUMCConstants.TG_INT_WEEK)
				{
				
					
					//Get the end Day of the week 
					Date dtCurrent = WebUtil.getCurrentDate(strTimeGMT);
					Calendar c = Calendar.getInstance();
					c.setTime(dtCurrent);
					int dayOfWeek = c.get(Calendar.DAY_OF_WEEK) - c.getFirstDayOfWeek();
					c.add(Calendar.DAY_OF_MONTH, -dayOfWeek);
					Date dtWeekStart=c.getTime();
					c.add(Calendar.DAY_OF_MONTH, 6); 
					Date dtWeekEnd = c.getTime();
					//Ends
					
					SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
					strStartDate=sdf.format(dtWeekStart);
					strEndDate=sdf.format(dtWeekEnd);
					String strCurrentDate=sdf.format(WebUtil.getCurrentDate(strTimeGMT));
					
					try {
						dtWeekStart=sdf.parse(strStartDate);
						dtCurrent=sdf.parse(strCurrentDate);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					
					
					//Check whether the current date is weekstart date
					if(null!=dtWeekStart && dtWeekStart.compareTo(dtCurrent)==0)
					{
						blnUpdatedReward=addbirthDayAniversaryRewards(objEPMV,strStartDate,strEndDate);
					}
				} //For Month to Trigger
				else if(null!=objTSCB && objTSCB.getLngTriggerControl()==IUMCConstants.TG_INT_MONTH)
				{
				  	DateRangeBean objDRB=WebUtil.getStartAndEndDate(strTimeGMT,IUMCConstants.DB_DATE_FORMAT);
				  	SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
				  	Date dtStartMonth;
				  	try
				  	{
				  	     dtStartMonth=sdf.parse(objDRB.getStrStartDate());//Check whether current date is start date of month
					  	if(null!=dtStartMonth && dtStartMonth.compareTo(sdf.parse(WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DATE_FORMAT)))==0)
						{
							blnUpdatedReward=addbirthDayAniversaryRewards(objEPMV,objDRB.getStrStartDate(),objDRB.getStrEndDate());
						}
				  	}
				  	catch (Exception e) {
						// TODO: handle exception
				  		e.printStackTrace();
					}
				     
				}
			}
			return blnUpdatedReward;
		}


	    //this code executes on call from the above function
		private static boolean addbirthDayAniversaryRewards(EntityPlanMasterView objEPMV, String strStartDate, String strEndDate) {
			// TODO Auto-generated method stub
			String strWhereBDay=IUMCConstants.GET_ACTIVE_ROWS+" AND DATE_FORMAT(dtDOB,'%m-%d') >= DATE_FORMAT('" + strStartDate+ "','%m-%d')  AND DATE_FORMAT(dtDOB,'%m-%d') <= DATE_FORMAT('" + strEndDate+ "','%m-%d')";
			String strWhereADay=IUMCConstants.GET_ACTIVE_ROWS+" AND DATE_FORMAT(dtDOM,'%m-%d') >= DATE_FORMAT('" + strStartDate+ "','%m-%d')  AND DATE_FORMAT(dtDOM,'%m-%d') <= DATE_FORMAT('" + strEndDate+ "','%m-%d')";
			boolean blnUpdatedReward=false;
			try {
				
			    List lstMemBday=DBUtilStatic.getRecordsFromCenter(objEPMV, IUMCConstants.BN_MEMBER_MASTER_BEAN, "", strWhereBDay, "");
			    List lstMemAday=DBUtilStatic.getRecordsFromCenter(objEPMV, IUMCConstants.BN_MEMBER_MASTER_BEAN, "", strWhereADay, "");
			    RewardPointsConfigurationBean objRPCB=(RewardPointsConfigurationBean)DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_REWARD_POINTS_CFG_BEAN, "",IUMCConstants.GET_ACTIVE_ROWS, "");
			    
			    //For birthday adding reward points
			    if(null!=lstMemBday && lstMemBday.size()>0)
			    { 
			    	
			    	for(int i=0;i<lstMemBday.size();i++)
			    	{
			    		MemberMasterBean objMMB=(MemberMasterBean)lstMemBday.get(i);
			    		if(null!=objMMB && null!=objEPMV && objEPMV.getIntEnableRewardSystem()==1 && isRewardEnabledForCenter(objEPMV) && null!=objRPCB && objRPCB.getIntBA()==1)
			    		{
			    		 	updateBDayAniversaryAttendRewardPoint(objMMB,objEPMV,objRPCB,IUMCConstants.LNG_BIRTHDAY);
			    		}
			    	}
			    }
			    
			  //For birthday adding reward points
			    if(null!=lstMemAday && lstMemAday.size()>0)
			    { 
			    	
			    	for(int i=0;i<lstMemAday.size();i++)
			    	{
			    		MemberMasterBean objMMB=(MemberMasterBean)lstMemAday.get(i);
			    		if(null!=objMMB && null!=objEPMV && objEPMV.getIntEnableRewardSystem()==1 && isRewardEnabledForCenter(objEPMV) && null!=objRPCB && objRPCB.getIntBA()==1)
			    		{
			    			updateBDayAniversaryAttendRewardPoint(objMMB,objEPMV,objRPCB,IUMCConstants.LNG_ANIVERSARY);
			    		}
			    	}
			    }
			    DBUtilStatic.updateRecordsOfCenter(objEPMV, IUMCConstants.BN_GLOBAL_SETTING, "dtLastBATrigger = '"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_FORMAT)+"'", IUMCConstants.GET_ACTIVE_ROWS);
			 } catch (UMBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			} 
			return blnUpdatedReward;
		}

		
		public static void updateBDayAniversaryAttendRewardPoint(MemberMasterBean objMMB, EntityPlanMasterView objEPMV, RewardPointsConfigurationBean objRPCB,long lngReasonType){
			boolean blnUpdatedReward=false;
				try {
					blnUpdatedReward=DBUtilStatic.updateRecordsOfCenter(objEPMV, IUMCConstants.BN_MEMBER_MASTER_BEAN, "dblRewardPoints =Round(dblRewardPoints +"+objRPCB.getDblAnivBDayPoints()+",2)",IUMCConstants.GET_ACTIVE_ROWS+" and lngID = "+objMMB.getLngID());
				
				//Get the Active financial year
				FinancialYearMasterBean objFYMB =(FinancialYearMasterBean)DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_FINANCIAL_YEAR_MASTER_BEAN, "", IUMCConstants.GET_ACTIVE_ROWS+" and intIsActive = 1", "");
				 if(null==objFYMB)
				 {
					 objFYMB=new FinancialYearMasterBean();
				 }
				//Here we create a logg for adding the points
		    	LoggTransactionBean objLTB=new LoggTransactionBean();
		    	objLTB.setLngLoggReasonID(lngReasonType);
		    	objLTB.setLngRedemptionType(IUMCConstants.LNG_ADD_POINTS);
		    	objLTB.setLngMemID(objMMB.getLngID());
		    	objLTB.setDblPoints(objRPCB.getDblReferalPoints());
		    	//objLTB.setLngCreatedBy(1);
		    	//objLTB.setLngUpdatedBy(1);
		    	objLTB.setDtCreatedOn(WebUtil.getCurrentDate(strTimeGMT));
		    	objLTB.setDtUpdatedOn(WebUtil.getCurrentDate(strTimeGMT));
		    	objLTB.setLngFinancialYearID(objFYMB.getLngID());
		    	objLTB.setLngCompanyID(objMMB.getLngCompanyID());
		    	objLTB.setLngFormsRefID(1);
		    	objLTB.setIntStatus(IUMCConstants.STATUS_ACTIVE);
		    	boolean blnModified = DBUtilStatic.modifyRecord(objEPMV,objLTB);
			} 
	    	catch (UMBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
		}
		
		private static boolean isRewardEnabledForCenter(EntityPlanMasterView objEPMV) {
			String strTableName = IUMCConstants.BN_GLOBAL_SETTING;
			String strSelectList = "";
			String  strWhereClause = IUMCConstants.GET_ACTIVE_ROWS;
			String strSortClause = "strName, strLastName";
			
			GlobalSettingBean objGSB = (GlobalSettingBean)	DBUtilStatic.getRecordFromCenter(objEPMV, strTableName, strSelectList, strWhereClause, "");
			
			if( null!=objGSB && objGSB.getIntIsRewards()==1 )
			{
				return true;
			}
			else
			{
			return false;
			}
		}

	///Attendance related  rewardpoints
		private static boolean attendanceRewards(EntityPlanMasterView objEPMV) {
			// TODO Auto-generated method stub
			
			
			String strTableName = IUMCConstants.BN_GLOBAL_SETTING;
			String strSelectList = "";
			String  strWhereClause = IUMCConstants.GET_ACTIVE_ROWS;
			String strSortClause = "strName, strLastName";
			boolean blnUpdatedReward=false;
			
			GlobalSettingBean objGSB = (GlobalSettingBean)	DBUtilStatic.getRecordFromCenter(objEPMV, strTableName, strSelectList, strWhereClause, "");
			
			if( null!=objGSB && objGSB.getIntIsRewards()==1 )
			{
				//To get the selected time zone of different countries
				   strTimeGMT=getTimeZone(objEPMV);
				//Getting  the time zone of different countries ends
				
				
				
				//Get the end Day of the week 
				Date dtCurrent = WebUtil.getCurrentDate(strTimeGMT);
				Calendar c = Calendar.getInstance();
				c.setTime(dtCurrent);
				int dayOfWeek = c.get(Calendar.DAY_OF_WEEK) - c.getFirstDayOfWeek();
				c.add(Calendar.DAY_OF_MONTH, -dayOfWeek);
				c.add(Calendar.DAY_OF_MONTH, 6); 
				Date weekEnd = c.getTime();
				SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
				String strWeekEnd= sdf.format(weekEnd);
				String strCurrentDate=sdf.format(dtCurrent);
				try {
					weekEnd=sdf.parse(strWeekEnd);
					dtCurrent=sdf.parse(strCurrentDate);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//Ends
				
				 if(weekEnd.compareTo(dtCurrent)==0)
				 {
					blnUpdatedReward=addAttendanceRewardPoints(objEPMV,objGSB);
				 }
				 
				//Check the last attendance trigger date
				/*if(last attendance trigger date==null)
				{
					//trigger now
					
				}else
				{
				   get the last triggered date and add 7 days to it
				   if(the 7 day is current day then trigger)
				   {
				       trigger now
				   }
				   else
				   {
				   return false;
				}*/
				 
				 return blnUpdatedReward;
			}
			else
			{
			return blnUpdatedReward;
			}
			
			
			
			
		}
	 //this code executes on call from the above code
	private static boolean addAttendanceRewardPoints(EntityPlanMasterView objEPMV,GlobalSettingBean objGSB) {
			// TODO Auto-generated method stub
		boolean blnUpdatedReward=false;
		try {
			/*DBUtilStatic.updateRecordsOfCenter(objEPMV, IUMCConstants.BN_GLOBAL_SETTING, "dtLastAttendanceTrigger = "+WebUtil.getCurrentDate(request), IUMCConstants.GET_ACTIVE_ROWS);*/
		     
			
		List lstCenterMembers=DBUtilStatic.getRecordsFromCenter(objEPMV, IUMCConstants.BN_MEMBER_MASTER_BEAN, "", IUMCConstants.GET_ACTIVE_ROWS, "");
		   if(null!=lstCenterMembers && lstCenterMembers.size()>0)
		   {
			   RewardPointsConfigurationBean objRPCB=(RewardPointsConfigurationBean)DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_REWARD_POINTS_CFG_BEAN, "",IUMCConstants.GET_ACTIVE_ROWS, "");
			  
			   for(int i=0;i<lstCenterMembers.size();i++)
			   {
				   MemberMasterBean objMMB=(MemberMasterBean)lstCenterMembers.get(i);
				   if(null!=objMMB)
				   {
					   List lstPresentDays=null;   
				   
					   if(null!=objGSB && objGSB.getDtLastAttendanceTrigger()==null)
					   {
					        lstPresentDays = DBUtilStatic.getRecordsFromCenter(objEPMV, IUMCConstants.BN_ATTENDANCE_GYM_BEAN, "", IUMCConstants.GET_ACTIVE_ROWS+" and intLoggTypeID =1  and lngMemberID = "+objMMB.getLngID()+" group by date(dtLoginDate)", "");
					   }
					   else if(null!=objGSB && objGSB.getDtLastAttendanceTrigger()!=null)
					   {
						   lstPresentDays = DBUtilStatic.getRecordsFromCenter(objEPMV, IUMCConstants.BN_ATTENDANCE_GYM_BEAN, "", IUMCConstants.GET_ACTIVE_ROWS+" and intLoggTypeID =1 and lngMemberID = "+objMMB.getLngID()+" and dtLoginDate > "+WebUtil.formatDate(objGSB.getDtLastAttendanceTrigger(), IUMCConstants.DB_DATE_FORMAT) +" group by date(dtLoginDate)", "");  
					   }
					   
				    
				       if(null!=objRPCB && objRPCB.getIntAttendance()==1 && null!=lstPresentDays && lstPresentDays.size()>0 &&  objRPCB.getLngPresentDays()>=lstPresentDays.size() && null!=objEPMV && objEPMV.getIntEnableRewardSystem()==1 && isRewardEnabledForCenter(objEPMV) )
				       {                
				    	   updateBDayAniversaryAttendRewardPoint(objMMB,objEPMV,objRPCB,IUMCConstants.LNG_ATTENDANCE);
				       }
				    
				   }
			   }
		   }
		   DBUtilStatic.updateRecordsOfCenter(objEPMV, IUMCConstants.BN_GLOBAL_SETTING, "dtLastAttendanceTrigger = '"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_FORMAT)+"'", IUMCConstants.GET_ACTIVE_ROWS); 
		} catch (UMBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
			return blnUpdatedReward;
		}



	/*trigger logic
	 * 
	 * Get list of all member of the current center
	 * 
	 * lstBirthDayMemberList = DBUtilStatic.getRecordsFromCenter(objEPMV, strTableName, strSelectList, strBDWhereClause, strSortClause);
	 * loop over each member 
	 * get the no of days present (last week till yesterday)
	 * if(the no of days is greater than the centers specified day)
	 * {
	 *   for that member profile add points of that center
	 * }
	 * 
	 * */	

	   //Checking whether the client has enabled the SMS in Global Setting
		private static boolean isSMSEnabledForCenter(EntityPlanMasterView objEPMV) {
			String strTableName = IUMCConstants.BN_GLOBAL_SETTING;
			String strSelectList = "";
			String  strWhereClause = IUMCConstants.GET_ACTIVE_ROWS;
			String strSortClause = "strName, strLastName";
			
			GlobalSettingBean objGSB = (GlobalSettingBean)	DBUtilStatic.getRecordFromCenter(objEPMV, strTableName, strSelectList, strWhereClause, "");
			
			if( null!=objGSB && objGSB.getIntIsSMS()==1 )
			{
				//To get the selected time zone of different countries
				 strTimeGMT=getTimeZone(objEPMV);
				//Getting  the time zone of different countries ends
				
				return true;
			}
			else
			{
			return false;
			}
		}

	
	
	private static boolean isAutoLogoutEnabledForCenter(EntityPlanMasterView objEPMV) {
		String strTableName = IUMCConstants.BN_GLOBAL_SETTING;
		String strSelectList = "";
		String  strWhereClause = IUMCConstants.GET_ACTIVE_ROWS;
		String strSortClause = "strName, strLastName";
		
		GlobalSettingBean objGSB = (GlobalSettingBean)	DBUtilStatic.getRecordFromCenter(objEPMV, strTableName, strSelectList, strWhereClause, "");
		
		if( null!=objGSB && objGSB.getIntAutoLogout()==1 )
		{
			return true;
		}
		else
		{
		return false;
		}
	}
	
	private static boolean autoLogout(EntityPlanMasterView objEPMV)
	{
		//List all members with logindatetime-3 and is_logged=1 and logg_type=1 
          List lstAttendanceMembers=null;
          boolean blnUpdated=false;
          
          String strTableName = IUMCConstants.BN_GLOBAL_SETTING;
  		String strSelectList = "";
  		String  strWhereClause = IUMCConstants.GET_ACTIVE_ROWS;
  		String strSortClause = "strName, strLastName";
  		
  		GlobalSettingBean objGSB = (GlobalSettingBean)	DBUtilStatic.getRecordFromCenter(objEPMV, strTableName, strSelectList, strWhereClause, "");
  		int hour=0;
          
          if(null!=objGSB)
          {
        	  hour=objGSB.getIntAutoLogoutTime(); 
          }
          
          //Getting the time zone of the country
          strTimeGMT=getTimeZone(objEPMV);
          //Getting time zone ends
          
        //Subtracting the time
          Calendar cal = Calendar.getInstance();
          cal.setTime(WebUtil.getCurrentDate(strTimeGMT));
          cal.add(Calendar.HOUR, -hour);
          Date dtThreeHourBack = cal.getTime();
          SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd"); 
         /* SimpleDateFormat sdfTime=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); */
          String strCurrDate=sdf.format(WebUtil.getCurrentDate(strTimeGMT));
          
          /*String strThreeHourBack=sdfTime.format(dtThreeHourBack);*/
          Date dtCurrDate=null;
          
          
          String strBDWhereClause=IUMCConstants.GET_ACTIVE_ROWS+" and intLoggTypeID = "+IUMCConstants.INT_LOGGED_MEMBER+" and intIsLogged = "+IUMCConstants.INT_CURRENTLY_LOGGED_IN+" and dtLoginDate <= '"+WebUtil.formatDate(dtThreeHourBack, IUMCConstants.DB_DATE_TIME_SEC_FORMAT)+"' " ;//and dtLoginDate like '"+strCurrDate+"%'
          lstAttendanceMembers=DBUtilStatic.getRecordsFromCenter(objEPMV, IUMCConstants.BN_ATTENDANCE_GYM_BEAN, "", strBDWhereClause, "");
          
          
          
          if(null!=lstAttendanceMembers)
		 {
        	  String strAttendanceID="";
			 for(int i=0;i<lstAttendanceMembers.size();i++)
			 {
				 AttendanceGymBean objAGB=new AttendanceGymBean();
				 objAGB=(AttendanceGymBean) lstAttendanceMembers.get(i);
				 if(null!=objAGB)
				 {
					 strAttendanceID=objAGB.getLngID()+",";
				 }
			 }
			 if(null!=strAttendanceID && strAttendanceID.trim().length()!=0)
			 {
				 strAttendanceID=strAttendanceID.substring(0, strAttendanceID.length()-1);
			 }
			 String strUpdateClause="dtLogoutDate = '"+WebUtil.formatDate(WebUtil.getCurrentDate(strTimeGMT), IUMCConstants.DB_DATE_TIME_SEC_FORMAT)+"', intIsLogged = 0 , intIsAutoLogout = 1";
			 String strWhereClauseAttendance="lngID in ("+strAttendanceID+") and intLoggTypeID = "+IUMCConstants.INT_LOGGED_MEMBER;
			 try {
				 blnUpdated=DBUtilStatic.updateRecordsOfCenter(objEPMV, IUMCConstants.BN_ATTENDANCE_GYM_BEAN, strUpdateClause, strWhereClauseAttendance);
			} catch (UMBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 }
		
		
		return blnUpdated;
		
	}
	
	
	
	public static String getTimeZone(EntityPlanMasterView objEPMV)
	{
	GlobalSettingBean objGSB = (GlobalSettingBean)	DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_GLOBAL_SETTING, "", IUMCConstants.GET_ACTIVE_ROWS, "");
			
			if( null!=objGSB )
			{
				//To get the selected time zone of different countries
				 List lstTimeZone=DBUtilStatic.getRecordsFromCenter(objEPMV, IUMCConstants.BN_TIME_ZONE, "", IUMCConstants.GET_ACTIVE_ROWS, "");
		         
		         
		         if(null!=lstTimeZone)
		         {
		        	 
		        	 for(int i=0;i<lstTimeZone.size();i++)
		        	 {
		        	    TimeZoneConfigurationBean objTZCB=(TimeZoneConfigurationBean)lstTimeZone.get(i);
		        	    if(null!=objTZCB && objTZCB.getLngID()==objGSB.getLngTimeZone())
		        	    {
		        	    	if(null!=objTZCB.getStrTimeZone())
		        	    	{
		        	    		strTimeGMT=objTZCB.getStrTimeZone();
		        	    	}
		        	    }
		        	 }
		         }
				//Getting  the time zone of different countries ends
			}
			
			return strTimeGMT;
	}
	
	
	
	
	
	
	
	// @SuppressWarnings("null")
	private static boolean activateAndDeactivateExtendedMembers(EntityPlanMasterView objEPMV) throws MalformedURLException, ProtocolException {
		// TODO Auto-generated method stub
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);    
        String strToDate=dateFormat.format(cal.getTime());
		boolean blnActivate=false;
		
        //Getting the time zone of the country
		
		strTimeGMT=getTimeZone(objEPMV);
		//Getting the time zone of the country ends
		
		
        String strCurrentDate=dateFormat.format(WebUtil.getCurrentDate(strTimeGMT));
		
		String strTableName=IUMCConstants.BN_MEMBER_PAUSE_TRANSACTION_BEAN;
		String strMemPauseWhereClause=IUMCConstants.GET_ACTIVE_ROWS+" and dtTo = '"+strToDate+"'";
		String strSortClause="";
		String strMemDeactiveWhereClause=IUMCConstants.GET_ACTIVE_ROWS+" and intLatestRecord = 1 and dtFrom = '"+strCurrentDate+"'";
		
		//Getting list of members who are about to freeze
		String strFreezeDeactivate=IUMCConstants.GET_ACTIVE_ROWS+" and intLatestRecord = 1 and lngPauseTypeID =1 and dtMemberShipExtension = '"+strCurrentDate+"'";
		List lstMemberFreezeDeactivate=DBUtilStatic.getRecordsFromCenter(objEPMV, strTableName, "", strFreezeDeactivate, "");
		
	
		
		//List of Members who are going to deactivate today based on extension
		List lstMemberDeactivate=DBUtilStatic.getRecordsFromCenter(objEPMV, strTableName, "", strMemDeactiveWhereClause, strSortClause);
		
		
		
		
		//Get the List of members where current date-1=to_date; Here the extension of the members ends and he will be activated
		List lstMemberExtensionEnds=DBUtilStatic.getRecordsFromCenter(objEPMV, strTableName, "", strMemPauseWhereClause, strSortClause);
		
		
		
		if(lstMemberExtensionEnds!=null)
		{ 
			DeviceControllerBean objDCB = new DeviceControllerBean();
			//for each member update the member status to active
			for(int i=0;i<lstMemberExtensionEnds.size();i++)
			{   
				boolean blnMemberModified = false;
				MemberPauseTransactionBean objMPauseTransactionBean= new MemberPauseTransactionBean();
				objMPauseTransactionBean=(MemberPauseTransactionBean)lstMemberExtensionEnds.get(i);
				
				if(null!=objMPauseTransactionBean)
				{
					String strMemUpdateClause="lngMemberStatus = 1,intIsPaused = 0,lngPauseTypeID = 0";
					String strMemWhereClause=IUMCConstants.GET_ACTIVE_ROWS+" and lngID = "+objMPauseTransactionBean.getLngMemeberID();
					
					String strPauseWhere=IUMCConstants.GET_ACTIVE_ROWS+" and lngID = "+objMPauseTransactionBean.getLngID();
					String strPauseUpdate="intIsPaused =0";
					
					try {
						blnMemberModified = DBUtilStatic.updateRecordsOfCenter(objEPMV, IUMCConstants.BN_MEMBER_MASTER_BEAN, strMemUpdateClause, strMemWhereClause);
						DBUtilStatic.updateRecordsOfCenter(objEPMV,IUMCConstants.BN_MEMBER_PAUSE_TRANSACTION_BEAN, strPauseUpdate, strPauseWhere);
						
						if(blnMemberModified) {

							objDCB = (DeviceControllerBean) createDeviceControllerBeanObject(objEPMV, objMPauseTransactionBean.getLngMemeberID(), IUMCConstants.BIOMETRIC_STATUS_ACTIVE);
							GlobalUrlSettingBean objGUST = (GlobalUrlSettingBean) DBUtilStatic.getRecordFromCenter(objEPMV, "GlobalUrlSettingBean", "", "", "");
							
							if(objGUST == null) {
								objGUST = new GlobalUrlSettingBean();
							}
							
							
							if(objDCB != null ) {
							updateDeviceCommand(objDCB, objEPMV);
							}
						}
						blnActivate=true;
					} catch (UMBException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						blnActivate=false;
					}
				}
			}
			
		}
		
		
		
		//For Deactivating member with extension
		if(lstMemberDeactivate!=null)
		{	
			DeviceControllerBean objDCB = new DeviceControllerBean();
			//for each member update the member status to de-active
			for(int i=0;i<lstMemberDeactivate.size();i++)
			{
				boolean blnMemberModified = false;
				MemberPauseTransactionBean objMPauseTransactionBean= new MemberPauseTransactionBean();
				objMPauseTransactionBean=(MemberPauseTransactionBean)lstMemberDeactivate.get(i);
				if(null!=objMPauseTransactionBean)
				{
					String strMemUpdateClause="lngMemberStatus = 2";
					String strMemWhereClause=IUMCConstants.GET_ACTIVE_ROWS+" and lngID = "+objMPauseTransactionBean.getLngMemeberID();
					try {
						blnMemberModified = DBUtilStatic.updateRecordsOfCenter(objEPMV, IUMCConstants.BN_MEMBER_MASTER_BEAN, strMemUpdateClause, strMemWhereClause);
						blnActivate=true;
						
						if(blnMemberModified) {
							
							
							
							objDCB = (DeviceControllerBean) createDeviceControllerBeanObject(objEPMV, objMPauseTransactionBean.getLngMemeberID(), IUMCConstants.BIOMETRIC_STATUS_INACTIVE);
							if(objDCB != null ) {
							updateDeviceCommand(objDCB, objEPMV);
							}
						}
						
					} catch (UMBException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						blnActivate=false;
					}
				}
			}
			
		}
		
		//For Deactivating member with freeze
		
		
				if(lstMemberFreezeDeactivate!=null)
				{
					//for each member update the member status to de-active
					for(int i=0;i<lstMemberFreezeDeactivate.size();i++)
					{
						MemberPauseTransactionBean objMPauseTransactionBean= new MemberPauseTransactionBean();
						objMPauseTransactionBean=(MemberPauseTransactionBean)lstMemberFreezeDeactivate.get(i);
						if(null!=objMPauseTransactionBean)
						{
							String strMemUpdateClause="lngMemberStatus = 2";
							String strMemWhereClause=IUMCConstants.GET_ACTIVE_ROWS+" and lngID = "+objMPauseTransactionBean.getLngMemeberID();
							try {
								DBUtilStatic.updateRecordsOfCenter(objEPMV, IUMCConstants.BN_MEMBER_MASTER_BEAN, strMemUpdateClause, strMemWhereClause);
								blnActivate=true;
							} catch (UMBException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								blnActivate=false;
							}
						}
					}
					
				}
		
				//For Deactivating member with MemberShip expiry
		List  lstMembershipEnds=DBUtilStatic.getRecordsFromCenter(objEPMV, IUMCConstants.BN_PLAN_SUBS_MEM_NAME, "", IUMCConstants.GET_ACTIVE_ROWS+" and intIsCurrent=1 and dtToDate >'"+strCurrentDate+"'", "");
		
		if(null!=lstMembershipEnds && lstMembershipEnds.size()>0){
		   String strMemIDs="";
			for(int i=0;i<lstMembershipEnds.size();i++){
				 PlanSubsPaidMemNameTransactionViewBean objPSMTV=(PlanSubsPaidMemNameTransactionViewBean)lstMembershipEnds.get(i);
				 if(null!=objPSMTV){
					 strMemIDs +=objPSMTV.getLngMemID()+";";
				 }
			}
			
			if(null!=strMemIDs && strMemIDs.trim().length()>0 && !strMemIDs.equalsIgnoreCase(";")){
				strMemIDs=strMemIDs.substring(0, strMemIDs.length()-1);
				try {
					DBUtilStatic.updateRecordsOfCenter(objEPMV, IUMCConstants.BN_MEMBER_MASTER_BEAN, "lngMemberStatus = 2", IUMCConstants.GET_ACTIVE_ROWS+" and lngID in ("+strMemIDs+")");
					blnActivate=true;
				} catch (UMBException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					blnActivate=false;
				}
			}
		}
		
		
		
		
		return blnActivate;
		
	}
	
	
public static boolean updateDeviceCommand(DeviceControllerBean objDCB,EntityPlanMasterView objEPMV ) {
		URL url;
		try {
			String strUrl = "";
			
			GlobalUrlSettingBean objGUST = new GlobalUrlSettingBean();
			objGUST = (GlobalUrlSettingBean)DBUtilStatic.getRecordFromSup(ILabelConstants.BN_CONGF_GLB_URL_SETTING, IUMCConstants.GET_ACTIVE_ROWS+" AND lngID = "+IUMCConstants.BIOMETRIC_URL_LNGID,"");

			if(objGUST == null) {
				objGUST = new GlobalUrlSettingBean();
				return false;
			}
			
			strUrl = objGUST.getStrName();
			
			url = new URL(strUrl);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json; utf-8");
			con.setRequestProperty("Accept", "application/json");
			con.setDoOutput(true);
			
			ObjectMapper mapper = new ObjectMapper();
			String jsonInputString = mapper.writeValueAsString(objDCB);
			
			try(OutputStream os = con.getOutputStream()) {
			    byte[] input = jsonInputString.getBytes("utf-8");
			    os.write(input, 0, input.length);           
			}
			
			try(BufferedReader br = new BufferedReader(
					  new InputStreamReader(con.getInputStream(), "utf-8"))) {
					    StringBuilder response = new StringBuilder();
					    String responseLine = null;
					    while ((responseLine = br.readLine()) != null) {
					        response.append(responseLine.trim());
					    } 
					    System.out.println(response.toString());
					}
			
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		
		return true;
	}
	
	
public static Object createDeviceControllerBeanObject(EntityPlanMasterView objEPMV, Long lngMemberID, int intAction) {
		MemberMasterBean objMMB = new MemberMasterBean();
		BiometricUserBean objBUB = new BiometricUserBean();
		DeviceControllerBean objDCB = new DeviceControllerBean();
		List<BiometricUserBean> lstBiometricIDs = new ArrayList<BiometricUserBean>();
		
		 objMMB = (MemberMasterBean) DBUtilStatic.getRecordFromCenter(objEPMV, IUMCConstants.BN_MEMBER_MASTER_BEAN, "", IUMCConstants.GET_ACTIVE_ROWS+" AND lngID = "+lngMemberID, "");
		 if(objMMB != null && objMMB.getLngBiometricID() > 0) {
			 objBUB.setLngBiometricID(objMMB.getLngBiometricID());
			 objDCB.setIntActionID(intAction);
			 objDCB.setLngCompanyID(objMMB.getLngCompanyID());
			 objDCB.setStrCenterCode(objEPMV.getStrGymCode());
			 
			 
			 lstBiometricIDs.add(objBUB);
			 objDCB.setLstBiometricIDs(lstBiometricIDs);
			 return objDCB;
		 }else {
			 return null;
		 }
	}
	
	
	public static boolean activateDeactivateMembers(EntityPlanMasterView objEPMV) {
		boolean blnUpdated = false;
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String strCurrentDate=dateFormat.format(WebUtil.getCurrentDate(strTimeGMT));
		
		Date dtCurrent = WebUtil.getCurrentDate(strTimeGMT);
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, -1);
		String strYesterdaytDate=dateFormat.format(c.getTime());
		System.out.println(" ---------------------- strYesterdaytDate ----------------- "+strYesterdaytDate);
		
		
		
		
		
		
		 
		
		
		List lstMembersToActivate = DBUtilStatic.getRecordsFromCenter(objEPMV, IUMCConstants.BN_PLAN_SUBS_MEM_NAME, "", IUMCConstants.GET_ACTIVE_ROWS+" and intIsCurrent=1 and dtFromDate ='"+strCurrentDate+"'", "");
		List lstMembersToDeactivate = DBUtilStatic.getRecordsFromCenter(objEPMV, IUMCConstants.BN_PLAN_SUBS_MEM_NAME, "", IUMCConstants.GET_ACTIVE_ROWS+" and intIsCurrent=1 and dtToDate ='"+strYesterdaytDate+"'", "");
		
		if(lstMembersToActivate != null && lstMembersToActivate.size()>0) {
			blnUpdated = activateMembers(objEPMV, lstMembersToActivate);
			
		}
		
		if(lstMembersToDeactivate != null && lstMembersToDeactivate.size()>0) {
			blnUpdated = deactivateMembers(objEPMV, lstMembersToDeactivate);
		}
		
				
		
		
		return blnUpdated;
	}
	
	
	public static boolean activateMembers(EntityPlanMasterView objEPMV, List lstMembersToActivate) {
		boolean blnMemberActivate = false;
		PlanSubsPaidMemNameTransactionViewBean objPSTVB = new PlanSubsPaidMemNameTransactionViewBean();
		
		for(int i = 1; i<=lstMembersToActivate.size(); i++) {
			objPSTVB = (PlanSubsPaidMemNameTransactionViewBean) lstMembersToActivate.get(i-1);
			
			String strMemUpdateClause="lngMemberStatus = 1";
			String strMemWhereClause=IUMCConstants.GET_ACTIVE_ROWS+" and lngID = "+objPSTVB.getLngMemID();
			
			try {
				blnMemberActivate =  DBUtilStatic.updateRecordsOfCenter(objEPMV, IUMCConstants.BN_MEMBER_MASTER_BEAN, strMemUpdateClause, strMemWhereClause);
				
				if(blnMemberActivate) {
					DeviceControllerBean objDCB = new DeviceControllerBean();
					objDCB = (DeviceControllerBean) createDeviceControllerBeanObject(objEPMV, objPSTVB.getLngMemID(), IUMCConstants.BIOMETRIC_STATUS_ACTIVE);
					if(objDCB != null ) {
					updateDeviceCommand(objDCB, objEPMV);
					}
				}
			
			} catch (UMBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		
		
		return blnMemberActivate;
	}
	
	public static boolean deactivateMembers(EntityPlanMasterView objEPMV, List lstMembersToDeactivate) {
		boolean blnMemberDeactivate = false;
		PlanSubsPaidMemNameTransactionViewBean objPSTVB = new PlanSubsPaidMemNameTransactionViewBean();
		
		for(int i = 1; i<=lstMembersToDeactivate.size(); i++) {
			objPSTVB = (PlanSubsPaidMemNameTransactionViewBean) lstMembersToDeactivate.get(i-1);
			
			String strMemUpdateClause="lngMemberStatus = 2";
			String strMemWhereClause=IUMCConstants.GET_ACTIVE_ROWS+" and lngID = "+objPSTVB.getLngMemID();
			
			try {
				blnMemberDeactivate =  DBUtilStatic.updateRecordsOfCenter(objEPMV, IUMCConstants.BN_MEMBER_MASTER_BEAN, strMemUpdateClause, strMemWhereClause);
				if(blnMemberDeactivate) {
					DeviceControllerBean objDCB = new DeviceControllerBean();
					objDCB = (DeviceControllerBean) createDeviceControllerBeanObject(objEPMV, objPSTVB.getLngMemID(), IUMCConstants.BIOMETRIC_STATUS_INACTIVE);
					if(objDCB != null ) {
					updateDeviceCommand(objDCB, objEPMV);
					}
				}
			} catch (UMBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		
		
		return blnMemberDeactivate;
	}
	
}
