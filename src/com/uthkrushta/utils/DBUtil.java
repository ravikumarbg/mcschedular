package com.uthkrushta.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import objectlocator.ObjectLocator;
import objectlocator.ObjectLocatorMatrixBean;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/*import com.crystaldecisions.reports.sdk.ReportClientDocument;
import com.crystaldecisions.sdk.occa.report.exportoptions.ReportExportFormat;
import com.crystaldecisions.sdk.occa.report.lib.ReportSDKException;*/
import com.uthkrushta.basis.bean.MessageBean;
import com.uthkrushta.basis.bean.NameValueBean;
import com.uthkrushta.basis.bean.PaginationBean;
import com.uthkrushta.basis.constants.IUMCConstants;
import com.uthkrushta.mm.frameWork.bean.OutputManagerBean;







public class DBUtil {

	/*@SuppressWarnings({ "rawtypes" })
	public static List getMenuList(long roleID) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		String SQL_QUERY = " select distinct m from RoleBasedMenuBean rbm, MenuBean m where m.lngID = rbm.lngMenuID and rbm.lngRoleID = "
				+ roleID;
		Query query = session.createQuery(SQL_QUERY);
		return query.list();
	}*/

	HttpServletRequest request = null;
	
	@SuppressWarnings("rawtypes")
	public List getActivityList(long roleID, long menuID) {
		Session session =  null;
		List lstReturn = null;
		try{
		session = new HibernateUtil().getSessionFactory(request).openSession();
		String SQL_QUERY = " select distinct a from RoleBasedMenuBean rbm, ActivityBean a where a.lngID = rbm.lngActivityID and rbm.lngRoleID = "
				+ roleID + " and rbm.lngMenuID = " + menuID;
		Query query = session.createQuery(SQL_QUERY);
		lstReturn = query.list();
		if (null != lstReturn && lstReturn.size() > 0) {
			return lstReturn;
		}
		}catch(HibernateException e){
			
		}finally{
			if(null != session){
				session.flush();
				session.close();
			}
			
		}
		return null;
	}

	public DBUtil(HttpServletRequest request) {
		
		super();
		this.request = request;
		
	}

	@SuppressWarnings("rawtypes")
	public  List getRecords(String strTableName, String strWhereClause, String strSortClause) {
		return getRecords( null,  strTableName,  strWhereClause,  strSortClause);
	}

	@SuppressWarnings("rawtypes")
	public  List getRecords(String strSelectList, String strTableName, String strWhereClause, String strSortClause) {
		Session session = null;
		List lstReturn = null;
		try{
			session = new HibernateUtil().getSessionFactory(request).openSession();
			String SQL_QUERY = null;
			if (null == strSelectList || strSelectList.equals("")) {
				SQL_QUERY = "";
			} else {
				SQL_QUERY = "select " + strSelectList;
			}
			SQL_QUERY = SQL_QUERY + " from " + strTableName;
			if (null != strWhereClause && !strWhereClause.equalsIgnoreCase("")) {
				SQL_QUERY = SQL_QUERY + "  where " + strWhereClause + " ";
			}
			if (null != strSortClause && !strSortClause.equalsIgnoreCase("")) {
				SQL_QUERY = SQL_QUERY + " order by " + strSortClause;
			}
			Query query = session.createQuery(SQL_QUERY);
              lstReturn = query.list();
			
			if (null != lstReturn && lstReturn.size() > 0) {
				return lstReturn;
			}
		}catch(HibernateException e){
			e.printStackTrace();
		}finally{
			if(null != session){
				session.flush();
				session.close();
			}
		}
		return null;
	}

	public  Object getRecord(String strTableName, String strWhereClause, String strSortClause) {
		return getRecord(null, strTableName, strWhereClause, strSortClause);
	}

	
	
	
	
	public  long getRecordsCount(String strTableName, String strWhereClause) {
		Session session = null;
		List lstReturn = null;
		try{
			session = new HibernateUtil().getSessionFactory(request).openSession();
			String SQL_QUERY = null;
			SQL_QUERY = "SELECT COUNT(*) FROM " + strTableName;
			if (null != strWhereClause && !strWhereClause.equalsIgnoreCase("")) {
				SQL_QUERY = SQL_QUERY + "  WHERE " + strWhereClause + " ";
			}
			Query query = session.createQuery(SQL_QUERY);
			
             lstReturn = query.list();
			
			if (null != lstReturn && lstReturn.size() > 0) {
				return ((Long)lstReturn.get(0)).longValue();
			}
		}catch(HibernateException e){
			
		}finally{
			if(null != session){
				session.flush();
				session.close();
			}
		}
		return 0;
	}
	
	
	public  boolean deleteRecordsFromTable(String strBean,String strWhereClause) throws UMBException{
		Session session = null;
		UMBException exUAVS = null;
		MessageBean msg = null;
		try {
			session = new HibernateUtil().getSessionFactory(request).openSession();
			Transaction tx = session.beginTransaction();
			Query objQuery  = session.createQuery("DELETE FROM " + strBean  + "  where  " + strWhereClause) ;
			objQuery.executeUpdate();
			tx.commit();
			session.flush();
		}catch (HibernateException ex) {
			msg = new MessageBean();
			msg.setStrMsgText(ex.getMessage());
			exUAVS =  new UMBException("Hibernate Exception", ex, msg);
			throw exUAVS;
		} finally {
			if (null != session) {
				session.flush();
				session.close();
			}
		}
		return true;
	}
	
	public  Object getRecord(String strSelectList, String strTableName, String strWhereClause, String strSortClause) {
		Session session = null;
		List lstReturn = null;
		try{
		session = new HibernateUtil().getSessionFactory(request).openSession();
		String SQL_QUERY = null;
		if (null == strSelectList || strSelectList.equals("")) {
			SQL_QUERY = "";
		} else {
			SQL_QUERY = "select " + strSelectList;
		}
		SQL_QUERY = SQL_QUERY + " from " + strTableName;
		if (null != strWhereClause && !strWhereClause.equalsIgnoreCase("")) {
			SQL_QUERY = SQL_QUERY + "  where " + strWhereClause + " ";
		}
		if (null != strSortClause && !strSortClause.equalsIgnoreCase("")) {
			SQL_QUERY = SQL_QUERY + " order by " + strSortClause;
		}
		Query query = session.createQuery(SQL_QUERY);
		
		
		
         lstReturn = query.list();
		
		if (null != lstReturn && lstReturn.size() > 0) {
			return lstReturn.get(0);
		}
		}catch(HibernateException e){
			e.printStackTrace();
		}finally{
			if(null != session){
				session.flush();
				session.close();
			}
		}
		return null;
	}

public List getRecords(String strTableName, String strWhereClause, String strSortClause,PaginationBean objPaginationBean) {
		
		return getRecords( null,  strTableName,  strWhereClause,  strSortClause,objPaginationBean);
	}
	@SuppressWarnings("rawtypes")
	public List getRecords(String strSelectList, String strTableName, String strWhereClause, String strSortClause , PaginationBean objPaginationBean) {
		Session session = null;
		List lstReturn = null;
		try{
			session = new HibernateUtil().getSessionFactory(request).openSession();
			String SQL_QUERY = null;
			if (null == strSelectList || strSelectList.equals("")) {
				SQL_QUERY = "";
			} else {
				SQL_QUERY = "select " + strSelectList;
			}
			SQL_QUERY = SQL_QUERY + " from " + strTableName;
			if (null != strWhereClause && !strWhereClause.equalsIgnoreCase("")) {
				SQL_QUERY = SQL_QUERY + "  where " + strWhereClause + " ";
			}
			if (null != strSortClause && !strSortClause.equalsIgnoreCase("")) {
				SQL_QUERY = SQL_QUERY + " order by " + strSortClause;
			}
			Query query = session.createQuery(SQL_QUERY);
			if(null != objPaginationBean){
				query.setFirstResult(objPaginationBean.getIntStartingCount());
				query.setMaxResults(objPaginationBean.getIntNumberOfRecords());
			}
           lstReturn = query.list();
			
			if (null != lstReturn && lstReturn.size() > 0) {
				return lstReturn;
			}
		}catch(HibernateException e){
			
		}finally{
			if(null != session){
				session.flush();
				session.close();
			}
		}
		
		return null;
	}
	
	@SuppressWarnings({ "rawtypes" })
	public Object getRecord(long lngID, String strClassName, int intOption) throws UMBException {
		String strPrefix;
		MessageBean msg = null;
		Object object = new Object();
		Session session = null;
		if (lngID == 0)
			return null;
		switch (intOption) {
		
		case IUMCConstants.OBJ_CODE:
			strPrefix = "com.uthkrushta.mm.code.bean.";
			break;
		case IUMCConstants.OBJ_COFIG:
			strPrefix = "com.uthkrushta.mm.configuration.bean.";
			break;
		case IUMCConstants.OBJ_MASTER:
			strPrefix = "com.uthkrushta.mm.master.bean.";
			break;
		case IUMCConstants.OBJ_TRANSACTION:
			strPrefix = "com.uthkrushta.mm.transaction.bean.";
			break;
		case IUMCConstants.OBJ_PRODUCT_VIEW:
			 strPrefix = "com.uthkrushta.mm.product.bean.";
			 break;		
		default:
			strPrefix = "";
		}
		try {
			Class myclass = Class.forName(strPrefix + strClassName);
			session = new HibernateUtil().getSessionFactory(request).openSession();
			session.beginTransaction();
			object = myclass.newInstance();
			if (intOption == IUMCConstants.CODE_BEAN) {
				session.load(object, (new Long(lngID)).intValue());
			} else {
				session.load(object, lngID);
			}
			
		} catch (ClassNotFoundException ex) {
			msg = new MessageBean();
			msg.setStrMsgType(IUMCConstants.ERROR);
			msg.setStrMsgText(ex.getMessage());
			throw new UMBException(msg.getStrMsgText(), ex, msg);
		} catch (InstantiationException ex) {
			msg = new MessageBean();
			msg.setStrMsgType(IUMCConstants.ERROR);
			msg.setStrMsgText(ex.getMessage());
			throw new UMBException(msg.getStrMsgText(), ex, msg);
		} catch (IllegalAccessException ex) {
			msg = new MessageBean();
			msg.setStrMsgType(IUMCConstants.ERROR);
			msg.setStrMsgText(ex.getMessage());
			throw new UMBException(msg.getStrMsgText(), ex, msg);
		}finally{
			if(null != session){
				session.flush();
				session.close();
			}
		}
		return object;
	}

	public boolean updateRecords(String strTableName, String strUpdateClause, String strWhereClause) throws UMBException {
		String SQL_QUERY = " update " + strTableName;
		Session session = null;
		MessageBean msg = null;
		try {
			session = new HibernateUtil().getSessionFactory(request).openSession();
			if (null != strUpdateClause && !strUpdateClause.equalsIgnoreCase("")) {
				SQL_QUERY = SQL_QUERY + "  set " + strUpdateClause + " ";
			}
			if (null != strWhereClause && !strWhereClause.equalsIgnoreCase("")) {
				SQL_QUERY = SQL_QUERY + "  where " + strWhereClause + " ";
			}
			Transaction tx = session.beginTransaction();
			Query strqu = session.createQuery(SQL_QUERY);
			strqu.executeUpdate();
			tx.commit();
			
		} catch (HibernateException ex) {
			msg = new MessageBean();
			msg.setStrMsgType(IUMCConstants.ERROR);
			msg.setStrMsgText(ex.getMessage());
			return false;
			//throw new UMBException(msg.getStrMsgText(), ex, msg);
		}finally{
		
			if(null != session){
				session.flush();
				session.close();
			}
		}
		return true;
	}

	// Modify record generic
	public boolean modifyRecord(Object objRecord) throws UMBException {
		Session session = null;
		UMBException exUAVS = null;
		MessageBean msg = null;
		try {
			session = new HibernateUtil().getSessionFactory(request).openSession();
			Transaction tx = null;
			tx = session.beginTransaction();
			session.saveOrUpdate(objRecord);
			tx.commit();
			
		} catch (HibernateException ex) {
			msg = new MessageBean();
			msg.setStrMsgText(ex.getMessage());
			exUAVS = new UMBException("Hibernate Exception", ex, msg);
			throw exUAVS;
		} finally {
			if (null != session) {
				session.flush();
				session.close();
			}
		}
		return true;
	}

	
	// Modify record with header and items
		public boolean modifyRecord(Object objRecord,int calledFrom) throws UMBException {
			Session session = null;
			UMBException exUAVS = null;
			MessageBean msg = null;
			try {
				session = new HibernateUtil().getSessionFactory(request).openSession();
				Transaction tx = null;
				tx = session.beginTransaction();
				session.saveOrUpdate(objRecord);
				/*switch (calledFrom){
					case IUBESPOKEConstants.MANAGE_PROJECT: 
						ProjectRepositoryBean objProjectRepositoryBean = (ProjectRepositoryBean) objRecord;
						Iterator<ProjectRepositoryItemBean> itItem = objProjectRepositoryBean.getStProjectRepositoryItems().iterator(); 
						for(;itItem.hasNext();){
							session.saveOrUpdate(itItem.next());
						}
				}*/
				tx.commit();
				
			} catch (HibernateException ex) {
				msg = new MessageBean();
				msg.setStrMsgText(ex.getMessage());
				exUAVS = new UMBException("Hibernate Exception", ex, msg);
				throw exUAVS;
			} finally {
				if (null != session) {
					session.flush();
					session.close();
				}
			}
			return true;
		}
	
	
	@SuppressWarnings( {"finally" })
	public boolean modifyRecords(List<Object> objList) throws UMBException {
		Session session = null;
		UMBException exUAVS = null;
		MessageBean msg = null;
		Transaction tx = null;
		try {
			Iterator<Object> it = objList.iterator();
			session = new HibernateUtil().getSessionFactory(request).openSession();
			
			tx = session.beginTransaction();
			for (int i = 1; it.hasNext();i++) {
				session.saveOrUpdate( it.next());
				if(i%IUMCConstants.COMMIT_SIZE == 0){
					tx.commit();
					tx = session.beginTransaction();
				}
			}
			
		} catch (HibernateException ex) {
			msg = new MessageBean();
			msg.setStrMsgText(ex.getMessage());
			exUAVS = new UMBException("Hibernate Exception", ex, msg);
			throw exUAVS;
		} finally {
			if (null != session) {
				tx.commit();
				session.flush();
				session.close();
			}
			return true;
		}
	}

	public boolean deleteRecords(String strBean, String strID) throws UMBException {
		Session session = null;
		UMBException exUAVS = null;
		MessageBean msg = null;
		try {
			session = new HibernateUtil().getSessionFactory(request).openSession();
			Transaction tx = session.beginTransaction();
			strID = strID.replaceAll(";", ",");
			strID = strID.substring(1,strID.length()-1);
			Query objQuery  = session.createQuery("update " + strBean  + " bn set bn.intStatus = "+ IUMCConstants.STATUS_DELETED+ " where bn.lngID in ( " + strID + ")");
			objQuery.executeUpdate();
			tx.commit();
						
		}catch (HibernateException ex) {
			msg = new MessageBean();
			msg.setStrMsgText(ex.getMessage());
			exUAVS =  new UMBException("Hibernate Exception", ex, msg);
			throw exUAVS;
		} finally {
			if (null != session) {
				session.flush();
				session.close();
			}
		}
		return true;
		
	}
	
	public  boolean deleteRecords(String strBean, String strID, boolean blnHardDelete) throws UMBException {
		Session session = null;
		UMBException exUAVS = null;
		MessageBean msg = null;
		try {
			session = new HibernateUtil().getSessionFactory(request).openSession();
			Transaction tx = session.beginTransaction();
			strID = strID.replaceAll(";", ",");
			strID = strID.substring(1,strID.length()-1);
			if(blnHardDelete){
				Query objQuery  = session.createQuery("DELETE FROM " + strBean  + " bn where bn.lngID in ( " + strID + ")");
				objQuery.executeUpdate();
			}else{
				Query objQuery  = session.createQuery("update " + strBean  + " bn set bn.intStatus="+ IUMCConstants.STATUS_DELETED + " where bn.lngID in ( " + strID + ")");
				objQuery.executeUpdate();
			}
			tx.commit();
			
		}catch (HibernateException ex) {
			msg = new MessageBean();
			msg.setStrMsgText(ex.getMessage());
			exUAVS =  new UMBException("Hibernate Exception", ex, msg);
			throw exUAVS;
		} finally {
			if (null != session) {
				session.flush();
				session.close();
			}
		}
		return true;
	}
	public boolean rejectRecords(String strBean, String strID, String strComments) throws UMBException {
		Session session = null;
		UMBException exUAVS = null;
		MessageBean msg = null;
		String strUpdateClause = "";
		try {
			session = new HibernateUtil().getSessionFactory(request).openSession();
			Transaction tx = session.beginTransaction();
			strID = strID.replaceAll(";", ",");
			strID = strID.substring(1,strID.length()-1);
			strUpdateClause = " bn set bn.intStatus="+ IUMCConstants.STATUS_REJECTED + " , strComments = '" + strComments + "'";
			Query objQuery  = session.createQuery("update " + strBean  + strUpdateClause + " where bn.lngID in ( " + strID + ")");
			objQuery.executeUpdate();
			tx.commit();
			
		}catch (HibernateException ex) {
			msg = new MessageBean();
			msg.setStrMsgText(ex.getMessage());
			exUAVS =  new UMBException("Hibernate Exception", ex, msg);
			throw exUAVS;
		} finally {
			if (null != session) {
				session.flush();
				session.close();
			}
		}
		return true;
	}
	@SuppressWarnings("rawtypes")
	public  boolean checkIsUnique(String strBeanName, String strAliasName, ArrayList<NameValueBean> arrNameValueList) {
		String strSQL = "";
		NameValueBean objNameValue = null;
		Session session = new HibernateUtil().getSessionFactory(request).openSession();
		strSQL = " From " + strBeanName + " " + strAliasName + " WHERE ";
		try{
			for (int i = 0; i < arrNameValueList.size(); i++) {
				objNameValue = arrNameValueList.get(i);
				if (objNameValue.isBlnKey() && WebUtil.parseInt(objNameValue.getStrBeanFieldValue()) > 0) {
					strSQL += strAliasName + "." + objNameValue.getStrBeanFieldName() + " != " + objNameValue.getStrBeanFieldValue() + " AND ";
				} else if (!objNameValue.isBlnKey()) {
					strSQL += strAliasName + "." + objNameValue.getStrBeanFieldName() + " = '" + objNameValue.getStrBeanFieldValue() + "' AND ";
				}
			}
			if (strSQL.contains("AND")) {
				strSQL = strSQL.substring(0, strSQL.lastIndexOf("AND"));
			} else {
				strSQL = strSQL.substring(0, strSQL.lastIndexOf("WHERE"));
			}
			strSQL += " AND intStatus = " + IUMCConstants.STATUS_ACTIVE;
			Query query = session.createQuery(strSQL);
			
			
			List queryResult = query.list();
			if (queryResult != null && queryResult.size() == 0) {
				return true;
			}
		}catch(HibernateException e){
			
		}finally{
			if (null != session) {
				session.flush();
				session.close();
			}
			
		}
		return false;
	}

	@SuppressWarnings("rawtypes")
	public boolean isObjectUsed(String strObjectName, String strObjectIDs) {
		List<ObjectLocatorMatrixBean> arrObjLocMatList = ObjectLocator.getObjectLocationMatrix(strObjectName);
		Iterator<ObjectLocatorMatrixBean> itObjLocMatList = arrObjLocMatList.iterator();
		ObjectLocatorMatrixBean objObjLocMatBean = null;
		List arrFoundList = null;
		strObjectIDs = strObjectIDs.replaceAll(";", ",");
		strObjectIDs = strObjectIDs.substring(1, strObjectIDs.length() - 1);
		for (; itObjLocMatList.hasNext();) {
			objObjLocMatBean = (ObjectLocatorMatrixBean) itObjLocMatList.next();
			String strWhereClause = objObjLocMatBean.getStrObjectLocatedTableColumn() + " in (" + strObjectIDs + ")";
			arrFoundList = getRecords(objObjLocMatBean.getStrObjectLocatedTableColumn(), objObjLocMatBean.getStrObjectLocatedTable(),
					strWhereClause, "");
			if (null != arrFoundList) {
				return true;
			}
		}
		return false;
	}

	@SuppressWarnings("rawtypes")
	public List<MessageBean> constructMessagesfromObjectLocator(String strMessage) {
		
		MessageBean msg = new MessageBean();
		ArrayList<MessageBean> arrMsg = new ArrayList<MessageBean>();
		HashMap<String, List> hmLocatedObjectMap = null;
		if (null != ObjectLocator.getHmLocatedObjectMap() && ObjectLocator.getHmLocatedObjectMap().size() > 0) {
			hmLocatedObjectMap = ObjectLocator.getHmLocatedObjectMap();
			Iterator<String> itSetLocatedObject = hmLocatedObjectMap.keySet().iterator();
			String strName;
			boolean blnNotString = true;
			Object objName;
			/*
			 * If there is no String field in the Object then it is enough to
			 * show the message once. This is done by using blnNotString.
			 */
			for (; itSetLocatedObject.hasNext();) {
				String strLocatedObjectMsg = itSetLocatedObject.next();
				Iterator itArrLocatedObjectList = hmLocatedObjectMap.get(strLocatedObjectMsg).iterator();
				for (; itArrLocatedObjectList.hasNext();) {
					objName = itArrLocatedObjectList.next();
					msg = new MessageBean();
					msg.setStrMsgType(IUMCConstants.ERROR);
					msg.setStrMsgText(strMessage + " " + strLocatedObjectMsg + " ");
					if (objName instanceof String) {
						strName = (String) objName;
						msg.setStrMsgText(msg.getStrMsgText().concat(strName));
						blnNotString = false;
					}
					arrMsg.add(msg);
					if (blnNotString) {
						break;
					}
				}
			}
		}
		return arrMsg;
	}
	
	//Commented not required here
	/*public static void autoSaveReportToDisk(ReportClientDocument reportClientDoc, OutputManagerBean objOPB) throws Exception {
		//NOTE: If parameters or database login credentials are required, they need to be set before.
		//calling the export() method of the PrintOutputController.
				
		//Export report and obtain an input stream that can be written to disk.
		//See the Java Reporting Component Developer's Guide for more information on the supported export format enumerations
		//possible with the JRC.
		ReportExportFormat refFormat = ReportExportFormat.PDF;
		
		switch (objOPB.getIntFileFormat()) {
		case IUMCConstants.EXP_FORMAT_PDF:
			refFormat = ReportExportFormat.PDF;
			break;
		case IUMCConstants.EXP_FORMAT_DOC:
			refFormat = ReportExportFormat.MSWord;
			break;
		case IUMCConstants.EXP_FORMAT_EXCEL:
			refFormat = ReportExportFormat.MSExcel;
			break;
		default:
			refFormat = ReportExportFormat.PDF;
			break;
		}
		
		ByteArrayInputStream byteArrayInputStream = (ByteArrayInputStream)reportClientDoc.getPrintOutputController().export(refFormat);
			
		//Release report.
		reportClientDoc.close();
		
		//Write file to disk...
		//String EXPORT_OUTPUT = EXPORT_LOC+ EXPORT_FILE;
		System.out.println(" OUTPUT FILE NAME " + objOPB.getStrFileLocation() + objOPB.getStrFileName());
		writeToFileSystem(byteArrayInputStream, objOPB.getStrFileLocation() + objOPB.getStrFileName());
		
	}
	
	// Write to file system
	
			public static void writeToFileSystem(ByteArrayInputStream byteArrayInputStream,String exportFile) throws Exception{
				// TODO Auto-generated method stub
				//Use the Java I/O libraries to write the exported content to the file system.
				byte byteArray[] = new byte[byteArrayInputStream.available()];

				//Create a new file that will contain the exported result.
				File file = new File(exportFile);
				FileOutputStream fileOutputStream = new FileOutputStream(file);

				ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(byteArrayInputStream.available());
				int x = byteArrayInputStream.read(byteArray, 0, byteArrayInputStream.available());

				byteArrayOutputStream.write(byteArray, 0, x);
				byteArrayOutputStream.writeTo(fileOutputStream);

				//Close streams.
				byteArrayInputStream.close();
				byteArrayOutputStream.close();
				fileOutputStream.close();
			}
			
			public static ReportClientDocument openReportDocument(OutputManagerBean objOPB) throws ReportSDKException {

				String db_username = "root";
				String db_password = "secret";
				ReportClientDocument reportClientDoc = new ReportClientDocument();
				reportClientDoc.open(objOPB.getStrReportPath(), 0);
				reportClientDoc.getDatabaseController().logon(db_username, db_password);

				
				return reportClientDoc;
			}	
*/
	//comment ends here		
			
			
}