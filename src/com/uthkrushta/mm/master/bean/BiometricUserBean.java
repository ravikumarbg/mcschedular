package com.uthkrushta.mm.master.bean;

public class BiometricUserBean {

	private long lngBiometricID;

	public long getLngBiometricID() {
		return lngBiometricID;
	}

	public void setLngBiometricID(long lngBiometricID) {
		this.lngBiometricID = lngBiometricID;
	}

	@Override
	public String toString() {
		return "BiometricUserBean [lngBiometricID=" + lngBiometricID + "]";
	}
	
	
}
